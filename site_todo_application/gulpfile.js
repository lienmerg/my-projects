/**
 * Created by Unicorn on 10/12/14.
 */

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    del = require('del'),
    jshint = require('gulp-jshint'),
    runSequence = require('run-sequence'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

//Clean the .tmp folder and dist folder (keep the .gitkeep)
gulp.task('clean', del.bind(null, ['.tmp', 'dist/*', '!dist/.gitkeep']));

// Copy all files from app to dist, except all html documents
gulp.task('copy', function(){
    return gulp.src([
        'app/*',
        '!app/**/*.html'
    ],{dot:true})
        .pipe(gulp.dest('dist'))
        .pipe(plugins.size({title:'copy'}));
});

// Copy all css images from app to dist
gulp.task('copycssimages', function(){
    return gulp.src([
        'app/styles/images/**/*',
        '!app/styles/images/**/.gitkeep'
    ],{dot:true})
        .pipe(gulp.dest('dist/styles/images'))
        .pipe(plugins.size({title:'copy'}));
});

// Copy all css fonts from app to dist
gulp.task('copycssfonts', function(){
    return gulp.src([
        'app/fonts/**/*',
        '!app/fonts/**/.gitkeep'
    ],{dot:true})
        .pipe(gulp.dest('dist/fonts'))
        .pipe(plugins.size({title:'copy'}));
});

// Copy all images (content) from app to dist
gulp.task('copyimages', function(){
    return gulp.src([
        'app/images/**/*',
        '!app/images/**/.gitkeep'
    ],{dot:true})
        .pipe(gulp.dest('dist/images'))
        .pipe(plugins.size({title:'copy'}));
});

// Copy all docs (content) from app to dist
gulp.task('copydocs', function(){
    return gulp.src([
        'app/documents/**/*',
        '!app/documents/**/.gitkeep'
    ],{dot:true})
        .pipe(gulp.dest('dist/documents'))
        .pipe(plugins.size({title:'copy'}));
});

// Styles
gulp.task('styles', function() {
    return gulp.src([
        'app/styles/**/*',
        '!app/styles/**/.gitkeep'
    ],{dot:true})
        //.pipe(gulp.dest('dist/styles'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifycss())
        .pipe(gulp.dest('dist/styles'))
        .pipe(plugins.size({title:'copy'}));
});

//Scripts
gulp.task('scripts', function() {
    return gulp.src([
        'app/scripts/**/*',
        '!app/scripts/**/.gitkeep'
    ],{dot:true})
        //.pipe(gulp.dest('dist/scripts'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('dist/scripts'))
        .pipe(plugins.size({title:'copy'}));
});

//Check my Custom JavaScript files --> Syntax check
gulp.task('jshint', function(){
   return gulp.src('app/scripts/**/*.js')
       .pipe(jshint('.jshintrc'))
       //.pipe(plugins.jshint())
       .pipe(plugins.jshint.reporter('jshint-stylish'));
});

//Get the assets from all the html documents and optimize them
gulp.task('html', function(){
    var assets = plugins.useref.assets({searchFor:'{.tmp, app}'});

    return gulp.src('app/**/*.html')
        .pipe(assets)
        .pipe(plugins.if('*.js', plugins.uglify()))
        .pipe(plugins.if('*.css', plugins.csso()))
        .pipe(assets.restore())
        .pipe(plugins.useref())
        .pipe(gulp.dest('dist'))
        .pipe(plugins.size({title:'html'}));
});

// Watch Files For Changes & Reload in the server
// Do Not Use This Task - It doesn't work for now!
gulp.task('serve', function () {
    browserSync({
        notify: false,
        logPrefix: 'WSK',
        https: false,
        server: {
            baseDir: [
                './'
            ],
            routes: {
                '/app': 'app',
                '/.tmp': '.tmp'
            },
            middleware: function (req, res, next) {
                req.url = '/app/index.html';
                return next();
            }
        },
        port:8081
    });

    gulp.watch(['app/**/*.html'], reload);
    gulp.watch(['app/scripts/**/*.js'], ['jshint'], reload);
    gulp.watch(['bower_components/**/*.js'], reload);
    gulp.watch(['app/images/**/*'], reload);
});

// Build and serve the output from the dist build
// 1. The task default will be executed first
// 2. After successfully executed the previous task
// A Node server will be launched on port
// EXEC: gulp serve:dist
gulp.task('serve:dist', ['default'], function () {
    browserSync({
        notify: false,
        logPrefix: 'WSK',
        https: false,
        server: 'dist',
        port:8082
    });
});

// Build Production Files, the Default Task
gulp.task('default', ['clean'], function (cb) {
    runSequence('copy', ['html', 'copycssimages', 'copycssfonts', 'copyimages', 'copydocs'], cb);
});