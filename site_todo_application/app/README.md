Zal de volgende folders bevatten:

* documents
* fonts
* images
* scripts
* styles

Zal de volgende bestanden bevatten rechtstreeks onder deze folder:

* 404.html
* humans.txt
* index.html
* manifest.webapp
* robots.txt
* sitemap.xml

Bestanden eventueel aangevuld met favicon en touch icons.