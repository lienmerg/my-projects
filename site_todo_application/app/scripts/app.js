/**
 * Created by Team Unicorn on 27/11/14.
 */

(function(){
    //onpage load
    setTimeout(function(){
        $("#logo").attr("class","").after(function(){
            $("#welcomScreen").animate({'opacity':'0'},1000).delay(100).hide(0);
        });


    }, 2000)

    //googlemapresize
    $('a[href="#/contact"]').click(function(){
        loadScript();
    })

    //toggle menu-hamburger layout
    $( "#nav-toggle").click(function() {
        $(this).toggleClass( "navigatieActive" );
    });

    //toggle subnavigation
    //verberg submenu
    $(".navigation ul").hide();

    //Enumertation of moods
    var priorities = {
        HIGH:1,
        MEDIUM:2,
        LOW:3,
        properties:{
            1:{id:1, name:'High'},
            2:{id:2, name:'Medium'},
            3:{id:3, name:'Low'}
        }
    };

    // Initialize the application
    var App = {
        init:function(){
            //Create the ListDBContext object via a clone
            this.listDBContext = ListDBContext;
            this.listDBContext.init('unicorn-app');//Initialize the ListDBContext
            //Cache the most important elements
            this.cacheElements();
            //Bind Events to the most important elements
            this.bindEvents();
            //Setup the routes
            this.setupRoutes();
            //Render the interface
            this.render();
        },
        setupRoutes:function(){
            this.router = crossroads;//Clone the crossroads object
            this.hash = hasher;//Clone the hasher object

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Crossroads settings
            var homeRoute = this.router.addRoute('/',function(){
                //Set the active page where id is equal to the route
                self.setActivePage('home');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('home');
            });
            var sectionRoute = this.router.addRoute('/{section}');//Add the section route to crossroads
            sectionRoute.matched.add(function(section){
                //Set the active page where id is equal to the route
                self.setActivePage(section);
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink(section);
            });//Hash matches to section route

            //this.router.routed.add(console.log, console);//Log all crossroads events

            //Hash settings
            this.hash.initialized.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse initial hash
            this.hash.changed.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse changes in the hash
            this.hash.init();//Start listening to the hashes
        },
        cacheElements:function(){
            //cache lists
            this.formListCreate = document.querySelector('#formListCreate');
            this.listOfLists = document.querySelector('#list-of-lists');
            this.selectLists = document.querySelector('#selectList');
            this.sortLists = document.querySelector('#listSort');
            this.listOfCategories = document.querySelector('#categories');
            this.createList = document.querySelector('#listAdd');
            this.createNewCategoryList = document.querySelector('#btnCategorySubmitList');
            this.listDetails = document.querySelector('#formListEdit');

            //cache tasks
            this.formTaskCreate = document.querySelector('#formTaskCreate');
            this.formNewTask = document.querySelector('#formNewTask');
            this.listOfTasks = document.querySelector('#list-of-tasks');
            this.createTask = document.querySelector('#taskAdd');
            this.createNewCategoryTask = document.querySelector('#btnCategorySubmitTask');
            this.listOfCategoriesTasks = document.querySelector('#categories-tasks');
            this.listOfFinishedTasks = document.querySelector('#list-of-finished-tasks');
            this.taskDetails = document.querySelector('#formTaskEdit');

            //cache deadlines
            this.listofDeadlines = document.querySelector('#list-of-deadlines');

            //cache settings
            this.formSettings = document.querySelector('#settingsPannel');
            this.saveSettings = document.querySelector('#saveSettings');
            this.fontKeuzeCss = document.querySelector('#fontKeuzeCss');
            this.styleKeuzeCss = document.querySelector('#styleKeuzeCss');

            //cache Alertbox
            this.Alertbox01 = document.querySelector('#alertBoxText01');
            this.Alertbox02 = document.querySelector('#alertBoxText02');

            //get reminder times
            this.reminderDay = document.querySelector('#reminderDay');
            this.reminderHour = document.querySelector('#reminderHour');
            this.reminderMinute = document.querySelector('#reminderMinute');

        },
        bindEvents:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Submit the list creation form --> listen to it!
            this.formListCreate.addEventListener('submit', function(ev){
                //Dismiss the default browser eventlistener for submit event
                ev.preventDefault();

                //Check Validity
                //console.log(this.checkValidity());

                //Create a new List Object
                var list = new List();
                list.id = Utils.guid();
                list.title = this.elements["txtTitleList"].value;
                list.category = this.elements["categories"].value;
                list.created = new Date().getTime();

                //Add List to the database via Context
                var lists = self.listDBContext.addList(list);
                //var categories = self.listDBContext.addCategory(category);

                //Refresh the user interface -> render
                self.render();

                //Go to lists
                self.hash.setHash('lists');

                return false;
            });

            //Submit the task creation form --> listen to it!
            this.formTaskCreate.addEventListener('submit', function(ev){
                //Dismiss the default browser eventlistener for submit event
                ev.preventDefault();

                //Get selected value of RadioButtonGroup
                var rdbtnGroupPriority = document.querySelector('.priority-settings input[type="radio"]:checked');
                console.log(rdbtnGroupPriority);

                //Check Validity
                //console.log(this.checkValidity());

                //Create a new Task Object
                var task = new Task();
                task.id = Utils.guid();
                task.title = this.elements["txtTitleTask"].value;
                task.category = this.elements["categories-tasks"].value;
                task.priority = convertPriorityStringToPriority(rdbtnGroupPriority.value);
                task.date = this.elements["lblDate"].value;
                task.description = this.elements["txtDescriptionTask"].value;
                task.created = new Date().getTime();
                task.toList = this.elements["selectList"].value;

                //Add Task to the database via Context
                var tasks = self.listDBContext.addTask(task);

                //Refresh the user interface -> render
                self.render();

                //Go to tasks
                self.hash.setHash('tasks');

                return false;
            });

            //Submit the Settings form --> listen to it!
            this.formSettings.addEventListener('submit', function(ev){
                //Dismiss the default browser eventlistener for submit event
                ev.preventDefault();

                //Check Validity
                //console.log(this.checkValidity());

                //Create a new Setting Object
                var setting = new Setting();
                setting.theme =  this.elements["theme"].value;
                setting.font = this.elements["font"].value;
                setting.reminder = this.elements["reminder"].value;
                setting.created = new Date().getTime();

                //Add Setting to the database via Context
                var settings = self.listDBContext.addSetting(setting);

                //Refresh the user interface -> render
                self.render();

                //Go to tasks
                self.hash.setHash('settings');

                return false;
            });

            //Submit the new category (tasks)
            this.createNewCategoryTask.addEventListener('click', function(ev){
                ev.preventDefault();

                //console.log(this.elements);

                var category = new Category();
                category = self.formTaskCreate.elements["txtCategoryTask"].value;
                //console.log(category);

                //Add category to the database via context
                self.listDBContext.addCategory(category);

                //Refresh the user interface -> render
                self.render();

                //Go to list-create
                self.hash.setHash('task-create');

            });

            //Submit the new category (lists)
            this.createNewCategoryList.addEventListener('click', function(ev){
                ev.preventDefault();

                //console.log(this.elements);

                var category = new Category();
                category = self.formListCreate.elements["txtCategoryList"].value;
                //console.log(category);

                //Add category to the database via context
                self.listDBContext.addCategory(category);

                //Refresh the user interface -> render
                self.render();

                //Go to list-create
                self.hash.setHash('list-create');

            });

            //Back buttons
            var backButtons = document.querySelectorAll('.back');
            if(backButtons !== null && backButtons.length > 0){
                _.each(backButtons, function(backButton){
                    backButton.addEventListener('click', function(ev){
                        ev.preventDefault();

                        window.history.go(-1);//Back to the previous page (HTML5 History API

                        return false;
                    });
                });
            }

            //Create New List
            this.createList.addEventListener('click', function(ev){
                ev.preventDefault();

                self.hash.setHash('list-create');

                return false;
            });

            //Create New Task
            this.createTask.addEventListener('click',function(ev){
                ev.preventDefault();

                self.hash.setHash('task-create');

                return false;
            });

            //Set settings
            this.saveSettings.addEventListener('click',function(ev){
                ev.preventDefault();

                var theme = document.getElementsByName('theme');
                var font = document.getElementsByName('font');
                var selectedTheme;
                var selectedFont;
                var reminder1 = document.getElementById('reminderDay').value;
                var reminder2 = document.getElementById('reminderHour').value;
                var reminder3 = document.getElementById('reminderMinute').value;
                var convertedTime = Utils.convertToReminder(reminder1,reminder2,reminder3);
                for (var i = 0, lengtha = theme.length; i < lengtha; i++) {
                    if (theme[i].checked) {
                        selectedTheme =  theme[i].value;
                        break;
                    }
                }
                for (var a = 0, lengthb = font.length; a < lengthb; a++) {
                    if (font[a].checked) {
                        selectedFont =  font[a].value;
                        break;
                    }
                }
                self.listDBContext.updateSetting(selectedTheme, selectedFont, convertedTime);
                return false;
            });

            //Sort lists; most recent ones first
            //this.sortLists.addEventListener('click', function(ev){
            //    ev.preventDefault();
            //
            //    self.listDBContext.lists = _.sortBy(self.listDBContext, 'title');
            //});
        },
        render:function(){
            this.renderLists();
            this.renderTasks();
            this.renderDeadlines();
            this.renderSettings();
            //this.renderListDetails();
            this.renderCategories();
        },
        renderLists:function(){
            var htmlStandaard =
                '<a href="#/list-create" id="listAdd" class="showOptions lab_item">' +
                '<div class="hexagon hexagon2">'+
                '<div class="hexagon-in1">'+
                '<div class="hexagon-in2">'+
                '<h3>Add New</h3>'+
                '<i class="fa fa-plus fa-3x"></i>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</a>';

            var html1 = '';
            this.listOfLists.innerHTML = '';

            var lists = this.listDBContext.getLists();

            if(lists !== null && lists.length > 0){
                _.each(lists, function(list){
                    html1 +=
                        '<a class="showOptions lab_item">'+
                        '<div class="list-item hexagon hexagon2" data-id="' + list.id + '" data-title="' + list.title + '">'+
                        '<div class="hexagon-in1">'+
                        '<div class="hexagon-in2">'+
                        '<div class="sliderOptions">'+
                        '<h3>' +  Utils.stringshortener(list.title) + '</h3>'+
                        '<h2>' +   Utils.stringshortener(list.category) + '</h2>'+
                        '<div class="actionButtons">'+
                        '<span class="list-delete" data-id="' + list.id + '"><i class="delete fa fa-times fa-3x" id="btnDelete"></i></span>'+
                        '<span class="list-edit" data-id="' + list.id + '"><i class="edit fa fa-pencil fa-3x"></i></span>'+
                        '<span class="list-completed"><i class="completed fa fa-check fa-3x" id="btnComplete"></i></span>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</a>';
                });
                //console.log(lists);
            }else{
                html1 = '<div class="nottingToShow"><p>:\'(<br>No lists to show</p></div>';
            }

            this.listOfLists.innerHTML = htmlStandaard + html1;

            var html2 = this.selectLists.innerHTML;
            this.selectLists.innerHTML = '';

            if(lists !== null && lists.length > 0){
                _.each(lists, function(list){
                    html2 += '<option value="' + list.id + '">' + list.title + '<option>';
                });
            }else{
                html2 += '<option>Attach to a list</option>';
            }

            this.selectLists.innerHTML += html2;

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Register listeners for all the lists
            var listNodes = document.querySelectorAll('.list-item');
            if(listNodes !== null && listNodes.length > 0){
                //console.log('test');
                _.each(listNodes,function(listNode){
                    listNode.querySelector('.list-delete')
                        .addEventListener('click', function(){
                            console.log(this);
                            //Get the id of the list by the dataset property of the data attributes
                            var idList = this.dataset.id;
                            //self.hash.setHash('list-details/' + id);
                            //Call the function: removeListById
                            var result = self.listDBContext.removeListById(idList);
                            if(result === 1){
                                var obj = document.querySelectorAll('.list-item[data-id="' + idList + '"]');
                                console.log(obj);
                                //Update the user interface
                                self.renderLists();
                            }

                        });
                });
            }

            //Edit list
            var editNodes = document.querySelectorAll('.list-item');
            if(editNodes !== null && editNodes.length > 0){
                _.each(editNodes,function(editNode){
                    editNode.querySelector('.list-edit')
                        .addEventListener('click', function(){
                            //Get the id of the list by the dataset property of the data attributes
                            var idList = this.dataset.id;

                            var listObj = self.listDBContext.getListById(idList);
                            console.log(listObj);
                            //Update the user interface
                            self.renderListDetails(listObj);

                        });
                });
            }
        },
        renderListDetails:function(listObj){
            this.listDetails.innerHTML = '';
            this.listDetails.dataset.id = listObj.id;

            //var idTask = document.querySelectorAll('data-list');
            //
            //var result = self.listDBContext.getTaskByToListId(idTask);
            //
            //if(result === 1){
            //    var taskObj = document.querySelectorAll('.task-item[data-list="' + idTask + '"]');
            //}

            var html = '' +
                '<h3>' + listObj.title + '</h3>'+
                '<h2>' + listObj.category +'</h2>'
            //'<ul>' + taskObj + '</ul>';
            //console.log(taskObj);

            this.listDetails.innerHTML = html;
//            var html3 = this.listDetails.innerHTML;

            //var list = this.listDBContext.getListById();
            //console.log(list);
            ////          if(listObj != null && listObj.length > 0){
            //              _.each(listObj, function(list){
            //                  html3 += '<h3>' + list.title + '</h3>'
            //                  + '<h2>' + list.category + '</h2>'
            //              });
            //          }else{
            //              html3 += '<h3>No title</h3>';
            //          }
            //          this.listTitle.innerHTML = html3;

        },
        renderTaskDetails:function(taskObj){
            this.taskDetails.innerHTML = '';
            this.taskDetails.dataset.id = taskObj.id;

            var html = '' +
                '<h3 contenteditable class="inputFields">' + taskObj.title + '</h3>'+
                '<h2 contenteditable class="inputFields">' + taskObj.category +'</h2>'+
                '<p contenteditable class="inputFields">'+ taskObj.description + '</p>';

            this.taskDetails.innerHTML = html;
        },
        renderSettings:function(){
            this.styleKeuzeCss.setAttribute('href','');
            this.fontKeuzeCss.setAttribute('href','');

            var settings = this.listDBContext.getSettings();
            var time = settings.reminder;

            this.styleKeuzeCss.setAttribute('href', settings.theme);
            this.fontKeuzeCss.setAttribute('href', settings.font);

            $('#settingsPannel').find('input[value="' + settings.theme + '"]').prop('checked', true);
            $('#settingsPannel').find('input[value="' + settings.font + '"]').prop('checked', true);

            var timeDay =  Utils.convertFromReminder(time,"d");
            this.reminderDay.setAttribute('value', timeDay);

            var timeMinusDay = time - (timeDay * 60 * 60 * 24);
            var timeHour = Utils.convertFromReminder(timeMinusDay,"h");
            this.reminderHour.setAttribute('value',  timeHour);

            var DayMinusHour = timeMinusDay - (timeHour * 60 * 60);
            var timeMinute = Utils.convertFromReminder(DayMinusHour,"m");
            this.reminderMinute.setAttribute('value',  timeMinute);
        },
        renderCategories:function(){
            //Add new category to the selection menu
            var html = '';// this.listOfCategories.innerHTML;
            this.listOfCategories.innerHTML = '';

            var categories = this.listDBContext.getCategories();

            if(categories !== null && categories.length > 0){
                _.each(categories, function(category){
                    html += '<option>' + category + '</option>';
                });
            }

            this.listOfCategories.innerHTML = html;

            var html2 = '';// this.listOfCategoriesTasks.innerHTML;
            this.listOfCategoriesTasks.innerHTML = '';

            if(categories !== null && categories.length > 0){
                _.each(categories, function(category){
                    html2 += '<option>' + category + '</option>';
                });
            }

            this.listOfCategoriesTasks.innerHTML = html2;
        },
        renderTasks:function(){
            var htmlStandaard =
                '<a href="#/task-create" id="taskAdd" class="showOptions lab_item">'+
                '<div class="hexagon hexagon2">'+
                '<div class="hexagon-in1">'+
                '<div class="hexagon-in2">'+
                '<h3>Add New</h3>' +
                '<i class="fa fa-plus fa-3x"></i>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</a>';

            var html1 = '';
            this.listOfTasks.innerHTML = '';

            var tasks = this.listDBContext.getTasks();

            if(tasks !== null && tasks.length > 0){
                _.each(tasks, function(task){
                    html1 +=
                        '<a class="showOptions lab_item">'+
                        '<div class="task-item hexagon hexagon2" data-id="' + task.id + '" data-list="' + task.toList + '">'+
                        '<div class="hexagon-in1">'+
                        '<div class="hexagon-in2">'+
                        '<div class="sliderOptions">'+
                        '<h3>' +  Utils.stringshortener(task.title) + '</h3>'+
                        '<h2>' +   Utils.stringshortener(task.category) + '</h2>'+
                        '<h4 class="priority">' + convertPriorityToIcon(task.priority) + '</h4>'+
                        '<div class="actionButtons">'+
                        '<span class="task-delete" data-id="' + task.id + '"><i class="delete fa fa-times fa-3x" id="btnDelete"></i></span>'+
                        '<span class="task-edit" data-id="' + task.id + '"><i class="edit fa fa-pencil fa-3x" id="btnEdit"></i></span>'+
                        '<span class="task-complete" data-id="' + task.id + '"><i class="completed fa fa-check fa-3x" id="btnComplete"></i></span>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</a>';
                });
            }else{
                html1 = '<div class="nottingToShow"><p>:\'(<br>No tasks to show</p></div>';
            }

            this.listOfTasks.innerHTML = htmlStandaard + html1;

            //if(tasks != null && tasks.length > 0){
            //    _.each(tasks, function(task){
            //        html2 += '<p>' + task.title + '</p>'
            //    });
            //}else{
            //    html2 = '';
            //}
            //
            //this.listDetails.innerHTML += html2;

            //AlertReminder

            var tasksToShow = 0;
            var htmlAlert = this.Alertbox01.innerHTML;
            if(tasks === null || tasks.length < 1){
                $('.closeAlert').parent().hide();
                $('.alert').hide();
            }else if((ListDBContext.calcTimeDifferences())){
                $('.closeAlert').parent().hide();
                $('.alert').hide();
            }else{
                $('.closeAlert').parent().show();
                $('.alert').show();
                if(tasks !== null && tasks.length > 0){
                    _.each(tasks, function(task){
                        tasksToShow +=1;
                        if(tasksToShow < 6){
                            htmlAlert += '<div>'
                            + '<h4>' + task.title + '</h4>'
                            + '<p>' +  task.category + '</p>'
                            + '</div><hr>';
                        }else
                        {
                        }
                    });
                }else{
                    htmlAlert = 'nothing to show';
                }
                this.Alertbox01.innerHTML = htmlAlert;
            }


            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Register listeners for all the tasks
            var taskNodes = document.querySelectorAll('.task-item');
            if(taskNodes !== null && taskNodes.length > 0){
                _.each(taskNodes,function(taskNode){
                    //console.log(taskNode.querySelector('.task-delete'));
                    taskNode.querySelector('.task-delete')
                        .addEventListener('click', function(){
                            console.log(this);
                            //Get the id of the task by the dataset property of the data attributes
                            var idTask = this.dataset.id;
                            //Call the function: removeTaskById
                            var result = self.listDBContext.removeTaskById(idTask);
                            if(result === 1) {
                                var obj = document.querySelectorAll('.task-item[data-id="' + idTask + '"]');
                                console.log(obj);
                                //Update the user interface
                                self.renderTasks();
                            }
                        });
                });
            }

            //Complete task
            var completeTaskNodes = document.querySelectorAll('.task-item');
            if(completeTaskNodes !== null && completeTaskNodes.length > 0){
                _.each(completeTaskNodes,function(completeTaskNode){
                    completeTaskNode.querySelector('.task-complete')
                        .addEventListener('click', function(){
                            console.log(this);
                            //Get the id of the task by the dataset property of the data attributes
                            var idTask = this.dataset.id;

                            var taskObj = self.listDBContext.getTaskById(idTask);

                            //Move the task object to finished tasks
                            self.listDBContext.addFinishedTask(taskObj);

                            //Call the function: removeTaskById
                            var result = self.listDBContext.removeTaskById(idTask);

                            if(result === 1) {
                                var obj = document.querySelectorAll('.task-item[data-id="' + idTask + '"]');
                                console.log(obj);
                                //Update the user interface
                                self.renderTasks();
                                self.renderFinishedTasks();
                            }

                        });
                });
            }

            //Edit task
            var editNodes = document.querySelectorAll('.task-item');
            if(editNodes !== null && editNodes.length > 0){
                _.each(editNodes,function(editNode){
                    editNode.querySelector('.task-edit')
                        .addEventListener('click', function(){
                            //Get the id of the list by the dataset property of the data attributes
                            var idTask = this.dataset.id;

                            var taskObj = self.listDBContext.getTaskById(idTask);
                            console.log(taskObj);
                            //Update the user interface
                            self.renderTaskDetails(taskObj);

                        });
                });
            }

        },
        renderFinishedTasks:function(){
            var htmlStandaard =
                '<a href="#/task-create" id="taskAdd" class="showOptions lab_item">'+
                '<div class="hexagon hexagon2">'+
                '<div class="hexagon-in1">'+
                '<div class="hexagon-in2">'+
                '<h3>Add New</h3>' +
                '<i class="fa fa-plus fa-3x"></i>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</a>';

            var html1 = '';
            this.listOfFinishedTasks.innerHTML = '';

            var finishedtasks = this.listDBContext.getFinishedTasks();

            if(finishedtasks !== null && finishedtasks.length > 0){
                _.each(finishedtasks, function(finishedtask){
                    html1 +=
                        '<a class="showOptions lab_item">'+
                        '<div class="task-item hexagon hexagon2" data-id="' + finishedtask.id + '" data-list="' + '">'+
                        '<div class="hexagon-in1">'+
                        '<div class="hexagon-in2">'+
                        '<div class="sliderOptions">'+
                        '<h3>' +  Utils.stringshortener(finishedtask.title) + '</h3>'+
                        '<h2>' +   Utils.stringshortener(finishedtask.category) + '</h2>'+
                        '<h2 class="priority">' + convertPriorityToIcon(finishedtask.priority) + '</h2>'+
                        '<div class="actionButtons">'+
                        '<span class="task-delete" data-id="' + finishedtask.id + '"><i class="delete fa fa-times fa-3x" id="btnDelete"></i></span>'+
                        '<span class="task-edit"><i class="edit fa fa-pencil fa-3x" id="btnEdit"></i></span>'+
                        '<span class="task-complete" data-id="' + finishedtask.id + '"><i class="completed fa fa-check fa-3x" id="btnComplete"></i></span>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</a>';
                });
            }else{
                html1 = '<div class="nottingToShow"><p>:\'(<br>No tasks to show</p></div>';
            }

            this.listOfFinishedTasks.innerHTML = htmlStandaard + html1;
        },
        renderDeadlines:function(){

            var html1 = "";
            this.listofDeadlines.innerHTML = '';

            var deadlines = this.listDBContext.getDeadlines();

            if(deadlines !== null && deadlines.length > 0){
                _.each(deadlines, function(deadline){
                    html1 +=
                        '<a class="showOptions lab_item">'+
                        '<div id="list-item" class="hexagon hexagon2">'+
                        '<div class="hexagon-in1">'+
                        '<div class="hexagon-in2">'+
                        '<div class="sliderOptions">'+
                        '<h3>' +  Utils.stringshortener(deadline.title) + '</h3>'+
                        '<h2>' +   Utils.stringshortener(deadline.category) + '</h2>'+
                        '<div class="actionButtons">'+
                        '<span class="list-delete"><i class="delete fa fa-times fa-3x" id="btnDelete"></i></span>'+
                        '<span class="list-edit"><i class="edit">' + Utils.timeToTwitterDateTimeString(deadline.created) + '</i></span>'+
                        '<i class="completed fa fa-check fa-3x" id="btnComplete"></i>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</a>';
                });
            }else{
                html1 = '<div class="nottingToShow"><p>:\'(<br>No Deadlines to show</p></div>';
            }

            //AlertReminder

            var ReminderToShow = 0;
            var htmlAlert = this.Alertbox02.innerHTML;
            if(deadlines === null || deadlines.length < 1){
                $('.closeAlert').parent().hide();
                $('.alert').hide();
            }else if((ListDBContext.calcTimeDifferences())){
                $('.closeAlert').parent().hide();
                $('.alert').hide();
            }else{
                $('.closeAlert').parent().show();
                $('.alert').show();
                if(deadlines !== null && deadlines.length > 0){
                    _.each(deadlines, function(deadline){
                        ReminderToShow +=1;
                        if(ReminderToShow < 6){
                            htmlAlert += '<div>'
                            + '<h4>' + deadline.title + '</h4>'
                            + '<p>' +  deadline.category + '</p>'
                            + '</div><hr>';
                        }else
                        {
                        }
                    });
                }else{
                    htmlAlert = 'nothing to show';
                }
                this.Alertbox02.innerHTML = htmlAlert;
            }

            this.listofDeadlines.innerHTML = html1;
        },
        //Event Listener: listen to matched section routes (parse)
        onSectionMatch:function(section){
            //Set the active page where id is equal to the route
            this.setActivePage(section);
            //Set the active menuitem where href is equal to the route
            this.setActiveNavigationLink(section);
        },
        setActivePage:function(section){
            //Set the active page where id is equal to the route
            var pages = document.querySelectorAll('.container');
            if(pages !== null && pages.length > 0){
                _.each(pages,function(page){
                    if(page.id === section){
                        page.classList.add('active');
                    }else{
                        page.classList.remove('active');
                    }
                });
            }
        },
        setActiveNavigationLink:function(section) {
            //Set the active menuitem where href is equal to the route
            var navLinks = document.querySelectorAll('.wrapper ul.navigation li a');
            if (navLinks !== null && navLinks.length > 0) {
                var effLink = '#/' + section;
                _.each(navLinks, function (navLink) {
                    if (navLink.getAttribute('href') === effLink) {
                        navLink.parentNode.classList.add('active');
                    } else {
                        navLink.parentNode.classList.remove('active');
                    }
                });
            }
        }
    };
    function convertPriorityStringToPriority(priorityStr){
        console.log(priorityStr);
        switch(priorityStr){
            case 'high':return priorities.HIGH;
            case 'medium':return priorities.MEDIUM;
            case 'low': return priorities.LOW;
            default:return priorities.HIGH;
        }
    }
    function convertPriorityToIcon(priority){
        switch(priority){
            case priorities.HIGH:return   '<svg x="0px" y="0px" viewBox="0 0 2.9 2.9" enable-background="new 0 0 2.9 2.9"><g><path fill="#FF0000" d="M1.5,0L0.2,0.7v1.4l1.2,0.7l1.2-0.7V0.7L1.5,0z M1.5,0.6c0.1,0,0.1,0,0.1,0.1c0,0,0,0.1,0,0.2 c0,0.3,0,0.6-0.1,0.9c0,0-0.1,0-0.1,0c0-0.3,0-0.6-0.1-0.9c0-0.1,0-0.2,0-0.2C1.3,0.6,1.4,0.6,1.5,0.6z M1.5,2.3 c-0.1,0-0.1-0.1-0.1-0.1C1.3,2.1,1.4,2,1.5,2c0.1,0,0.1,0.1,0.1,0.1C1.6,2.2,1.5,2.3,1.5,2.3z"/></g></svg>';
            case priorities.MEDIUM:return '<svg x="0px" y="0px" viewBox="0 0 2.9 2.9" enable-background="new 0 0 2.9 2.9"><g><path fill="#3399FF" d="M1.5,0L0.2,0.7v1.4l1.2,0.7l1.2-0.7V0.7L1.5,0z M1.5,0.6c0.1,0,0.1,0,0.1,0.1c0,0,0,0.1,0,0.2 c0,0.3,0,0.6-0.1,0.9c0,0-0.1,0-0.1,0c0-0.3,0-0.6-0.1-0.9c0-0.1,0-0.2,0-0.2C1.3,0.6,1.4,0.6,1.5,0.6z M1.5,2.3 c-0.1,0-0.1-0.1-0.1-0.1C1.3,2.1,1.4,2,1.5,2c0.1,0,0.1,0.1,0.1,0.1C1.6,2.2,1.5,2.3,1.5,2.3z"/></g></svg>';
            case priorities.LOW:return    '<svg x="0px" y="0px" viewBox="0 0 2.9 2.9" enable-background="new 0 0 2.9 2.9"><g><path fill="#009245" d="M1.5,0L0.2,0.7v1.4l1.2,0.7l1.2-0.7V0.7L1.5,0z M1.5,0.6c0.1,0,0.1,0,0.1,0.1c0,0,0,0.1,0,0.2 c0,0.3,0,0.6-0.1,0.9c0,0-0.1,0-0.1,0c0-0.3,0-0.6-0.1-0.9c0-0.1,0-0.2,0-0.2C1.3,0.6,1.4,0.6,1.5,0.6z M1.5,2.3 c-0.1,0-0.1-0.1-0.1-0.1C1.3,2.1,1.4,2,1.5,2c0.1,0,0.1,0.1,0.1,0.1C1.6,2.2,1.5,2.3,1.5,2.3z"/></g></svg>';
            default:return '.9';
        }
    }

    App.init();//Call the init function

    //Kleine animaties in app
    $('.systemButton').click(function(e){
        e.preventDefault();
        //tonen en verbergen van settingsmodules
        var toonSettingsModule = $(this).attr('id');
        console.log(toonSettingsModule);
        $(".settingsModule").addClass('hidden');
        $(".settingsModule[data-id='" + toonSettingsModule +"']").removeClass('hidden');
        //tonen van het settinspanneel
        $(this).parent().parent().parent().find(".systemPannel").addClass('systemPannelShow');
        return false;
    });
    $('.list-edit').click(function(e){
        e.preventDefault();
        //tonen en verbergen van list
        var showList = $(this).parent().attr('data-title');
        //console.log(showList);
        //tonen van de list
        $(this).parents().eq(9).find(".systemPannel").addClass('systemPannelShow');
        console.log($(this).parents().eq(9));
        //$('#list-title').html('test');
        return false;
    });
    $('.task-edit').click(function(e){
        e.preventDefault();
        //tonen en verbergen van taak
        var showList = $(this).parent().attr('data-title');
        //tonen van de taak
        $(this).parents().eq(9).find(".systemPannel").addClass('systemPannelShow');
        console.log($(this).parents().eq(9));
        //$('#list-title').html('test');
        return false;
    });
    $('.closePannel').click(function(e){
        e.preventDefault();
        $(this).parent().removeClass('systemPannelShow');
        return false;
    });
    // Switch themes
    $(".themeOfChoice").on("click", function(event){
        var themaKeuze = $(this).data('href');
        $("#styleKeuzeCss").attr("href",themaKeuze);
    });
    $(".fontOfChoice").on("click", function(event){
        var fontKeuze = $(this).data('href');
        $("#fontKeuzeCss").attr("href",fontKeuze);
    });
    $(".closeAlert").click(function(){
        $(this).parent().hide();
        $('.alert').hide();
    });

})();