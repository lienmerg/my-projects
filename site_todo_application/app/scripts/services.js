/**
 * Created by TeamUnicorn on 22/11/14.
 */

var ListDBContext = {
    init:function(connString){
        this.connString = connString;
        //Create the AppData object for the ToDo Application
        this.AppData = {
            "information":{
                "title":"BeeZee Application",
                "version":"1.0",
                "modified":"22-11-2014",
                "author":"Team UniCorn"
            },
            "lists":[],
            "tasks":[],
            "finishedtasks":[],
            "categories":["work","personal","hobby"],
            "settings":{
                "theme":"styles/style1.css",
                "font":"styles/font01.css",
                "updated":0,
                "reminder":94800
            }
        };
        //Get Application Data from the localstorage
        if(Utils.store(this.connString) !== null){
            this.AppData = Utils.store(this.connString);
        }else{
            Utils.store(this.connString, this.AppData);
        }
    },
    getLists:function(){
        var lists = this.AppData.lists;
        if(lists === null)
            return null;
        lists = _.sortBy(lists, 'title');
        return lists;
    },
    getTasks:function(){
        var tasks = this.AppData.tasks;
        if(tasks === null)
            return null;
        tasks = _.sortBy(tasks, 'title');
        return tasks;
    },
    getFinishedTasks:function(){
        var finishedtasks = this.AppData.finishedtasks;
        if(finishedtasks === null)
            return null;
        finishedtasks = _.sortBy(tasks, 'title');
        return finishedtasks;
    },
    getSettings:function(){
        var settings = this.AppData.settings;
        if(settings === null)
            return null;

        return settings;
    },
    getDeadlines:function(){
        var deadlines = this.AppData.tasks;
        if(deadlines === null)
            return null;
        deadlines = _.sortBy(deadlines, 'created');
        return deadlines;
    },
    getCategories:function(){
        var categories = this.AppData.categories;
        if(categories === null)
            return null;

        return categories;
    },
    getListById:function(id){
        var list = _.find(this.AppData.lists, function(list){
            return list.id === id;
        });

        return list;
    },
    getTaskById:function(id){
        var task = _.find(this.AppData.tasks, function(task){
            return task.id === id;
        });

        return task;
    },
    getTaskByToListId:function(id){
        var task = _.find(this.AppData.tasks, function(task){
            return task.toList === id;
        });

        return task;
    },
    addList:function(list){
        this.AppData.lists.push(list);
        this.save();

        return 1;
    },
    addTask:function(task){
        this.AppData.tasks.push(task);
        this.save();

        return 1;
    },
    addFinishedTask:function(finished){
        this.AppData.finishedtasks.push(finished);
        this.save();
        return 1;
    },
    addSetting:function(setting){
        this.AppData.settings.push(setting);
        this.save();

        return 1;
    },
    addCategory:function(category){
        this.AppData.categories.push(category);
        this.save();
        return 1;
    },
    updateSetting:function(settingTheme, settingFont, settingsReminder){
        var settings = this.AppData.settings;
        settings.updated = new Date().getTime();
        settings.theme = settingTheme;
        settings.font = settingFont;
        settings.reminder = settingsReminder;
        this.save();
        return 1;
    },
    removeListById:function(idList){
        if(this.AppData !== null && this.AppData.lists !== null && this.AppData.lists.length > 0) {
            //Find the list in the Array by his id
            var index = _.findIndex(this.AppData.lists, function (list) {
                return list.id === idList;
            });

            if (index > -1){
                //Remove the list from the Array
                this.AppData.lists.splice(index, 1);

                this.save();

                return 1;
            }
        }
        return 0;
    },
    removeTaskById:function(idTask){
        if(this.AppData !== null && this.AppData.tasks !== null && this.AppData.tasks.length > 0) {
            //Find the task in the Array by his id
            var index = _.findIndex(this.AppData.tasks, function (task) {
                return task.id === idTask;
            });

            if (index > -1){
                //Remove the task from the Array
                this.AppData.tasks.splice(index, 1);

                this.save();

                return 1;
            }
        }
        return 0;
    },
    save:function(){
        Utils.store(this.connString, this.AppData);
    },
    calcTimeDifferences: function(){
        var curTime = new Date().getTime();
        console.log(curTime);
        var settings = this.AppData.settings;
        var summedTime = settings.updated + settings.reminder;
        console.log(summedTime);
        if(curTime > summedTime){
            return false;
        }else{
            return true;
        }
    }
};