/**
 * Created by Unicorn on 29/11/14.
*/
    function initialize() {
        //Map position
        var myLatlng = new google.maps.LatLng(51.087028, 3.671567);
        //map view
        var mapOptions = {
            zoom: 16,
            center: myLatlng,
            disableDefaultUI: true
        };
        //marker
        var image = 'images/markers/weezee-tumb.png';
        var marker = new google.maps.Marker({
            position: myLatlng,
            title: 'BeeZee',
            icon: image
        });
        var contentString = '<div id="content">'
            + '<div id="siteNotice">'
            + '</div>'
            + '<h1 id="firstHeading" class="firstHeading">BeeZee</h1>'
            + '<div id="bodyContent">'
            + '<p><b>BeeZee</b>, is a web appliction produced by <b>Unicorn</b>'
            + 'commissioned by <b>Arteveldehoogeschool</b>'
            + 'by the OLOD <b>New Media Design & Development</b></p><p>'
            + 'Adress:<br><b>Mediacampus Mariakerke Arteveldehoogeschool</b>:<br>'
            + 'Industrieweg 232<br>'
            + '9030 Gent-Mariakerke'
            + '</p><br><br>'
            + '</div>'
            + '</div>';
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });

    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        // Add Marker to Map
        marker.setMap(map);
    }

    function loadScript() {
        var key = 'AIzaSyBcleTUCnk1hbD-FGiKpWDJY7MYjfESaqs';
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp'
        + '&key=' + key
        + '&callback=initialize';
        document.body.appendChild(script);
    }

    window.onload = loadScript;
