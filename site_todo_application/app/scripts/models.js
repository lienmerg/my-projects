/**
 * Created by Team Unicorn on 22/11/14.
 */

/*
 Object Literal for List
 */

/*
 Object constructor for Task (-> class)
 Public properties, methods
 */
function Task(){
    this.id;
    this.title;
    this.category;
    this.description;
    this.created;
    this.priority;
    this.date;
    this.toList;
    this.updated = null;
    this.deleted = null;//soft-delete
}
/*
 Object constructor for List (-> class)
 Public properties, methods
 */
function List(){
    this.id;
    this.title;
    this.category;
    this.created;
    this.updated = null;
    this.deleted = null;//soft-delete
}
/*
 Object constructor for Categories (-> class)
 Public properties, methods
 */
function Category(){
    this.id;
    this.title;
    this.created;
}
/*
 Object constructor for Settings (-> class)
 Public properties, methods
 */
function Setting(){
    this.theme;
    this.font;
    this.reminder;
    this.updated;
}


