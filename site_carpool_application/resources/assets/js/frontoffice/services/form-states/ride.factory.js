/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('RideFormStateFactory', RideFormStateFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideFormStateFactory.$inject = [
        // Custom
        'RideModelFactory',
        'UserModelFactory'
    ];

    function RideFormStateFactory(
        // Custom
        //DriverModelFactory,
        RideModelFactory,
        UserModelFactory
    ) {
        return {
            //driver  : new DriverModelFactory(),
            ride    : new RideModelFactory(),
            user    : new UserModelFactory()
        };
    }

})();
