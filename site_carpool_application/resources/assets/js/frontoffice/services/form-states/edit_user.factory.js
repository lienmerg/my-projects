/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('EditUserFormStateFactory', EditUserFormStateFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    EditUserFormStateFactory.$inject = [
        // Custom
        'AddressModelFactory',
        'CompanyModelFactory',
        'CountryModelFactory',
        'LocalityModelFactory',
        'RegionModelFactory',
        'SettingsModelFactory',
        'UserModelFactory',
        'DriverModelFactory'
    ];

    function EditUserFormStateFactory (
        // Custom
        AddressModelFactory,
        CompanyModelFactory,
        CountryModelFactory,
        LocalityModelFactory,
        RegionModelFactory,
        SettingsModelFactory,
        UserModelFactory,
        DriverModelFactory
    ) {
        return {
            address : new AddressModelFactory(),
            country : new CountryModelFactory(),
            company : new CompanyModelFactory(),
            locality: new LocalityModelFactory(),
            region  : new RegionModelFactory(),
            settings: new SettingsModelFactory(),
            user    : new UserModelFactory(),
            driver  : new DriverModelFactory()
        };
    }
})();
