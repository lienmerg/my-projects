/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('DriverResourceFactory', DriverResourceFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DriverResourceFactory.$inject = [
        // Angular
        '$resource',
        // Custom
        'GenericResponseTransformerFactory',
        'UriFactory',
        'DriverResponseInterceptorFactory'
    ];

    function DriverResourceFactory(
        // Angular
        $resource,
        // Custom
        GenericResponseTransformerFactory,
        UriFactory,
        DriverResponseInterceptorFactory
    ) {
        var url = UriFactory.getApi('drivers/:driverId');

        var paramDefaults = {
            driverId: '@id'
        };

        var actions = {
            'get': {
                interceptor: {
                    response: DriverResponseInterceptorFactory
                },
                isArray: false,
                method: 'GET',
                transformResponse: GenericResponseTransformerFactory
            },
            'query': {
                interceptor: {
                    response: DriverResponseInterceptorFactory
                },
                isArray: true,
                method: 'GET',
                transformResponse: GenericResponseTransformerFactory
            },
            'save': {
                isArray: false,
                method: 'POST',
                transformResponse: GenericResponseTransformerFactory
            },
            'update': {
                interceptor: {
                    response: DriverResponseInterceptorFactory
                },
                isArray: false,
                method: 'PUT',
                transformResponse: GenericResponseTransformerFactory
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();