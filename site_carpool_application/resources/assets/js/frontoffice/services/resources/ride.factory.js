/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('RideResourceFactory', RideResourceFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideResourceFactory.$inject = [
        // Angular
        '$resource',
        // Custom
        'GenericResponseTransformerFactory',
        'RideResponseInterceptorFactory',
        'UriFactory'
    ];

    function RideResourceFactory(
        // Angular
        $resource,
        // Custom
        GenericResponseTransformerFactory,
        RideResponseInterceptorFactory,
        UriFactory
    ) {
        var url = UriFactory.getApi('rides/:rideId');

        var paramDefaults = {
            rideId: '@id'
        };

        var actions = {
            'query': {
                interceptor: {
                    response: RideResponseInterceptorFactory
                },
                isArray: true,
                method: 'GET',
                transformResponse: GenericResponseTransformerFactory
            },
            'save': {
            isArray: false,
                method: 'POST',
                transformResponse: GenericResponseTransformerFactory
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();