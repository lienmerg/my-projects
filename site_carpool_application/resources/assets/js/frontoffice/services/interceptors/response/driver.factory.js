/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('DriverResponseInterceptorFactory', DriverResponseInterceptorFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DriverResponseInterceptorFactory.$inject = [
        // Angular
        '$log',
        // Custom
        'DriverModelFactory'
    ];

    function DriverResponseInterceptorFactory(
        // Angular
        $log,
        // Custom
        DriverModelFactory
    ) {
        function interceptor(response) {
            //$log.info('UserResponseInterceptor:', response);
            if (angular.isArray(response.data)) {
                response.resource = response.data.map(transformToModel);
            } else {
                response.resource = transformToModel(response.data);
            }

            return response;
        }

        function transformToModel(data) {
            return new DriverModelFactory(data);
        }

        return interceptor;
    }

})();
