/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('RideModelFactory', RideModelFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideModelFactory.$inject = [
        // Angular
        '$log'
    ];

    function RideModelFactory(
        // Angular
        $log
    ) {
        /**
         * Ride Model. A driver has rides.
         *
         * @param data
         * @constructor
         */
        function Ride(data) {
            data = data || {};

            // PK
            this.id = data.id || null;

            // FK
            this.user_id = data.user_id || null;

            // Properties
            this.ride_from = data.ride_from || null;
            this.ride_to = data.ride_to || null;
            this.ride_starttime = data.ride_starttime || null;
            this.ride_endtime = data.ride_endtime || null;
            this.ride_price = data.ride_price || null;
            this.ride_comments = data.ride_comments || null;


            // Properties omitted from JSON by Angular due to `$$` prefix.
            //this.$$name = angular.isDefined(data.name) ? angular.uppercase(data.name) : null; // Upper case name for searches.
        }

        // Methods
        // =======

        $log.log(Ride);

        return Ride;
    }

})();
