/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('DriverModelFactory', DriverModelFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DriverModelFactory.$inject = [
        // Angular
        '$log',
        // Custom
        'RideModelFactory'
    ];

    function DriverModelFactory(
        // Angular
        $log,
        // Custom
        RideModelFactory
    ) {
        /**
         * Driver Model.
         *
         * @param data
         * @constructor
         */
        function Driver(data) {
            data = data || {};

            moment.locale('en');

            // PK
            this.id = data.id || null;

            // FK
            this.user_id = data.user_id || null;
            this.ride_id = data.ride_id || null;

            // Properties
            this.role = data.role || null;
            this.driverseats      = data.driverseats || null;
            this.car  = data.car || null;
            this.rides = data.rides || [];

            // Properties omitted from JSON by Angular due to `$$` prefix.
            this.$$rides          = []; // All possible rides should be pushed to this array.
        }

        // Methods
        // =======
        Driver.prototype.$$addRide = function (chip) {
            var ride;

            if (angular.isObject(chip)) {
                ride = chip;
            } else {
                var data = { name: chip };
                ride = new RideModelFactory(data);

                // Add to complete list of interests (`md-no-cache` required on `md-autocomplete`)
                this.$$rides.push(ride);
            }
            return ride;
        };

        return Driver;
    }

})();