/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('UserUpdateCtrl', UserUpdateCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    UserUpdateCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        '$window',
        // Custom
        'DialogFactory',
        'UserResourceFactory',
        'EditUserFormStateFactory'
    ];

    function UserUpdateCtrl(
        // Angular
        $log,
        $scope,
        $window,
        // Custom
        DialogFactory,
        UserResourceFactory,
        EditUserFormStateFactory
    ) {
        $log.info('UserUpdateCtrl');

        var vm = this;

        var formState  = EditUserFormStateFactory;
        vm.address     = formState.address;
        vm.country     = formState.country;
        vm.locality    = formState.locality;
        vm.region      = formState.region;
        vm.settings    = formState.settings;
        vm.user        = formState.user;

        vm.name        = formState.user.name;
        vm.family_name = formState.user.family_name;
        vm.mobile      = formState.user.mobile;
        //vm.driver      = formState.driver;

        $log.log('vm:', vm);

        vm.processFormStep = processFormStep;

        $scope.showDetail = function(user) {
            $scope.user = user;
            $log.log(user);
            $window.location.href = '#/users/update';
        };

        function processFormStep(event) {
            event.preventDefault();
            $log.info('user:', vm.user);
            if ($scope.edit_user_form.$valid) {
                // Set the form state.
                formState.user             = vm.user;
                formState.user.name        = vm.name;
                formState.user.family_name = vm.family_name;
                formState.user.mobile      = vm.mobile;
                updateUser(user.id);
            }
            $log.info('vm:', vm);
        }

        //$scope.updateUser = function(user) {
        //    UserResourceFactory.update( { userId: user }, function() {
        //        $log.log('The user has been updated');
        //    })
        //        .then(updateUserResourceSuccess())
        //        .catch(updateUserResourceError());
        //};

        function updateUser(user) {
            UserResourceFactory.update( { userId: user }, function() {
                $log.log('The user has been updated');
            })
                .then(updateUserResourceSuccess)
                .catch(updateUserResourceError);
        }

        //function updateUser() {
        //    var userResource = new UserResourceFactory;
        //    var params = {
        //        userId: vm.user.id
        //    };
        //    userResource.$get(params)
        //        .then(function () {
        //            //userResource.driver_id = vm.driver.id;
        //            userResource.$update()
        //                .then(updateUserResourceSuccess)
        //                .catch(updateUserResourceError);
        //        });
        //}

        function updateUserResourceError(reason) {
            $log.error('updateUserResourceError:', reason);
            DialogFactory
                .showItems('The user profile could not be updated!', reason.errors);
        }

        function updateUserResourceSuccess(response) {
            $log.log('updateUserResourceSuccess:', response);
        }
    }

})();