/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('UsersCtrl', UsersCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    UsersCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        // Custom
        'UserResourceFactory',
        'StorageFactory',
        'DialogFactory',
        'AddressResourceFactory'
    ];

    function UsersCtrl(
        // Angular
        $log,
        $scope,
        // Custom
        UserResourceFactory,
        AddressResourceFactory,
        StorageFactory,
        DialogFactory
    ) {
        $log.info('UsersCtrl');

        $scope.users = UserResourceFactory.query();

        //SHOW USER DETAILS - SELECTED ROW
        $scope.selIdx= -1;
        $scope.selUser = function(user,idx){
            $scope.selectedUser = user;
            $scope.selIdx = idx;
        };

        $scope.isSelUser = function(user){
            return $scope.selectedUser === user;
        };

        //DELETE USER
        $scope.deleteUserSubject = function(user) {
            UserResourceFactory.delete( { userId: user }, function() {
                $log.log('Deleted from server');
                $scope.users = UserResourceFactory.query();
            } )
        };

        //$scope.deleteUser = function(id){
        //    $scope.deletedUser = id;
        //    $log.log('user_id: ', $scope.deletedUser);
        //
        //    //UserResourceFactory.delete(id);
        //    UserResourceFactory.delete(id)
        //        .then(deleteUserSuccess)
        //        .catch(deleteUserError);
        //    $scope.users = UserResourceFactory.query();
        //    //$log.log($scope.users);
        //};


        //$scope.deleteUser = function(id) {
        //    $log.info('deleteUser');
        //    $scope.deletedUser = id;
        //    $log.log('user_id: ', $scope.deletedUser);
        //    UserResourceFactory.delete();
        //};

        //$scope.deleteUserSubject = function(deleteduser) {
        //    var id = deleteduser.id;
        //    user.deleteUser(id)
        //        .then(function(success) {
        //            UserResourceFactory.query();
        //            $log.log(success);
        //        }, function(error) {
        //            $log.log(error)
        //        })
        //};

        //$scope.userResource = UserResourceFactory.get({ userId: $scope.id }, function() {
        //    $scope.userResource.$delete(function() {
        //        //gone forever!
        //        $log.log('Deleted from server');
        //    });
        //});

        //$scope.deleteUser = function(id) {
        //    var user = UserResourceFactory.get(id, function() {
        //        user.deletedUser = id;
        //        $log.log('user_id: ', user.deletedUser);
        //        user.$delete()
        //            .then(deleteUserSuccess)
        //            .catch(deleteUserError);
        //    })
        //};

        //$scope.remove = function (id,index){
        //    var removeUser = UserResourceFactory.delete(id);
        //    removeUser.success(function(response){
        //        $scope.flash = response.status;
        //        $scope.users.splice(index,1);
        //        $scope.users = UserResourceFactory.query();
        //    })
        //};

        //function showDetails(){
        //    $log.info('showDetails');
        //    var user = UserResourceFactory.get();
        //    user.then(function(data) {
        //        $scope.get = data;
        //        console.log(data);
        //    });
        //    $scope.select = function(user) {
        //        $scope.selected = user;
        //    };
        //    $scope.selected = {};
        //}
    }

})();