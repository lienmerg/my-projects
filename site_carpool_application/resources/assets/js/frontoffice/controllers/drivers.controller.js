/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('DriversCtrl', DriversCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DriversCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        // Custom
        'DriverResourceFactory',
        'StorageFactory',
        'DialogFactory'
    ];

    function DriversCtrl(
        // Angular
        $log,
        $scope,
        // Custom
        DriverResourceFactory,
        StorageFactory,
        DialogFactory
    ) {
        $log.info('DriversCtrl');

        $scope.drivers = DriverResourceFactory.query();

    }

})();