/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('RideCreateCtrl', RideCreateCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideCreateCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        // Custom
        'DialogFactory',
        'RideFormStateFactory',
        'RideResourceFactory',
        'UserResourceFactory',
        'DriverResourceFactory'
    ];

    function RideCreateCtrl(
        // Angular
        $log,
        $scope,
        // Custom
        DialogFactory,
        RideFormStateFactory,
        RideResourceFactory,
        UserResourceFactory,
        DriverResourceFactory
    ) {
        $log.info('RideCreateCtrl');

        //View Model
        var vm = this;

        // Get the form state.
        var formState   = RideFormStateFactory;
        vm.ride         = formState.ride;
        vm.user         = formState.user;
        //vm.user         = StorageFactory.Local.getUserModel('user');
        //vm.ride.user_id = vm.user.id;
        vm.ride_from    = formState.ride_from;
        vm.ride_to      = formState.ride_to;
        $log.log('vm:', vm);

        vm.processFormStep = processFormStep;

        function processFormStep(event) {
            event.preventDefault();
            $log.info('ride: ', vm.ride);
            $log.info('user: ', vm.user);
            if ($scope.ride_creation_form.$valid) {
                // Set the form state.
                //formState.driver = vm.driver;
                formState.ride      = vm.ride;
                formState.user      = vm.user;
                formState.ride_from = vm.ride_from;
                formState.ride_to   = vm.ride_to;
                saveRide();
            }
        }

        function saveRide(){
            $log.info('saveRide');
            if (!angular.isNumber(formState.ride.id)) {
                var rideResource  = new RideResourceFactory();
                var userResource = new UserResourceFactory();
                //var paramsRide = {
                //    rideId : formState.ride.id
                //};
                var paramsUser = {
                    userId : formState.user.id
                };
                userResource.$get(paramsUser);
                rideResource.ride = vm.ride;
                //rideResource.ride.user_id = vm.user.id;
                //rideResource.driver = formState.driver;
                $log.log(rideResource);
                userResource.$update(paramsUser);
                rideResource.$save()
                    .then(saveRideResourceSuccess)
                    .catch(saveRideResourceError);
                //updateDriver();
            }
        }

        function saveRideResourceError(reason) {
            $log.error('saveRideResourceError:', reason);
            DialogFactory
                .showItems('The ride could not be saved!', reason.errors);
        }

        function saveRideResourceSuccess(response) {
            $log.log('saveRideResourceSuccess:', response);
            // Update the form state.
            formState.ride.id          = response.id;
            //formState.user.id          = response.user.id;
            formState.ride.ride_from   = response.ride_from;
            formState.ride.ride_to     = response.ride_to;
            //formState.ride.user_id     = response.user.id;
            //formState.driver.id      = response.driver_id;
        }

        //$scope.ride = new RideResourceFactory();
        //$scope.addRide = $scope.ride.$save;

        // UpdateDriver
        // ------------
        function updateDriver() {
            $log.info('updateDriver');
            var driverResource = new DriverResourceFactory;
            var params = {
                userId    : formState.user.id,
                rideId    : formState.ride.id
            };
            driverResource.$get(params)
                .then(function () {
                    angular.merge(driverResource, formState.ride);
                    driverResource.$update(params)
                        .then(updateUserSettingsResourceSuccess)
                        .catch(updateUserSettingsResourceError);
                });
        }

        function updateUserSettingsResourceError(reason) {
            $log.error('saveUserSettingsResourceError:', reason);
            DialogFactory
                .showItems('The user settings could not be updated!', reason.errors);
        }

        function updateUserSettingsResourceSuccess(response) {
            $log.log('saveUserSettingsResourceSuccess:', response);
            ToastFactory
                .show('User settings updated!');
        }
    }

})();
