/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('RidesCtrl', RidesCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RidesCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        // Custom
        'RideResourceFactory'
    ];

    function RidesCtrl(
        // Angular
        $log,
        $scope,
        // Custom
        RideResourceFactory
    ) {
        $log.info('RidesCtrl');

        //var users = UserResourceFactory.query(function() {
        //    console.log(users);
        //});

        $scope.rides = RideResourceFactory.query();

        //$scope.users = [];
        //$scope.loading = false;
        //
        //$scope.init = function() {
        //    $scope.loading = true;
        //    UserResourceFactory.get().success(function(data){
        //        $scope.users = data.data;
        //        $log.log('users: ', users);
        //        $scope.loading = false;
        //    });
        //}
    }

})();