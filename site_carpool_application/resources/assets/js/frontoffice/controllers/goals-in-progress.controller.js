/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('GoalsInProgressCtrl', GoalsInProgressCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalsInProgressCtrl.$inject = [
		// Angular
		'$log',
		// Custom
		'StorageFactory',
		'UserGoalResourceFactory'
	];

	function GoalsInProgressCtrl(
		// Angular
		$log,
		// Custom
		StorageFactory,
		UserGoalResourceFactory
	) {
		$log.info('GoalsInProgressCtrl');
		// ViewModel
		// =========
		var vm = this;

		vm.user  = StorageFactory.Local.getUserModel('user');
		vm.goals = loadUserGoal();

		// Functions
		// =========
		function loadUserGoal() {
			$log.info('loadUserGoal');
			var params = {
				userId: vm.user.id
			};
			var goals = UserGoalResourceFactory.queryInProgress(params);
			goals.$promise
				.then(loadUserGoalResourceSuccess)
				.catch(loadUserGoalResourceError);

			return goals;
		}

		function loadUserGoalResourceError(reason) {
			$log.error('loadUserGoalResourceError:', reason);
		}

		function loadUserGoalResourceSuccess(response) {
			$log.info('loadUserGoalResourceSuccess:', response);
		}
	}

})();
