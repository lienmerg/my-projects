/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.cssOutput   = elixir.config.publicDir + '/styles/css';   // Override default 'public/css'
elixir.config.jsOutput    = elixir.config.publicDir + '/scripts/js';   // Override default 'public/js'
elixir.config.fontsOutput = elixir.config.publicDir + '/styles/fonts'; // New
elixir.config.baseDir     = './';                                      // New
var bowerDir = 'vendor/bower_components';

var vendor = {
		angular			: bowerDir + '/angular/angular.js',
		angularModule	: {
			animate		: bowerDir + '/angular-animate/angular-animate.js',
			aria		: bowerDir + '/angular-aria/angular-aria.js',
			cookies		: bowerDir + '/angular-cookies/angular-cookies.js',
			material	: bowerDir + '/angular-material',
			messages	: bowerDir + '/angular-messages/angular-messages.js',
			resource	: bowerDir + '/angular-resource/angular-resource.js',
			route		: bowerDir + '/angular-route/angular-route.js',
			new_router	: bowerDir + '/angular-new-router/dist/router.es5.js',
			sanitize	: bowerDir + '/angular-sanitize/angular-sanitize.js',
			touch		: bowerDir + '/angular-touch/angular-touch.js'
		},
		bootstrap		: bowerDir + '/bootstrap-sass-official/assets',
		chartjs			: bowerDir + '/chartjs/Chart.min.js',
		fontawesome		: bowerDir + '/fontawesome',
		jquery			: bowerDir + '/jquery/dist',
		leaflet			: bowerDir + '/leaflet/dist',
		lodash			: bowerDir + '/lodash/lodash.min.js',
		moment			: bowerDir + '/moment/min/moment-with-locales.min.js'
	},
	options = {
		sass: {
			includePaths: [
				elixir.config.baseDir + vendor.bootstrap   + '/stylesheets/',
				elixir.config.baseDir + vendor.fontawesome + '/scss/'
			]
		}
	};

elixir(function (mix) {
	mix
		// AngularJS (https://angularjs.org) + AngularJS Modules
		.copy(
			vendor.angularModule.material + '/angular-material.css',
			elixir.config.cssOutput       + '/angular.css'
		)
		.scripts([
				vendor.angular,
				vendor.angularModule.animate,
				vendor.angularModule.aria,
				vendor.angularModule.material + '/angular-material.js',
				vendor.angularModule.messages,
				//vendor.angularModule.new_router,
				vendor.angularModule.route,
				vendor.angularModule.resource
			],
			elixir.config.jsOutput + '/angular.js',
			elixir.config.baseDir
		)

		// Chart.js (http://www.chartjs.org)
		.copy(
			vendor.chartjs,
			elixir.config.jsOutput + '/chart.js'
		)

		// Leaflet (http://leafletjs.com)
		.copy(
			vendor.leaflet          + '/leaflet.css',
			elixir.config.cssOutput + '/leaflet.css'
		)
		.copy(
			vendor.leaflet         + '/leaflet.js',
			elixir.config.jsOutput + '/leaflet.js'
		)

		// LoDash (https://lodash.com)
		.copy(
			vendor.lodash,
			elixir.config.jsOutput + '/lodash.js'
		)

		// Moment (http://momentjs.com)
		.copy(
			vendor.moment,
			elixir.config.jsOutput + '/moment.js'
		)

		// App
		.sass(
			'backoffice.scss',
			elixir.config.cssOutput + '/backoffice.scss',
			options.sass
		)
		.sass(
			'frontoffice.scss',
			elixir.config.cssOutput + '/frontoffice.css',
			options.sass
		)
		.sass(
			'styleguide.scss',
			elixir.config.cssOutput + '/styleguide.scss',
			options.sass
	)
		.copy(
			vendor.fontawesome + '/fonts',
			elixir.config.fontsOutput
		)
		.copy(
			vendor.bootstrap + '/fonts/bootstrap',
			elixir.config.fontsOutput
		)
		.copy(
			elixir.config.assetsDir + 'html',
			elixir.config.publicDir
		)
		.copy(
			elixir.config.assetsDir +  'images',
			elixir.config.publicDir + '/images'
		)
		.scripts([
				vendor.jquery    + '/jquery.js',
				vendor.bootstrap + '/javascripts/bootstrap.js'
			],
			elixir.config.jsOutput + '/backoffice.js',
			elixir.config.baseDir
		)
		.scriptsIn(
			elixir.config.assetsDir + '/js/frontoffice',
			elixir.config.jsOutput  + '/frontoffice.js'
		)
		.scriptsIn(
			elixir.config.assetsDir + '/js/styleguide',
			elixir.config.jsOutput  + '/styleguide.js'
		)
		//.phpUnit()
		//.phpSpec()
	;
});
