<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */

namespace StartMeUp\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
        'driverseats',
        'car'
    ];

    protected $table = 'drivers';

    /**
     * A driver can have many rides.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rides()
    {
        return $this->belongsToMany('Driver', 'ride_driver', 'driver_id', 'ride_id');
    }
}
