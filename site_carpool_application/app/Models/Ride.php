<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */

namespace StartMeUp\Models;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    const DEFAULT_RIDES = [
        [
            'ride_from' => 'Aalst',
            'ride_to' => 'Mariakerke',
            'ride_starttime' => '2015-07-31 09:00:00',
            'ride_endtime' => '2015-07-31 10:00:00',
            'ride_price' => '4',
        ],
        [
            'ride_from' => 'Brussel',
            'ride_to' => 'Mechelen',
            'ride_starttime' => '2015-08-01 09:00:00',
            'ride_endtime' => '2015-08-01 10:00:00',
            'ride_price' => '3',
        ],
        [
            'ride_from' => 'Aalst',
            'ride_to' => 'Leuven',
            'ride_starttime' => '2015-08-02 15:30:00',
            'ride_endtime' => '2015-08-02 16:00:00',
            'ride_price' => '2',
        ],
        [
            'ride_from' => 'Gent',
            'ride_to' => 'Genk',
            'ride_starttime' => '2015-08-06 09:00:00',
            'ride_endtime' => '2015-08-06 10:00:00',
            'ride_price' => '3',
        ],
        [
            'ride_from' => 'Brussel',
            'ride_to' => 'Hasselt',
            'ride_starttime' => '2015-08-15 09:15:00',
            'ride_endtime' => '2015-08-15 09:30:00',
            'ride_price' => '1',
        ],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ride_from',
        'ride_to',
        'ride_starttime',
        'ride_endtime',
        'ride_price',
    ];

    protected $table = 'rides';


    /**
     * A ride is owned by a driver.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo('StartMeUp\Models\Driver');
    }
}
