<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */

namespace StartMeUp\Http\Controllers\Api;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter as FractalPaginatorAdapter;
use League\Fractal\Resource as FractalResource;
use StartMeUp\Jobs\AddDefaultRidesForNewDriverJob;
use StartMeUp\Jobs\SaveDriverRidesJob;
use StartMeUp\Http\Requests\StoreDriverRequest;
use StartMeUp\Http\Requests\UpdateDriverRequest;
use StartMeUp\Repositories\Eloquent\RidesRepository;
use StartMeUp\Repositories\Eloquent\DriversRepository;
use StartMeUp\Transformers\GenericTransformer;
use StartMeUp\Transformers\IdOnlyTransformer;
use StartMeUp\Models\Driver;
use Validator;

class DriversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Laravel Eloquent
        list($drivers, $paginator) = $this->_getCollectionAndPaginator(new DriversRepository());

        // Fractal
        $resource = new FractalResource\Collection($drivers, new GenericTransformer());
        $resource->setPaginator(new FractalPaginatorAdapter($paginator));

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $driverData = $this->_getRequestData($request, 'driver');
        $rules = $this->_getValidationRules(new StoreDriverRequest());

        $validator = Validator::make($driverData, $rules);
        if ($validator->fails()) {
            return response()
                ->json($this->_getResponseDataValidatorError($validator))
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $driver = new Driver($driverData);

        if ($driver->save()) {

//            $this->dispatch(new AddDefaultRidesForNewDriverJob($driver));
//
//            if (is_array($driverData['rides'])) {
//                $this->dispatch(new SaveDriverRidesJob($driver, $driverData['rides']));
//            }

            $resource = new FractalResource\Item($driver, new IdOnlyTransformer());
            return response()
                ->json($this->_getResponseData($resource))
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $driversRepository = new DriversRepository();
        $driver = $driversRepository->find($id);
        if (!$driver) {
            return response()
                ->json($this->_getResponseDataMessageEntityDoesNotExistError('Driver', $id))
                ->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        // Fractal
        $resource = new FractalResource\Item($driver, new GenericTransformer());

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $driverData = $this->_getRequestData($request);
        $rules = $this->_getValidationRules(new UpdateDriverRequest());

        $validator = Validator::make($driverData, $rules);
        if ($validator->fails()) {
            return response()
                ->json($this->_getResponseDataValidatorError($validator))
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $driversRepository = new DriversRepository();
        $driver = $driversRepository->find($id);
        if (!$driver) {
            return response()
                ->json($this->_getResponseDataMessageEntityDoesNotExistError('Driver', $id))
                ->setStatusCode(Response::HTTP_NOT_FOUND);
        }
        $driver->fill($driverData); // Update Driver model's data.

        if (isset($driverData['ride_id'])) {
            $ridesRepository = new RidesRepository();
            $ride = $ridesRepository->find($driverData['ride_id']);
            if ($ride) {
                $driver->ride()->associate($ride);
            }
        }

        $driver->save(); // Persist Driver model.

        // Fractal
        $resource = new FractalResource\Item($driver, new GenericTransformer());

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
