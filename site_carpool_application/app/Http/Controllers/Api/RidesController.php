<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */

namespace StartMeUp\Http\Controllers\Api;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter as FractalPaginatorAdapter;
use League\Fractal\Resource as FractalResource;
use StartMeUp\Http\Requests\StoreRideRequest;
use StartMeUp\Http\Requests\UpdateRideRequest;
use StartMeUp\Repositories\Eloquent\RidesRepository;
use StartMeUp\Repositories\Eloquent\DriversRepository;
use StartMeUp\Transformers\GenericTransformer;
use StartMeUp\Transformers\IdOnlyTransformer;
use StartMeUp\Models\Ride;
use Validator;

class RidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Laravel Eloquent
        list($rides, $paginator) = $this->_getCollectionAndPaginator(new RidesRepository());

        // Fractal
        $resource = new FractalResource\Collection($rides, new GenericTransformer());
        $resource->setPaginator(new FractalPaginatorAdapter($paginator));

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rideData = $this->_getRequestData($request, 'ride');
        $rules = $this->_getValidationRules(new StoreRideRequest());

        $validator = Validator::make($rideData, $rules);
        if ($validator->fails()) {
            return response()
                ->json($this->_getResponseDataValidatorError($validator))
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $ride = new Ride($rideData);
        if ($ride->save()) {

            $resource = new FractalResource\Item($ride, new IdOnlyTransformer());
            return response()
                ->json($this->_getResponseData($resource))
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $ridesRepository = new RidesRepository();
        $ride = $ridesRepository->find($id);
        if (!$ride) {
            return response()
                ->json($this->_getResponseDataMessageEntityDoesNotExistError('Ride', $id))
                ->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        // Fractal
        $resource = new FractalResource\Item($ride, new GenericTransformer());

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rideData = $this->_getRequestData($request);
        $rules = $this->_getValidationRules(new UpdateRideRequest());

        $validator = Validator::make($rideData, $rules);
        if ($validator->fails()) {
            return response()
                ->json($this->_getResponseDataValidatorError($validator))
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $ridesRepository = new RidesRepository();
        $ride = $ridesRepository->find($id);
        if (!$ride) {
            return response()
                ->json($this->_getResponseDataMessageEntityDoesNotExistError('Ride', $id))
                ->setStatusCode(Response::HTTP_NOT_FOUND);
        }
        $ride->fill($rideData); // Update Ride model's data.

        if (isset($rideData['id'])) {
            $driversRepository = new DriversRepository();
            $driver = $driversRepository->find($rideData['id']);
            if ($driver) {
                $ride->driver()->associate($driver);
            }
        }

        $ride->save(); // Persist Ride model.

        // Fractal
        $resource = new FractalResource\Item($ride, new GenericTransformer());

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
