<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */

namespace StartMeUp\Repositories\Eloquent;

use StartMeUp\Contracts\Repositories\RidesContract;
use StartMeUp\Models\Ride;

class RidesRepository extends Repository implements RidesContract
{
    protected $filtersValid = [];

    protected $includesValid = [];

    protected $sortsValid = [
        'id'
    ];

    /**
     * @param array $additionalInput
     */
    public function __construct(array $additionalInput = [])
    {
        $this->model = new Ride();
        $this->query = $this->model->query();
        parent::__construct($additionalInput);
    }

    public function applyFilters()
    {
        //
    }
}