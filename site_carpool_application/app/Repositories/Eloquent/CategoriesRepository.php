<?php

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

namespace StartMeUp\Repositories\Eloquent;

use CreateUsersTable;
use StartMeUp\Contracts\Repositories\CategoriesContract;
use StartMeUp\Models\Category;

class CategoriesRepository extends Repository implements CategoriesContract
{
    protected $filtersValid = [
        CreateUsersTable::TABLE,
    ];

    protected $includesValid = [
        'goals',
        'goals.target',
    ];

    protected $sortsValid = [
        'order',
    ];

    /**
     * @param array $additionalInput
     */
    public function __construct(array $additionalInput = [])
    {
        $this->model = new Category();
        $this->query = $this->model->query();
        parent::__construct($additionalInput);
    }

    public function applyFilters()
    {
        foreach ($this->filters as $filter => $value) {
            switch ($filter) {
                case CreateUsersTable::TABLE: // filter[users]=1
                    $this->model = $this->model->where(CreateUsersTable::FK, $value);
                    break;
                default:
                    break;
            }
        }
    }
}
