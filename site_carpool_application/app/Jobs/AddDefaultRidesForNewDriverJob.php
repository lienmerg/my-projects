<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */

namespace StartMeUp\Jobs;

use Exception;
use Illuminate\Contracts\Bus\SelfHandling;
use StartMeUp\Models\Driver;
use StartMeUp\Models\Ride;

class AddDefaultRidesForNewDriverJob extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     * @param Driver $driver
     */
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rideOrder = 0;
        foreach (Ride::DEFAULT_RIDES as $rideData) {
            $ride = Ride::create($rideData);
            $ride->order = $rideOrder++;
            $ride->driver()->associate($this->driver);
            $ride->save();
        }
    }
}
