<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */

namespace StartMeUp\Jobs;

use Illuminate\Contracts\Bus\SelfHandling;
use StartMeUp\Models\Ride;
use StartMeUp\Models\Driver;


class SaveDriverRidesJob extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     * @param Driver  $driver
     * @param array $rides
     */
    public function __construct(Driver $driver, array $rides = [])
    {
        $this->driver = $driver;
        $this->rides = $rides;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rides = [];
        foreach ($this->rides as $rideData) {
            if (isset($rideData['id'])) {
                $ride = Ride::find($rideData['id']);
                if ($ride) {
                    array_push($rides, $ride);
                }
            }
        }

        if (!empty($rides)) {
            $this->driver->rides()->saveMany($rides);
        }
    }
}
