<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversUsersTable extends Migration
{
    const TABLE = 'drivers_users';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers_users', function (Blueprint $table) {
            $table->integer('driver_id')
                ->unsigned();
            $table->integer('user_id')
                ->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->foreign('driver_id')
                ->references('id')
                ->on('users');

            $table->primary(array('user_id', 'driver_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drivers_users');
    }
}
