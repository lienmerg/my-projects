<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    const TABLE = 'drivers';

    const PK = 'id';

    const FK = 'driver_id';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('role');
            $table->tinyInteger('driverseats');
            $table->string('car');
            $table->timestamps();

            // Foreign Keys
            $table->unsignedInteger(CreateUsersTable::FK)
                ->nullable();
            $table->foreign(CreateUsersTable::FK)
                ->references(CreateUsersTable::PK)
                ->on(CreateUsersTable::TABLE)
                ->onDelete('cascade');

            $table->unsignedInteger(CreateRidesTable::FK)
                ->nullable();
            $table->foreign(CreateRidesTable::FK)
                ->references(CreateRidesTable::PK)
                ->on(CreateRidesTable::TABLE)
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drivers');
    }
}
