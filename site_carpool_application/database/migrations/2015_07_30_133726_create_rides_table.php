<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    const TABLE = 'rides';
    const PK = 'id';

    const FK = 'ride_id';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->increments(self::PK);
            $table->text('ride_from');
            $table->text('ride_to');
            $table->dateTime('ride_starttime');
            $table->dateTime('ride_endtime');
            $table->tinyInteger('ride_price');
            $table->text('ride_comment')
                ->nullable();
            $table->timestamps();

            // Foreign Keys
            $table->unsignedInteger(CreateUsersTable::FK)
                ->nullable();

            $table->foreign(CreateUsersTable::FK)
                ->references(CreateUsersTable::PK)
                ->on(CreateUsersTable::TABLE)
                ->onDelete('cascade');

            $table->unsignedInteger(CreateLocalitiesTable::FK)
                ->nullable();


            $table->foreign(CreateLocalitiesTable::FK)
                ->references(CreateLocalitiesTable::PK)
                ->on(CreateLocalitiesTable::TABLE)
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rides');
    }
}
