<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRideDriverTable extends Migration
{
    const TABLE = 'ride_driver';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ride_driver', function (Blueprint $table) {
            $table->integer('ride_id')
                ->unsigned();
            $table->integer('driver_id')
                ->unsigned();

            $table->foreign('driver_id')
                ->references('id')
                ->on('users');
            $table->foreign('ride_id')
                ->references('id')
                ->on('users');

            $table->primary(array('driver_id', 'ride_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ride_driver');
    }
}
