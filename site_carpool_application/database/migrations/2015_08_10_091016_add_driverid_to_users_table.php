<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriveridToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            // Foreign Key
            $table->unsignedInteger(CreateDriversTable::FK)
                ->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            // Foreign Key
            $table->foreign(CreateDriversTable::FK)
                ->references(CreateDriversTable::PK)
                ->on(CreateDriversTable::TABLE);
        });
    }
}
