<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */


use StartMeUp\User;
use StartMeUp\Models\Driver;

class DriverUserTableSeeder extends StartMeUpSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(CreateDriversUsersTable::TABLE)->delete();

        $drivers = Driver::all();
        $users = User::all();

        foreach ($drivers as $driver) {
            foreach ($users as $user) {
                $driver->users()->attach($user);
                $driver->save();
            }
        }
    }
}
