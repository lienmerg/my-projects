<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */


use StartMeUp\Models\Driver;
use StartMeUp\Models\Ride;

class RideDriverTableSeeder extends StartMeUpSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(CreateRideDriverTable::TABLE)->delete();

        $rides = Ride::all();
        $drivers = Driver::all();

        foreach ($rides as $ride) {
            foreach ($drivers as $driver) {
                $ride->drivers()->attach($driver);
                $ride->save();
            }
        }
    }
}
