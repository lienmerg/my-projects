<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */


use StartMeUp\User;
use StartMeUp\Models\Ride;

class RideUserTableSeeder extends StartMeUpSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(CreateRideUserTable::TABLE)->delete();

        $rides = Ride::all();
        $users = User::all();

        foreach ($rides as $ride) {
            foreach ($users as $user) {
                $ride->users()->attach($user);
                $ride->save();
            }
        }
    }
}
