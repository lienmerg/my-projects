<?php

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

use StartMeUp\User;

class UserTableSeeder extends StartMeUpSeeder
{
    public function run()
    {
        DB::table(CreateUsersTable::TABLE)->delete();

        // Create a test user
        User::create([
            'email' => 'traftrav_user@arteveldehs.be',
            'name' => 'traftrav_user',
            'password' => Hash::make('traftrav_password'),
            'given_name' => 'TrafTrav',
            'family_name' => 'User',
        ]);

        // Faker
        // -----
        factory(User::class, self::$maxItems)->create();
    }
}
