<?php

/** @author Lien Mergan
 *  Bachelor Graphical and Digital Media
 *  @copyright Arteveldehogeschool - Academic year 2014-2015
 */

use StartMeUp\Models\Ride;

class RideTableSeeder extends StartMeUpSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(CreateRidesTable::TABLE)->delete();

        // Faker
        // -----
        factory(Ride::class, self::$maxItems)->create();
    }
}
