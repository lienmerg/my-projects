/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	// StartMeUpBuddy.io application
	// -----------------------------

	angular.module('smuApplication', [
		// Angular module dependencies
		'ngAnimate',
		'ngMaterial',
		'ngMessages',
		//'ngNewRouter',
		'ngResource',
		'ngRoute',

		// StartMeUpBuddy.io module dependencies
		'smuControllers',
		'smuDirectives',
		'smuFilters',
		'smuServices'
	]);

	// StartMeUpBuddy.io module declarations
	// -------------------------------------

	angular.module('smuControllers', []);
	angular.module('smuDirectives' , []);
	angular.module('smuFilters'    , []);
	angular.module('smuServices'   , []);

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuApplication')
		.config(Config);

	// Inject dependencies into constructor (needed when JS minification is applied).
	Config.$inject = [
		// Angular
		'$compileProvider',
		'$resourceProvider',
		// Angular Material Design
		'$mdThemingProvider'
	];

	function Config(
		// Angular
		$compileProvider,
		$resourceProvider,
		// Angular Material Design
		$mdThemingProvider
	) {
		$compileProvider.debugInfoEnabled(true); // Set to `false` for production

		$resourceProvider.defaults.actions.query.isArray = false; // Allow { 'data': [ … ] } rather than [ … ]
		//console.info($resourceProvider.defaults.actions);

		// @link: http://www.google.com/design/spec/style/color.html
		$mdThemingProvider.theme('default').
			primaryPalette('brown', {
				'default':  '50',
				'hue-1':    '50',
				'hue-2':   '600',
				'hue-3':  'A100'
			}).
			accentPalette('teal', {
				'default': '500',
				'hue-1':    '50',
				'hue-2':   '600',
				'hue-3':  'A100'
			}).
			warnPalette('red');
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuApplication')
		.constant('INCREMENT_TYPES', {
			a_QuarterHour: {
				value: 'QUARTER_HOUR',
				label: 'Quarter hour',
				minutes: 15
			},
			b_Hour: {
				value: 'HOUR',
				label: 'Hour',
				minutes: 60
			},
			c_Day: {
				value: 'DAY',
				label: 'Day',
				minutes: 480
			}
		})

		.constant('MOOD_TYPES', {
			a_FeelingEnergized: {
				value: 'ENERGIZED',
				label: 'Energized'
			},
			b_FeelingGood: {
				value: 'GOOD',
				label: 'Good'
			},
			c_FeelingOk: {
				value: 'OK',
				label: 'Ok'
			},
			d_FeelingTired: {
				value: 'TIRED',
				label: 'Tired'
			},
			e_FeelingExhausted: {
				value: 'EXHAUSTED',
				label: 'Exhausted'
			}
		})

		.constant('PRIORITY_TYPES', {
			a_Highest: {
				value: 'HIGHEST',
				label: 'Highest priority'
			},
			b_High: {
				value: 'HIGH',
				label: 'High priority'
			},
			c_Normal: {
				value: 'NORMAL',
				label: 'Normal priority'
			},
			d_Low: {
				value: 'LOW',
				label: 'Low priority'
			},
			e_Lowest: {
				value: 'LOWEST',
				lable: 'Lowest priority'
			}
		})

		.constant('REPEAT_TYPES', {
			a_Daily: {
				value: 'DAILY',
				label: 'Every day'
			},
			b_Weekly: {
				value: 'WEEKLY',
				label: 'Every week'
			},
			c_Fortnightly: {
				value: 'FORTNIGHTLY',
				label: 'Every 2 weeks'
			},
			d_Monthly: {
				value: 'MONTHLY',
				label: 'Every month'
			}
		})

		.constant('TARGET_TYPES', {
			a_Checkbox: {
				value: 'TargetCheckbox',
				label: 'Checkbox'
			},
			b_RecurringCheckbox: {
				value: 'TargetRecurringCheckbox',
				label: 'Recurring Checkbox'
			},
			c_Duration: {
				value: 'TargetDuration',
				label: 'Duration'
			}
		})

		.constant('configApi', {
			protocol: null,
			host    : null,
			path    : '/api/v1/'
		})

		.constant('configChart', {
			bar: {
				//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
				scaleBeginAtZero : true,

				//Boolean - Whether grid lines are shown across the chart
				scaleShowGridLines : true,

				//String - Colour of the grid lines
				scaleGridLineColor : "rgba(0,0,0,.05)",

				//Number - Width of the grid lines
				scaleGridLineWidth : 1,

				//Boolean - Whether to show horizontal lines (except X axis)
				scaleShowHorizontalLines: true,

				//Boolean - Whether to show vertical lines (except Y axis)
				scaleShowVerticalLines: true,

				//Boolean - If there is a stroke on each bar
				barShowStroke : true,

				//Number - Pixel width of the bar stroke
				barStrokeWidth : 2,

				//Number - Spacing between each of the X value sets
				barValueSpacing : 5,

				//Number - Spacing between data sets within X values
				barDatasetSpacing : 1,

				//String - A legend template
				legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
			},
			line: {

				//Boolean - Whether grid lines are shown across the chart
				scaleShowGridLines : true,

				//String - Colour of the grid lines
				scaleGridLineColor : "rgba(0,0,0,.05)",

				//Number - Width of the grid lines
				scaleGridLineWidth : 1,

				//Boolean - Whether to show horizontal lines (except X axis)
				scaleShowHorizontalLines: true,

				//Boolean - Whether to show vertical lines (except Y axis)
				scaleShowVerticalLines: true,

				//Boolean - Whether the line is curved between points
				bezierCurve : true,

				//Number - Tension of the bezier curve between points
				bezierCurveTension : 0.4,

				//Boolean - Whether to show a dot for each point
				pointDot : true,

				//Number - Radius of each point dot in pixels
				pointDotRadius : 4,

				//Number - Pixel width of point dot stroke
				pointDotStrokeWidth : 1,

				//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
				pointHitDetectionRadius : 20,

				//Boolean - Whether to show a stroke for datasets
				datasetStroke : true,

				//Number - Pixel width of dataset stroke
				datasetStrokeWidth : 2,

				//Boolean - Whether to fill the dataset with a colour
				datasetFill : true,

				//String - A legend template
				legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
			}
		})

		.constant('configMap', {
			tile: {
				urlTemplate: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
				options: {
					attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
				}
			},
			icon: {
				company: {
					iconUrl     : '/images/nearby/icon.svg',
					iconSize    : [50,  50], // size of the icon
					iconAnchor  : [50,  50], // point of the icon which will correspond to marker's location
					popupAnchor : [ 0, -25], // point from which the popup should open relative to the iconAnchor
					shadowUrl   : '/images/nearby/icon-shadow.svg',
					shadowSize  : [50, 50], // size of the shadow
					shadowAnchor: [52, 50]  // the same for the shadow
				},
				user: {
					iconUrl    : '/images/nearby/icon-user.svg',
					iconSize   : [50,  50], // size of the icon
					iconAnchor : [25,  48], // point of the icon which will correspond to marker's location
					popupAnchor: [ 0, -48]  // point from which the popup should open relative to the iconAnchor
				}
			}
		});

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuApplication')
		.config(Routes)
		.run(Preloads);

	// Routes
	// ------
	// Inject dependencies into constructor (needed when JS minification is applied).
	Routes.$inject = [
		// Angular
		'$routeProvider'
	];

	function Routes(
		// Angular
		$routeProvider
	) {
		$routeProvider
			// Gamification
			.when('/gamification', {
				templateUrl: _getView('gamification/shoot')
			})
			.when('/gamification/leaderboard', {
				templateUrl: _getView('gamification/leaderboard')
			})
			.when('/gamification/trophies', {
				templateUrl: _getView('gamification/trophies')
			})

			// Goals
			.when('/goals', {
				templateUrl: _getView('goals/goals-overview'),
				controller: 'GoalsCtrl',
				controllerAs: 'vm'
			})
			.when('/goals/category/:categoryId/edit', {
				templateUrl: _getView('goals/category-edit'),
				controller: 'CategoryEditCtrl',
				controllerAs: 'vm'
			})
			.when('/goals/category/create', {
				templateUrl: _getView('goals/category-create'),
				controller: 'CategoryCreateCtrl',
				controllerAs: 'vm'
			})
			.when('/goals/category/:categoryId/goal/create', {
				templateUrl: _getView('goals/goal-create'),
				controller: 'GoalCreateCtrl',
				controllerAs: 'vm'
			})
			.when('/goals/category/:categoryId/goal/:goalId/edit', {
				templateUrl: _getView('goals/goal-edit'),
				controller: 'GoalEditCtrl',
				controllerAs: 'vm'
			})
			.when('/goals/category/:categoryId/goal/:goalId/target/update/create', {
				templateUrl: _getView('goals/goal-update'),
				controller: 'GoalUpdateCtrl',
				controllerAs: 'vm'
			})
			.when('/goals/category/:categoryId/goal/:goalId/progress', {
				templateUrl: _getView('goals/goal-progress'),
				controller: 'GoalProgressCtrl',
				controllerAs: 'vm'
			})
			.when('/goals/in-progress', {
				templateUrl: _getView('goals/goals-in-progress'),
				controller: 'GoalsInProgressCtrl',
				controllerAs: 'vm'
			})
			.when('/goals/progress', {
				templateUrl: _getView('goals/goals-progress'),
				controller: 'GoalsProgressCtrl',
				controllerAs: 'vm'
			})

			// Moods
			.when('/moods', {
				templateUrl: _getView('moods/mood-update'),
				controller: 'MoodUpdateCtrl',
				controllerAs: 'vm'
			})
			.when('/moods/statistics', {
				templateUrl: _getView('moods/mood-statistics'),
				controller: 'MoodStatisticsCtrl',
				controllerAs: 'vm'
			})

			// Nearby
			.when('/nearby', {
				templateUrl: _getView('nearby/map'),
				controller: 'NearbyCtrl',
				controllerAs: 'vm'
			})

			// Registration
			.when('/registration/step/1/of/4', {
				templateUrl: _getView('registration/step-1-user-account'),
				controller: 'RegistrationStep1Ctrl',
				controllerAs: 'vm'
			})
			.when('/registration/step/2/of/4', {
				templateUrl: _getView('registration/step-2-personal-profile'),
				controller: 'RegistrationStep2Ctrl',
				controllerAs: 'vm'
			})
			.when('/registration/step/3/of/4', {
				templateUrl: _getView('registration/step-3a-driver-profile'),
				controller: 'RegistrationStep3Ctrl',
				controllerAs: 'vm'
			})
			.when('/registration/step/4/of/4', {
				templateUrl: _getView('registration/step-4-completed'),
				controller: 'RegistrationStep4Ctrl',
				controllerAs: 'vm'
			})

            //Users
            .when('/users', {
                templateUrl: _getView('users/users-overview'),
                controller: 'UsersCtrl',
                controllerAs: 'vm'
            })
            .when('/users/update', {
                templateUrl: _getView('users/user-detailview'),
                controller: 'UserUpdateCtrl',
                controllerAs: 'vm'
            })

			//Drivers
            .when ('/drivers', {
                templateUrl: _getView('drivers/drivers-overview'),
                controller: 'DriversCtrl',
                controllerAs: 'vm'
            })

            //Rides
            .when('/rides', {
                templateUrl: _getView('rides/rides-overview'),
                controller: 'RidesCtrl',
                controllerAs: 'vm'
            })

            .when('/rides/create', {
                templateUrl: _getView('rides/ride-create'),
                controller: 'RideCreateCtrl',
                controllerAs: 'vm'
            })

			// Settings
			.when('/settings', {
				templateUrl: _getView('settings/menu'),
				controller: 'SettingsCtrl',
				controllerAs: 'vm'
			})

			// Log In
			.when('/log-in', {
				templateUrl: _getView('log-in'),
				controller: 'LogInCtrl',
				controllerAs: 'vm'
			})

			// Splash Screen
			.when('/splash', {
				templateUrl: _getView('splash')
			})

			// Default
			.otherwise({
				redirectTo: '/splash'
			});
	}

	// Preload Templates
	// -----------------
	Preloads.$inject = [
		// Angular
		'$http',
		'$templateCache'
	];

	function Preloads(
		// Angular
		$http,
		$templateCache
	) {
		var partials = [
				'validation-messages'
			],
			views = [
				'registration/step-1-user-account',
				'registration/step-2-personal-profile',
				'registration/step-3-company-profile',
				'registration/step-4-completed'
			];

		partials.forEach(function (template) {
			$http.get(_getPartialView(template), { cache: $templateCache });
		});

		views.forEach(function (template) {
			$http.get(_getView(template), { cache: $templateCache });
		});
	}

	/**
	 *
	 * @param uri
	 * @returns {string}
	 * @private
	 */
	function _getPartialView(uri) {
		return '/templates/' + uri + '.partial.html';
	}

	/**
	 *
	 * @param uri
	 * @returns {string}
	 * @private
	 */
	function _getView(uri) {
		return '/templates/' + uri + '.view.html';
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
	'use strict';

	angular.module('smuDirectives')
		.directive('smuMatchModel', smuMatchModel);

	// Inject dependencies into constructor (needed when JS minification is applied).
	smuMatchModel.$inject = [
		// Angular
		'$parse'
	];

	function smuMatchModel(
		// Angular
		$parse
	) {

		return {
			require: '^ngModel', // ngModel is required
			restrict: 'A',       // Attribute directive only
			link: function (scope, element, attrs, NgModelController) {

				var targetFunction = $parse(attrs.smuMatchModel);
				function targetValue() {
					return targetFunction(scope);
				}

				// Add new validator
				NgModelController.$validators.smuMatchModel = function () {
					return NgModelController.$viewValue === targetValue();
				};

				// Run all validators when targetValue changes.
				scope.$watch(targetValue, function () {
					NgModelController.$$parseAndValidate();
				});

			}

		};

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('DialogFactory', DialogFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	DialogFactory.$inject = [
		// Angular Material Design
		'$mdDialog',
		// Custom
		'UriFactory'
	];

	function DialogFactory(
		// Angular Material Design
		$mdDialog,
		// Custom
		UriFactory
	) {
		function show(title, message) {
			var config = {
				bindToController: true,
				controller: 'DialogCtrl',
				controllerAs: 'vm',
				locals: {
					messages: message,
					title  : title
				},
				templateUrl: UriFactory.getDialog('message')

			};

			return $mdDialog.show(config);
		}

		function showItems(title, items) {
			var config = {
				bindToController: true,
				controller: 'DialogCtrl',
				controllerAs: 'vm',
				locals: {
					items: items,
					title: title
				},
				templateUrl: UriFactory.getDialog('items')

			};

			return $mdDialog.show(config);
		}

		return {
			show     : show,
			showItems: showItems
		};

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('StorageFactory', StorageFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	StorageFactory.$inject = [
		// Angular
		'$log',
		'$window',
		// Custom
		'CategoryModelFactory',
		'UserModelFactory'
	];

	function StorageFactory(
		// Angular
		$log,
		$window,
		// Custom
		CategoryModelFactory,
		UserModelFactory
	) {
		function get(key) {
			var value = $window.localStorage.getItem(key);
			//$log.info('get `' + key + '`: ', value);
			return value;
		}

		function getCategoryModel(key) {
			var object = getObject(key);
			if (angular.isArray(object)) {
				return object.map(function (value) {
					return new CategoryModelFactory(value);
				})
			} else {
				return new CategoryModelFactory(object);
			}
		}

		function getObject(key) {
			var objectJson = get(key);

			return angular.fromJson(objectJson);
		}

		function getUserModel(key) {
			var object = getObject(key);

			return new UserModelFactory(object);
		}

		function has(key) {
			return $window.localStorage.hasOwnProperty(key);
		}

		function remove(key) {
			$window.localStorage.removeItem(key);
		}

		function set(key, value) {
			//$log.info('set `' + key + '`: ', value);
			$window.localStorage.setItem(key, value);
		}

		function setObject(key, object) {
			var objectJson = angular.toJson(object);
			this.set(key, objectJson);
		}

		return {
			Local: {
				get             : get,
				getCategoryModel: getCategoryModel,
				getObject       : getObject,
				getUserModel    : getUserModel,
				has             : has,
				remove          : remove,
				set             : set,
				setObject       : setObject
			}
		};
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('ToastFactory', ToastFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	ToastFactory.$inject = [
		// Angular Material Design
		'$mdToast',
		// Custom
		'UriFactory'
	];

	function ToastFactory(
		// Angular Material Design
		$mdToast,
		// Custom
		UriFactory
	) {
		function show(message) {
			var config = {
				bindToController: true,
				controller: 'ToastCtrl',
				controllerAs: 'vm',
				//hideDelay: 3000,
				locals: {
					message: message
				},
				//position: 'bottom left',
				templateUrl: UriFactory.getToast('message')

			};

			return $mdToast.show(config);
		}

		function showSimple(message) {
			var toast = $mdToast
				.simple()
				.content(message);

			return $mdToast.show(toast);
		}

		return {
			show: show,
			showSimple: showSimple
		};

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UriFactory', UriFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UriFactory.$inject = [
		// Angular
		'$location',
		// Custom
		'configApi'
	];

	function UriFactory(
		// Angular
		$location,
		// Custom
		configApi
	) {

		function getApi(path) {
			var protocol = configApi.protocol ? configApi.protocol : $location.protocol();
			var host     = configApi.host     ? configApi.host     : $location.host();
			var uri      = protocol + '://' + host + configApi.path + path;

			return uri;
		}

		function getDialog(uri) {
			return '/templates/' + uri + '.dialog.html';
		}

		function getPartialView(uri) {
			return '/templates/' + uri + '.partial.html';
		}

		function getToast(uri) {
			return '/templates/' + uri + '.toast.html';
		}

		function getView(uri) {
			return '/templates/' + uri + '.view.html';
		}

		return {
			getApi        : getApi,
			getDialog     : getDialog,
			getPartialView: getPartialView,
			getToast      : getToast,
			getView       : getView
		};

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('CategoryCreateCtrl', CategoryCreateCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	CategoryCreateCtrl.$inject = [
		// Angular
		'$log',
		// Custom
		'CategoryResourceFactory',
		'StorageFactory'
	];

	function CategoryCreateCtrl(
		// Angular
		$log,
		// Custom
		CategoryResourceFactory,
		StorageFactory
	) {
		$log.info('CategoryCreateCtrl');
		// ViewModel
		// =========
		var vm = this;

		// @todo Implement create Category.

		// Functions
		// =========

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('CategoryEditCtrl', CategoryEditCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	CategoryEditCtrl.$inject = [
		// Angular
		'$log',
		// Custom
		'CategoryResourceFactory',
		'StorageFactory'
	];

	function CategoryEditCtrl(
		// Angular
		$log,
		// Custom
		CategoryResourceFactory,
		StorageFactory
	) {
		$log.info('CategoryEditCtrl');
		// ViewModel
		// =========
		var vm = this;

		// @todo Implement edit Category.

		// Functions
		// =========

	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('DriversCtrl', DriversCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DriversCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        // Custom
        'DriverResourceFactory',
        'StorageFactory',
        'DialogFactory'
    ];

    function DriversCtrl(
        // Angular
        $log,
        $scope,
        // Custom
        DriverResourceFactory,
        StorageFactory,
        DialogFactory
    ) {
        $log.info('DriversCtrl');

        $scope.drivers = DriverResourceFactory.query();

    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('GoalCreateCtrl', GoalCreateCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalCreateCtrl.$inject = [
		// Angular
		'$log',
		'$routeParams',
		'$scope',
		'$window',
		// Constants
		'INCREMENT_TYPES',
		'REPEAT_TYPES',
		'TARGET_TYPES',
		// Custom
		'CategoryModelFactory',
		'DialogFactory',
		'GoalModelFactory',
		'GoalResourceFactory',
		'StorageFactory'
	];

	function GoalCreateCtrl(
		// Angular
		$log,
		$routeParams,
		$scope,
		$window,
		// Constants
		INCREMENT_TYPES,
		REPEAT_TYPES,
		TARGET_TYPES,
		// Custom
		CategoryModelFactory,
		DialogFactory,
		GoalModelFactory,
		GoalResourceFactory,
		StorageFactory
	) {
		$log.info('GoalCreateCtrl');
		// ViewModel
		// =========
		var vm = this;

		vm.category = new CategoryModelFactory({ id: $routeParams.categoryId })
		vm.goal     = new GoalModelFactory();
		vm.user     = StorageFactory.Local.getUserModel('user');

		// User Interface variables
		vm.$$INCREMENT_TYPES = INCREMENT_TYPES;
		vm.$$REPEAT_TYPES    = REPEAT_TYPES;
		vm.$$TARGET_TYPES    = TARGET_TYPES;

		vm.save = save;

		// Watchers
		// --------
		$scope.$watch('vm.goal.target_class'         , watchTargetClassHandler);
		$scope.$watch('vm.goal.target.time_estimated', watchTargetDurationHandler);
		$scope.$watch('vm.goal.target.time_increment', watchTargetDurationHandler);

		// Functions
		// =========
		function save(event) {
			event.preventDefault();

			vm.goal.user_id     = vm.user.id;
			vm.goal.category_id = vm.category.id;

			$log.info(vm.goal);

			saveGoal();
		}

		// Goal
		// ----
		function saveGoal() {
			var goalResource = new GoalResourceFactory();
			goalResource.goal = vm.goal;
			goalResource.$save()
				.then(saveGoalResourceSuccess)
				.catch(saveGoalResourceError);
		}

		function saveGoalResourceError(reason) {
			$log.error('saveGoalResourceError:', reason);
			DialogFactory
				.showItems('The goal could not be saved!', reason.errors);
		}

		function saveGoalResourceSuccess(response) {
			$log.log('saveGoalResourceSuccess:', response);
			$window.location.href = '#/goals';
		}

		// Watch Handlers
		// --------------
		function watchTargetClassHandler(newValue) {
			vm.goal.$$addTarget(newValue);
		}

		function watchTargetDurationHandler() {
			if (vm.goal.$$hasTargetTypeDuration()) {
				vm.goal.target.$$updateDuration();
			}
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('GoalEditCtrl', GoalEditCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalEditCtrl.$inject = [
		// Angular
		'$log',
		'$routeParams',
		'$scope',
		'$window',
		// Constants
		'INCREMENT_TYPES',
		'REPEAT_TYPES',
		// Custom
		'GoalResourceFactory',
		'StorageFactory',
		'TargetCheckboxResourceFactory',
		'TargetDurationResourceFactory',
		'TargetRecurringCheckboxResourceFactory'
	];

	function GoalEditCtrl(
		// Angular
		$log,
		$routeParams,
		$scope,
		$window,
		// Constants
		INCREMENT_TYPES,
		REPEAT_TYPES,
		// Custom
		GoalResourceFactory,
		StorageFactory,
		TargetCheckboxResourceFactory,
		TargetDurationResourceFactory,
		TargetRecurringCheckboxResourceFactory

	) {
		$log.info('GoalEditCtrl');
		// ViewModel
		// =========
		var vm = this;

		vm.categories       = StorageFactory.Local.getCategoryModel('categories');
		vm.user             = StorageFactory.Local.getUserModel('user');
		vm.category         = _.find(vm.categories, isCurrentCategoryFilter);
		vm.goal             = _.find(vm.category.goals, isCurrentGoalFilter);
		vm.goal.category_id = vm.category.id;
		vm.goal.user_id     = vm.user.id;

		// User Interface variables
		vm.$$INCREMENT_TYPES = INCREMENT_TYPES;
		vm.$$REPEAT_TYPES    = REPEAT_TYPES;

		vm.save = save;

		// Watchers
		// --------
		$scope.$watch('vm.goal.target.time_estimated', watchTargetDurationHandler);
		$scope.$watch('vm.goal.target.time_increment', watchTargetDurationHandler);

		// Functions
		// =========
		function gotoOverview() {
			$window.location.href = '#/goals';
		}

		function save(event) {
			event.preventDefault();

			$log.log('vm.goal:', vm.goal);

			updateGoal();
		}

		// Goal
		// ----
		function updateGoal() {
			$log.info('updateGoal');
			var goalResource = new GoalResourceFactory();
			goalResource.goal = vm.goal;
			var params = {
				goalId: vm.goal.id
			};
			goalResource.$update(params)
				.then(updateGoalResourceSuccess)
				.catch(updateGoalResourceError);
		}

		function updateGoalResourceError(reason) {
			$log.error('updateGoalResourceError:', reason);
		}

		function updateGoalResourceSuccess(response) {
			$log.log('updateGoalResourceSuccess:', response);
			updateTarget();
		}

		// Target
		// -----
		function updateTarget() {
			$log.info('updateTarget');
			if (vm.goal.$$hasTargetTypeCheckbox()) {
				updateTargetCheckbox();
			} else
			if (vm.goal.$$hasTargetTypeRecurringCheckbox()) {
				updateTargetRecurringCheckbox();
			} else
			if (vm.goal.$$hasTargetTypeDuration()) {
				updateTargetDuration();
			}
		}

		// Target Checkbox
		// ---------------
		function updateTargetCheckbox() {
			$log.info('updateTargetCheckbox');
			var targetCheckboxResource = new TargetCheckboxResourceFactory();
			targetCheckboxResource.target = vm.goal.target;
			var params = {
				targetCheckboxId: vm.goal.target.id
			};
			targetCheckboxResource.$update(params)
				.then(updateTargetCheckboxResourceSuccess)
				.catch(updateTargetCheckboxResourceError);
		}

		function updateTargetCheckboxResourceError(reason) {
			$log.error('updateTargetCheckboxResourceError:', reason);
		}

		function updateTargetCheckboxResourceSuccess(response) {
			$log.log('updateTargetCheckboxResourceSuccess:', response);
			gotoOverview();
		}

		// Target Duration
		// ---------------
		function updateTargetDuration() {
			$log.info('updateTargetDuration');
			var targetDurationResource = new TargetDurationResourceFactory();
			targetDurationResource.target = vm.goal.target;
			var params = {
				targetDurationId: vm.goal.target.id
			};
			targetDurationResource.$update(params)
				.then(updateTargetDurationResourceSuccess)
				.catch(updateTargetDurationResourceError);
		}

		function updateTargetDurationResourceError(reason) {
			$log.error('updateTargetDurationResourceError:', reason);
		}

		function updateTargetDurationResourceSuccess(response) {
			$log.log('updateTargetDurationResourceSuccess:', response);
			gotoOverview();
		}

		// Target Recurring Checkbox
		// -------------------------
		function updateTargetRecurringCheckbox() {
			$log.info('updateTargetRecurringCheckbox');
			var targetRecurringCheckboxResource = new TargetRecurringCheckboxResourceFactory();
			targetRecurringCheckboxResource.target = vm.goal.target;
			var params = {
				targetRecurringCheckboxId: vm.goal.target.id
			};
			targetRecurringCheckboxResource.$update(params)
				.then(updateTargetRecurringCheckboxResourceSuccess)
				.catch(updateTargetRecurringCheckboxResourceError);
		}

		function updateTargetRecurringCheckboxResourceError(reason) {
			$log.error('updateTargetRecurringCheckboxResourceError:', reason);
		}

		function updateTargetRecurringCheckboxResourceSuccess(response) {
			$log.log('updateTargetRecurringCheckboxResourceSuccess:', response);
			gotoOverview();
		}

		// Filters
		// -------
		function isCurrentCategoryFilter(category) {
			return parseInt(category.id) === parseInt($routeParams.categoryId);
		}

		function isCurrentGoalFilter(goal) {
			return parseInt(goal.id) === parseInt($routeParams.goalId);
		}

		// Watch Handlers
		// --------------
		function watchTargetDurationHandler() {
			if (vm.goal.$$hasTargetTypeDuration()) {
				vm.goal.target.$$updateDuration();
			}
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('GoalProgressCtrl', GoalProgressCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalProgressCtrl.$inject = [
		// Angular
		'$log',
		'$routeParams',
		'$scope',
		// Constants
		'configChart'
	];

	function GoalProgressCtrl(
		// Angular
		$log,
		$routeParams,
		$scope,
		// Constants
		configChart
	) {
		$log.info('GoalProgressCtrl');

		$scope.categoryId = $routeParams.categoryId;
		$scope.goalId     = $routeParams.goalId;

		var ctx = document.getElementById("smu-chart-0").getContext("2d");

		var data = {
			labels: ["January", "February", "March", "April", "May", "June", "July"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.5)",
					strokeColor: "rgba(220,220,220,0.8)",
					highlightFill: "rgba(220,220,220,0.75)",
					highlightStroke: "rgba(220,220,220,1)",
					data: [12, 10, 11, 9, 5, 3, 2]
				}
			]
		};

		var smuChart0 = new Chart(ctx).Bar(data, configChart.bar);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('GoalUpdateCtrl', GoalUpdateCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalUpdateCtrl.$inject = [
		// Angular
		'$log',
		'$routeParams',
		'$window',
		// Constants
		'INCREMENT_TYPES',
		'REPEAT_TYPES',
		// Custom
		'StorageFactory',
		'TargetCheckboxResourceFactory',
		'UpdateDurationResourceFactory',
		'UpdateRecurringCheckboxResourceFactory'
	];

	function GoalUpdateCtrl(
		// Angular
		$log,
		$routeParams,
		$window,
		// Constants
		INCREMENT_TYPES,
		REPEAT_TYPES,
		// Custom
		StorageFactory,
		TargetCheckboxResourceFactory,
		UpdateDurationResourceFactory,
		UpdateRecurringCheckboxResourceFactory
	) {
		$log.info('GoalUpdateCtrl');
		// ViewModel
		// =========
		var vm = this;

		vm.categories       = StorageFactory.Local.getCategoryModel('categories');
		vm.user             = StorageFactory.Local.getUserModel('user');
		vm.category         = _.find(vm.categories, isCurrentCategoryFilter);
		vm.goal             = _.find(vm.category.goals, isCurrentGoalFilter);
		vm.goal.category_id = vm.category.id;
		vm.goal.user_id     = vm.user.id;

		if (vm.goal.$$hasTargetTypeDuration()) {
			vm.goal.target.update.$$setTimeIncrement(vm.goal.target.time_increment);
		}

		// User Interface variables
		vm.$$INCREMENT_TYPES = INCREMENT_TYPES;
		vm.$$REPEAT_TYPES    = REPEAT_TYPES;
		vm.$$showGoalDetails = false;

		vm.save              = save;
		vm.toggleGoalDetails = toggleGoalDetails;

		$log.log('vm.goal:', vm.goal);

		// Functions
		// =========
		function gotoOverview() {
			$window.location.href = '#/goals/in-progress';
		}

		function save(event) {
			event.preventDefault();

			$log.log('vm.goal:', vm.goal);

			updateTarget();
		}

		function toggleGoalDetails(event) {
			event.preventDefault();

			vm.$$showGoalDetails = !vm.$$showGoalDetails;
			$log.log('vm.$$showGoalDetails:', vm.$$showGoalDetails);
		}

		function updateTarget() {
			$log.info('updateTarget');
			if (vm.goal.$$hasTargetTypeCheckbox()) {
				updateTargetCheckbox();
			} else
			if (vm.goal.$$hasTargetTypeRecurringCheckbox()) {
				saveUpdateRecurringCheckbox();
			} else
			if (vm.goal.$$hasTargetTypeDuration()) {
				saveUpdateDuration();
			}
		}

		// Target Checkbox
		// ---------------
		function updateTargetCheckbox() {
			$log.info('updateTargetCheckbox');
			var targetCheckboxResource = new TargetCheckboxResourceFactory();
			targetCheckboxResource.target = vm.goal.target;
			var params = {
				targetCheckboxId: vm.goal.target.id
			};
			targetCheckboxResource.$update(params)
				.then(updateTargetCheckboxResourceSuccess)
				.catch(updateTargetCheckboxResourceError);
		}

		function updateTargetCheckboxResourceError(reason) {
			$log.error('updateTargetCheckboxResourceError:', reason);
		}

		function updateTargetCheckboxResourceSuccess(response) {
			$log.log('updateTargetCheckboxResourceSuccess:', response);
			gotoOverview();
		}

		// Target Duration
		// ---------------
		function saveUpdateDuration() {
			$log.info('saveUpdateDuration');
			vm.goal.target.update.target_id = vm.goal.target.id;
			var updateDurationResource = new UpdateDurationResourceFactory();
			updateDurationResource.update = vm.goal.target.update;
			updateDurationResource.$save()
				.then(saveUpdateDurationResourceSuccess)
				.catch(saveUpdateDurationResourceError);
		}

		function saveUpdateDurationResourceError(reason) {
			$log.error('saveUpdateDurationResourceError:', reason);
		}

		function saveUpdateDurationResourceSuccess(response) {
			$log.log('saveUpdateDurationResourceSuccess:', response);
			gotoOverview();
		}

		// Target Recurring Checkbox
		// -------------------------
		function saveUpdateRecurringCheckbox() {
			$log.info('saveUpdateRecurringCheckbox');
			vm.goal.target.update.target_id = vm.goal.target.id;
			var updateRecurringCheckboxResource = new UpdateRecurringCheckboxResourceFactory();
			updateRecurringCheckboxResource.update = vm.goal.target.update;
			updateRecurringCheckboxResource.$save()
				.then(saveUpdateRecurringCheckboxResourceSuccess)
				.catch(saveUpdateRecurringCheckboxResourceError);
		}

		function saveUpdateRecurringCheckboxResourceError(reason) {
			$log.error('saveUpdateRecurringCheckboxResourceError:', reason);
		}

		function saveUpdateRecurringCheckboxResourceSuccess(response) {
			$log.log('saveUpdateRecurringCheckboxResourceSuccess:', response);
			gotoOverview();
		}

		// Filters
		// -------
		function isCurrentCategoryFilter(category) {
			return parseInt(category.id) === parseInt($routeParams.categoryId);
		}

		function isCurrentGoalFilter(goal) {
			return parseInt(goal.id) === parseInt($routeParams.goalId);
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('GoalsInProgressCtrl', GoalsInProgressCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalsInProgressCtrl.$inject = [
		// Angular
		'$log',
		// Custom
		'StorageFactory',
		'UserGoalResourceFactory'
	];

	function GoalsInProgressCtrl(
		// Angular
		$log,
		// Custom
		StorageFactory,
		UserGoalResourceFactory
	) {
		$log.info('GoalsInProgressCtrl');
		// ViewModel
		// =========
		var vm = this;

		vm.user  = StorageFactory.Local.getUserModel('user');
		vm.goals = loadUserGoal();

		// Functions
		// =========
		function loadUserGoal() {
			$log.info('loadUserGoal');
			var params = {
				userId: vm.user.id
			};
			var goals = UserGoalResourceFactory.queryInProgress(params);
			goals.$promise
				.then(loadUserGoalResourceSuccess)
				.catch(loadUserGoalResourceError);

			return goals;
		}

		function loadUserGoalResourceError(reason) {
			$log.error('loadUserGoalResourceError:', reason);
		}

		function loadUserGoalResourceSuccess(response) {
			$log.info('loadUserGoalResourceSuccess:', response);
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('GoalsProgressCtrl', GoalsProgressCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalsProgressCtrl.$inject = [
		// Angular
		'$log',
		'$routeParams',
		'$scope',
		// Constants
		'configChart'
	];

	function GoalsProgressCtrl(
		// Angular
		$log,
		$routeParams,
		$scope,
		// Constants
		configChart
	) {
		$log.log('GoalsProgressCtrl');
		$scope.categoryId = $routeParams.categoryId;
		$scope.goalId     = $routeParams.goalId;

		var ctx = document.getElementById("smu-chart-0").getContext("2d");

		var data = {
			labels: ["January", "February", "March", "April", "May", "June", "July"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.5)",
					strokeColor: "rgba(220,220,220,0.8)",
					highlightFill: "rgba(220,220,220,0.75)",
					highlightStroke: "rgba(220,220,220,1)",
					data: [12, 10, 11, 9, 5, 3, 2]
				}
			]
		};

		var smuChart0 = new Chart(ctx).Bar(data, configChart.bar);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('GoalsCtrl', GoalsCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalsCtrl.$inject = [
		// Angular
		'$log',
		// Custom
		'StorageFactory',
		'GoalResourceFactory',
		'UserCategoryResourceFactory'
	];

	function GoalsCtrl(
		// Angular
		$log,
		// Custom
		StorageFactory,
		GoalResourceFactory,
		UserCategoryResourceFactory
	) {
		$log.info('GoalsCtrl');
		// ViewModel
		// =========
		var vm = this;

		vm.user       = StorageFactory.Local.getUserModel('user');
		vm.categories = loadUserCategory();

		vm.goalChanged = goalChanged;

		// Functions
		// =========

		/**
		 * Update Goal when changed.
		 */
		function goalChanged(goal) {
			$log.log(goal);
			updateGoal(goal);
		}

		// Goal
		// ----
		function updateGoal(goal) {
			$log.info('updateGoal');
			var params = {
				goalId: goal.id
			};
			var goalResource = new GoalResourceFactory();
			goalResource.goal = {
				user_id    : vm.user.id,
				name       : goal.name,
				in_progress: goal.in_progress
			};
			goalResource.$update(params)
				.then(updateGoalResourceSuccess)
				.catch(updateGoalResourceError);
		}

		function updateGoalResourceError(reason) {
			$log.error('updateGoalResourceError:', reason);
		}

		function updateGoalResourceSuccess(response) {
			$log.log('updateGoalResourceSuccess:', response);
		}

		// UserCategory
		// ------------
		function loadUserCategory() {
			$log.info('loadUserCategory');
			var params = {
				'userId'     : vm.user.id,
				'sort[order]': 'asc'
			};
			var categories = UserCategoryResourceFactory.queryWithTarget(params);

			categories.$promise
				.then(loadUserCategoryResourceSuccess)
				.catch(loadUserCategoryResourceError);

			return categories;
		}

		function loadUserCategoryResourceError(reason) {
			$log.error('loadUserCategoryResourceError:', reason);
		}

		function loadUserCategoryResourceSuccess(response) {
			$log.info('loadUserCategoryResourceSuccess:', response);
			StorageFactory.Local.setObject('categories', vm.categories);
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('LeaderboardCtrl', LeaderboardCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	LeaderboardCtrl.$inject = [
		'$scope'
	];

	function LeaderboardCtrl($scope) {

		$scope.leaderboard = [
			{
				position: 1,
				score: 910,
				user: {
					firstName: "Jane"
				},
				company: {
					name: "Arteveldehogeschool"
				}
			},{
				position: 2,
				score: 44,
				user: {
					firstName: "Olivier"
				},
				company: {
					name: "Superstar-up"
				}
			},{
				position: 3,
				score: 30,
				user: {
					firstName: "Christel"
				},
				company: {
					name: "Arteveldehogeschool"
				}
			}
		];

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('LogInCtrl', LogInCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	LogInCtrl.$inject = [
		// Angular
		'$log',
		'$scope',
		'$window',
		// Custom
		'AuthUserResourceFactory',
		'DialogFactory',
		'StorageFactory',
		'ToastFactory',
		'UserModelFactory'
        //'DriverModelFactory'
	];

	function LogInCtrl(
		// Angular
		$log,
		$scope,
		$window,
		// Custom
		AuthUserResourceFactory,
		DialogFactory,
		StorageFactory,
		ToastFactory,
		UserModelFactory
		//DriverModelFactory
	) {
		// ViewModel
		var vm = this;

		// The function is inherited from the parent controller (AppCtrl) scope.
		vm.openLeftSidenav = $scope.openLeftSidenav;

		vm.processForm = processForm;

		vm.user = new UserModelFactory();
        //vm.driver = new DriverModelFactory();

		// Functions
		// =========
		function loginError(reason) {
			$log.error('loginError:', reason);
			DialogFactory
				.showItems('Could not log you in!', reason.errors);
		}

		function loginSuccess(response) {
			$log.log('loginSuccess:', response);
			ToastFactory
				.show('User logged in!')
				.finally(function() {
					vm.user.id = response.id;
                    //vm.driver.id = response.id;
					StorageFactory.Local.setObject('user', vm.user);
                    //StorageFactory.Local.setObject('driver', vm.driver);
					$window.location.href = '#/rides';
				});
		}

		function processForm(event) {
			event.preventDefault();
			if ($scope.login_form.$valid) {
				$log.log('user: ', vm.user);

				var authUserResource = new AuthUserResourceFactory();
				authUserResource.user = vm.user;
				authUserResource.$login()
					.then(loginSuccess)
					.catch(loginError);
			} else {
				ToastFactory
					.show('Please fill in the required fields to continue!');
			}
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('MoodStatisticsCtrl', MoodStatisticsCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	MoodStatisticsCtrl.$inject = [
		// Angular
		'$log',
		// Configs
		'configChart',
		// Constants
		'MOOD_TYPES',
		// Custom
		'StorageFactory',
		'UserMoodResourceFactory'
	];

	function MoodStatisticsCtrl(
		// Angular
		$log,
		// Configs
		configChart,
		// Constants
		MOOD_TYPES,
		// Custom
		StorageFactory,
		UserMoodResourceFactory
	) {
		$log.info('MoodStatisticsCtrl');
		// ViewModel
		// =========
		var vm = this;

		vm.user = StorageFactory.Local.getUserModel('user');

		vm.$$moodCounts = _.map(MOOD_TYPES, function (mood) {
			mood.count = 0;
			return mood;
		});

		$log.log('vm.$$moodCounts', vm.$$moodCounts);

		loadMoodStatistics();

		// Functions
		// =========
		function loadMoodStatistics() {
			$log.info('loadMoodStatistics');
			var params = {
				'userId': vm.user.id,
				'moodId': 'statistics'
			};
			var statistics = UserMoodResourceFactory.statistics(params);

			statistics.$promise
				.then(loadMoodStatisticsResourceSuccess)
				.catch(loadMoodStatisticsResourceError);
		}

		function loadMoodStatisticsResourceError(reason) {
			$log.error('loadMoodStatisticsResourceError:', reason);
		}

		function loadMoodStatisticsResourceSuccess(response) {
			$log.log('loadMoodStatisticsResourceSuccess:', response);

			vm.$$moodCounts.map(function (mood) {
				var foundMood =_.find(response.data, function (dataMood) {
					return dataMood.feeling == mood.value;
				});
				if (foundMood) {
					mood.count = foundMood.count;
				}
			});

			drawChart();
		}

		// Response Handlers
		// -----------------
		function drawChart() {
			var ctx = document.getElementById("mood-chart-0").getContext("2d");

			var data = {
				labels: _.pluck(vm.$$moodCounts, 'label'),
				datasets: [
					{
						label          : "Moods",
						fillColor      : "rgba(220, 220, 220 , .5 )",
						strokeColor    : "rgba(220, 220, 220 , .8 )",
						highlightFill  : "rgba(220, 220, 220 , .75)",
						highlightStroke: "rgba(220, 220, 220 ,1   )",
						data           : _.pluck(vm.$$moodCounts, 'count')
					}
				]
			};

			var options = configChart.bar;

			new Chart(ctx).Bar(data, options);
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('MoodUpdateCtrl', MoodUpdateCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	MoodUpdateCtrl.$inject = [
		// Angular
		'$log',
		'$window',
		// Constants
		'MOOD_TYPES',
		// Custom
		'DialogFactory',
		'MoodModelFactory',
		'StorageFactory',
		'ToastFactory',
		'UserMoodResourceFactory'
	];

	function MoodUpdateCtrl(
		// Angular
		$log,
		$window,
		// Constants
		MOOD_TYPES,
		// Custom
		DialogFactory,
		MoodModelFactory,
		StorageFactory,
		ToastFactory,
		UserMoodResourceFactory
	) {
		$log.info('MoodUpdateCtrl');
		// ViewModel
		// =========
		var vm = this;

		vm.user         = StorageFactory.Local.getUserModel('user');
		vm.mood         = new MoodModelFactory();
		vm.mood.user_id = vm.user.id;

		vm.$$MOOD_TYPES = MOOD_TYPES;

		vm.save = save;

		$log.log('vm.mood:', vm.mood);

		// Functions
		// =========
		function gotoOverview() {
			$window.location.href = '#/goals';
		}

		function save(event) {
			event.preventDefault();

			var params = {
				userId: vm.user.id
			};
			var userMoodResource = new UserMoodResourceFactory();
			userMoodResource.mood = vm.mood;
			userMoodResource.$save(params)
				.then(saveUserMoodResourceResourceSuccess)
				.catch(saveUserMoodResourceResourceError);
		}

		// Response Handlers
		// -----------------
		function saveUserMoodResourceResourceError(reason) {
			$log.error('saveUserResourceError:', reason);
			DialogFactory
				.showItems('The user mood could not be saved!', reason.errors);
		}

		function saveUserMoodResourceResourceSuccess(response) {
			$log.log('saveUserResourceSuccess:', response);
			vm.mood.id = response.id;
			ToastFactory
				.show('User mood saved!')
				.finally(gotoOverview);
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('NearbyCtrl', NearbyCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	NearbyCtrl.$inject = [
		// Angular
		'$interpolate',
		'$log',
		// Constants
		'configMap',
		// Custom
		'LocationResourceFactory'
	];

	function NearbyCtrl(
		// Angular
		$interpolate,
		$log,
		// Constants
		configMap,
		// Custom
		LocationResourceFactory
	) {
		// ViewModel
		var vm = this;

		var templates = {
			location: '<b>{{ title }}</b><i><br>{{ subtitle }}</i><br>{{ address.street }} {{ address.street_number }}<br>{{ address.locality.postal_code }} {{ address.locality.name }}',
			user: '<b>You</b><br>You are here!'
		};

		var compileTemplateLocation = $interpolate(templates.location);

		var map = L.map('smu-map-0');
		L.tileLayer(configMap.tile.urlTemplate, configMap.tile.options).addTo(map);

		var iconCompany = L.icon(configMap.icon.company);

		LocationResourceFactory
			.queryWithAddress()
			.$promise
				.then(locationsSuccess)
				.catch(locationsError);

		navigator.geolocation.getCurrentPosition(geoSuccess, geoError);

		// Functions
		// ---------

		function geoDraw(company) {
			var point  = [company.latitude, company.longitude];
			var marker = L.marker(point, {icon: iconCompany}).addTo(map);

			marker.bindPopup(compileTemplateLocation(company)).openPopup();
		}

		function geoError() {
			$log.error('geolocation could not be determined');
		}

		// Current Position of User
		function geoSuccess(position) {
			var point    = [position.coords.latitude, position.coords.longitude];
			var iconUser = L.icon(configMap.icon.user);
			var marker   = L.marker(point, {icon: iconUser}).addTo(map);

			map.setView(point, 12);
			marker.bindPopup(templates.user).openPopup();
		}

		function locationsSuccess(response) {
			$log.log('locationsSuccess:', response);
			response.data.forEach(geoDraw);
		}

		function locationsError(reason) {
			$log.error('locationsError:', reason);
		}

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('RegistrationStep1Ctrl', RegistrationStep1Ctrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	RegistrationStep1Ctrl.$inject = [
		// Angular
		'$log',
		'$scope',
		'$window',
		// Custom
		'RegistrationFormStateFactory',
		'ToastFactory'
	];

	function RegistrationStep1Ctrl(
		// Angular
		$log,
		$scope,
		$window,
		// Custom
		RegistrationFormStateFactory,
		ToastFactory
	) {
		$log.info('Registration: step 1');
		// ViewModel
		var vm = this;

		// Get the form state.
		var formState = RegistrationFormStateFactory;
		vm.user = formState.user;
		$log.log('vm:', vm);

		vm.processFormStep = processFormStep;

		// Functions
		// =========
		function gotoNextStep() {
			$window.location.href = '#/registration/step/2/of/4';
		}

		function processFormStep(event) {
			event.preventDefault();
			if ($scope.registration_form.$valid) {
				// Set the form state.
				formState.user = vm.user;
				gotoNextStep();
			} else {
				ToastFactory
					.show('Please fill in the required fields to continue!');
			}
		}

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('RegistrationStep2Ctrl', RegistrationStep2Ctrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	RegistrationStep2Ctrl.$inject = [
		// Angular
		'$log',
		'$scope',
		'$window',
		// Custom
		'DialogFactory',
		'InterestResourceFactory',
		'RegistrationFormStateFactory',
		'ToastFactory',
		'UserResourceFactory',
		'UserSettingsResourceFactory'
	];

	function RegistrationStep2Ctrl(
		// Angular
		$log,
		$scope,
		$window,
		// Custom
		DialogFactory,
		InterestResourceFactory,
		RegistrationFormStateFactory,
		ToastFactory,
		UserResourceFactory,
		UserSettingsResourceFactory
	) {
		$log.info('Registration: step 2');
		// ViewModel
		// =========
		var vm = this;

		// Get the form state.
		var formState = RegistrationFormStateFactory;
		vm.settings = formState.settings;
		vm.user     = formState.user;
		$log.log('vm:', vm);

		vm.processFormStep = processFormStep;

		loadInterest();

		// Functions
		// =========
		function gotoNextStep() {
			$window.location.href = '#/registration/step/3/of/4';
		}

		function processFormStep(event) {
			event.preventDefault();

			$log.log('user: ', vm.user);
			if ($scope.registration_form.$valid) {
				// Set the form state.
				formState.settings = vm.settings;
				formState.user     = vm.user;
				saveUser();
			} else {
				ToastFactory.show('Please fill in the required fields to continue!');
			}
		}

		// Interest
		// --------
		function loadInterest() {
			$log.info('loadInterest');
			InterestResourceFactory.query()
				.$promise
					.then(loadInterestResourceSuccess)
					.catch(loadInterestResourceError);
		}

		function loadInterestResourceError(reason) {
			$log.error('loadInterestResourceError:', reason);
		}

		function loadInterestResourceSuccess(response) {
			$log.log('loadInterestResourceSuccess:', response);
			vm.user.$$interests = response.resource; // response.resource contains the data processed by the interceptor, while $promise contains raw data.
		}

		// User
		// ----
		function saveUser() {
			$log.info('saveUser');
			if (!angular.isNumber(formState.user.id)) {
				var userResource = new UserResourceFactory();
				userResource.user = formState.user;
				userResource.$save()
					.then(saveUserResourceSuccess)
					.catch(saveUserResourceError);
			} else {
				$log.warn('User already exists');
				updateUserSettings();
			}
		}

		function saveUserResourceError(reason) {
			$log.error('saveUserResourceError:', reason);
			DialogFactory
				.showItems('The user profile could not be saved!', reason.errors);
		}

		function saveUserResourceSuccess(response) {
			$log.log('saveUserResourceSuccess:', response);
            // Update the form state.
			formState.user.id          = response.id;
			formState.settings.id      = response.settings_id;
			formState.settings.user_id = response.id;
			updateUserSettings();
		}

		// UserSettings
		// ------------
		function updateUserSettings() {
			$log.info('updateUserSettings');
			var userSettingsResource = new UserSettingsResourceFactory;
			var params = {
				userId    : formState.user.id,
				settingsId: formState.settings.id
			};
			userSettingsResource.$get(params)
				.then(function () {
					angular.merge(userSettingsResource, formState.settings);
					userSettingsResource.$update(params)
						.then(updateUserSettingsResourceSuccess)
						.catch(updateUserSettingsResourceError);
				});
		}

		function updateUserSettingsResourceError(reason) {
			$log.error('saveUserSettingsResourceError:', reason);
			DialogFactory
				.showItems('The user settings could not be updated!', reason.errors);
		}

		function updateUserSettingsResourceSuccess(response) {
			$log.log('saveUserSettingsResourceSuccess:', response);
			ToastFactory
				.show('User settings updated!')
				.finally(gotoNextStep);
		}

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('RegistrationStep3Ctrl', RegistrationStep3Ctrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	RegistrationStep3Ctrl.$inject = [
		// Angular
		'$log',
		'$scope',
		'$window',
		// Custom
		'AddressResourceFactory',
		//'CompanyResourceFactory',
		'CountryResourceFactory',
		'CountryUiModelFactory',
		'DialogFactory',
		'LocalityResourceFactory',
		'LocalityUiModelFactory',
		'RegionResourceFactory',
		'RegionUiModelFactory',
		'RegistrationFormStateFactory',
		'ToastFactory',
		'UserResourceFactory',
		'UserSettingsResourceFactory',
        'DriverResourceFactory'
	];

	function RegistrationStep3Ctrl(
		// Angular
		$log,
		$scope,
		$window,
		// Custom
		AddressResourceFactory,
		//CompanyResourceFactory,
		CountryResourceFactory,
		CountryUiModelFactory,
		DialogFactory,
		LocalityResourceFactory,
		LocalityUiModelFactory,
		RegionResourceFactory,
		RegionUiModelFactory,
		RegistrationFormStateFactory,
		ToastFactory,
		UserResourceFactory,
		UserSettingsResourceFactory,
        DriverResourceFactory
	) {
		$log.info('Registration: step 3');

		// ViewModel
		// =========
		var vm = this;

		// Get the form state.
		var formState = RegistrationFormStateFactory;
		vm.address  = formState.address;
		vm.country  = formState.country;
		//vm.company  = formState.company;
		vm.locality = formState.locality;
		vm.region   = formState.region;
		vm.settings = formState.settings;
		vm.user     = formState.user;
		vm.driver   = formState.driver;
		$log.log('vm:', vm);

		vm.$$country  = new CountryUiModelFactory();
		vm.$$region   = new RegionUiModelFactory();
		vm.$$locality = new LocalityUiModelFactory();

		vm.processFormStep = processFormStep;

		// Watchers
		// --------
		// UI Watchers
		$scope.$watch('vm.$$country.$$selected' , watchCountryUiHandler);
		$scope.$watch('vm.$$locality.$$selected', watchLocalityUiHandler);
		$scope.$watch('vm.$$region.$$selected'  , watchRegionUiHandler);

		// Normal Watchers
		$scope.$watch('vm.locality.name', watchLocalityHandler);
		$scope.$watch('vm.region.name'  , watchRegionHandler);

		// Functions
		// =========
		function gotoNextStep() {
			$window.location.href = '#/registration/step/4/of/4';
		}

		function processFormStep(event) {
			event.preventDefault();
			$log.info('vm:', vm);
			if ($scope.registration_form.$valid) {
				if (angular.isNumber(vm.user.id)) {
					process1Country();
				} else {
					ToastFactory.show('Please complete the previous steps first');
				}
			} else {
				ToastFactory.show('Please fill in the required fields to continue!');
			}
			$log.info('vm:', vm);
		}

		function process1Country() {
			$log.info('process1Country:', vm.country);
			// Set the form state.
			formState.country = vm.country;
			if (angular.isNumber(vm.country.id)) {
				$log.log('Using country:', vm.country.id);
				process2Region();
			} else {
				formState.country.name = vm.$$country.$$searchTextName;
				saveCountry();
			}
		}

		function process2Region() {
			$log.info('process2Region:', vm.region);
			// Set the form state.
			formState.region = vm.region;
			if (angular.isNumber(vm.region.id)) {
				$log.log('Using region:', vm.region.id);
				process3Locality();
			} else {
				formState.region.name       = vm.$$region.$$searchTextName;
				formState.region.country_id = vm.country.id;
				saveRegion();
			}
		}

		function process3Locality() {
			$log.info('process3Locality');
			// Set the form state.
			formState.locality = vm.locality;
			if (angular.isNumber(vm.locality.id)) {
				$log.log('Using locality: ', vm.locality.id);
				process4Address();
				// @todo Update resource.
			} else {
				formState.locality.name        = vm.$$locality.$$searchTextName;
				formState.locality.postal_code = vm.$$locality.$$searchTextPostalCode;
				formState.locality.region_id   = vm.region.id;
				saveLocality();
			}
		}

		function process4Address() {
			$log.info('process4Address');
			// Set the form state.
			formState.address = vm.address;
			if (angular.isNumber(vm.address.id)) {
				$log.log('Using address: ', vm.address.id);
                process5Driver();
                //process6User();
				// @todo Update resource.
			} else {
				saveAddress();
			}
		}

        /** @author Lien Mergan */
        function process5Driver() {
            $log.info('process5Driver');
            formState.driver = vm.driver;
            if (angular.isNumber(vm.driver.id)) {
                $log.log('Using driver: ', vm.driver.id);
                process6User();
            } else {
                saveDriver();
            }
        }

        //function process5Company() {
        //    $log.info('process5Company');
        //    // Set the form state.
        //    formState.company = vm.company;
        //    if (angular.isNumber(vm.company.id)) {
        //        $log.log('Using company: ', vm.company.id);
        //        process6User();
        //        // @todo Update resource.
        //    } else {
        //        saveCompany();
        //    }
        //}

		function process6User() {
			$log.info('process6User');
			updateUser(driver.id);
		}

		function process7Settings() {
			$log.info('process7Settings');
			updateUserSettings();
		}

		// Address
		// -------
		function saveAddress() {
			$log.info('saveAddress');
			var addressResource = new AddressResourceFactory;
			addressResource.address             = vm.address;
			addressResource.address.locality_id = vm.locality.id;
			addressResource.$save()
				.then(saveAddressResourceSuccess)
				.catch(saveAddressResourceError);
		}

		function saveAddressResourceError(reason) {
			$log.error('saveAddressResourceError:', reason);
			DialogFactory
				.showItems('The address could not be saved!', reason.errors);
		}

		function saveAddressResourceSuccess(response) {
			$log.log('saveAddressResourceSuccess:', response);
			vm.address.id = response.id;
            process5Driver();
		}

		// Company
		// -------
		//function saveCompany() {
		//	$log.info('saveCompany');
		//	var companyResource = new CompanyResourceFactory;
		//	companyResource.company            = vm.company;
		//	companyResource.company.address_id = vm.address.id;
		//	companyResource.$save()
		//		.then(saveCompanyResourceSuccess)
		//		.catch(saveCompanyResourceError);
		//}
        //
		//function saveCompanyResourceError(reason) {
		//	$log.error('saveCompanyResourceError:', reason);
		//	DialogFactory
		//		.showItems('The company profile could not be saved!', reason.errors);
		//}
        //
		//function saveCompanyResourceSuccess(response) {
		//	$log.log('saveCompanyResourceSuccess:', response);
		//	vm.company.id = response.id;
		//	process6User();
		//}

		// Country
		// -------
		function saveCountry() {
			$log.info('saveCountry');
			var countryResource = new CountryResourceFactory();
			countryResource.country = formState.country;
			countryResource.$save()
				.then(saveCountryResourceSuccess)
				.catch(saveCountryResourceError);
		}

		function saveCountryResourceError(reason) {
			$log.error('saveCountryResourceError:', reason);
			DialogFactory
				.showItems('The country could not be saved!', reason.errors);
		}

		function saveCountryResourceSuccess(response) {
			$log.log('saveCountryResourceSuccess:', response);
			vm.country.id = response.id;
			process2Region();
		}

		// Locality
		// --------
		function saveLocality() {
			$log.info('saveLocality');
			var localityResource = new LocalityResourceFactory();
			localityResource.locality = formState.locality;
			localityResource.$save()
				.then(saveLocalityResourceSuccess)
				.catch(saveLocalityResourceError);
		}

		function saveLocalityResourceError(reason) {
			$log.error('saveLocalityResourceError:', reason);
			DialogFactory
				.showItems('The postal code and locality could not be saved!', reason.errors);
		}

		function saveLocalityResourceSuccess(response) {
			$log.log('saveLocalityResourceSuccess:', response);
			vm.locality.id = response.id;
			process4Address();
		}

		// Region
		// ------
		function saveRegion() {
			$log.info('saveRegion');
			var regionResource = new RegionResourceFactory();
			regionResource.region = formState.region;
			regionResource.$save()
				.then(saveRegionResourceSuccess)
				.catch(saveRegionResourceError);
		}

		function saveRegionResourceError(reason) {
			$log.error('saveRegionResourceError:', reason);
			DialogFactory
				.showItems('The region could not be saved!', reason.errors);
		}

		function saveRegionResourceSuccess(response) {
			$log.log('saveRegionResourceSuccess:', response);
			vm.region.id = response.id;
			process3Locality();
		}

		// User
		// ----
		function updateUser(user) {
			UserResourceFactory.update( { userId: user }, function() {
				$log.log('The user has been updated');
			})
				.then(updateUserResourceSuccess)
				.catch(updateUserResourceError);

			//var userResource = new UserResourceFactory();
			//var params = {
			//	userId: vm.user.id
			//};
			//userResource.$get(params)
			//	.then(function () {
			//		userResource.driver_id = vm.driver.id;
			//		userResource.$update()
			//			.then(updateUserResourceSuccess)
			//			.catch(updateUserResourceError);
			//	});
		}

		function updateUserResourceError(reason) {
			$log.error('updateUserResourceError:', reason);
			DialogFactory
				.showItems('The user profile could not be updated!', reason.errors);
		}

		function updateUserResourceSuccess(response) {
			$log.log('updateUserResourceSuccess:', response);
			process7Settings();
		}

        /** @author Lien Mergan */
        // Driver
        // ------
        function saveDriver() {
            $log.info('saveDriver');
            var driverResource = new DriverResourceFactory();
            driverResource.driver            = formState.driver;
            driverResource.driver.role       = formState.driver.role;
            $log.log(driverResource);
            //driverResource.driver.ride_id    = formState.ride.id;
            driverResource.$save()
                .then(saveDriverResourceSuccess)
                .catch(saveDriverResourceError);
        }

        function saveDriverResourceError(reason) {
            $log.error('saveDriverResourceError:', reason);
            DialogFactory
                .showItems('The driver profile could not be saved!', reason.errors);
        }

        function saveDriverResourceSuccess(response) {
            $log.log('saveDriverResourceSuccess:', response);
            vm.driver.id      = response.id;
            vm.driver.role    = response.role;
            vm.driver.user_id = response.id;
            process6User();
        }

		// UserSettings
		// ------------
		function updateUserSettings() {
			$log.info('updateUserSettings');
			var userSettingsResource = new UserSettingsResourceFactory;
			var params = {
				userId    : formState.user.id,
				settingsId: formState.settings.id
			};
			userSettingsResource.$get(params)
				.then(function () {
					angular.merge(userSettingsResource, formState.settings);
					userSettingsResource.$update(params)
						.then(updateUserSettingsResourceSuccess)
						.catch(updateUserSettingsResourceError);
				});
		}

		function updateUserSettingsResourceError(reason) {
			$log.error('saveUserSettingsResourceError:', reason);
			DialogFactory
				.showItems('The user settings could not be updated!', reason.errors);
		}

		function updateUserSettingsResourceSuccess(response) {
			$log.log('saveUserSettingsResourceSuccess:', response);
			ToastFactory
				.show('User settings updated!')
				.finally(gotoNextStep);
		}

		// Watch Handlers
		// --------------
		/**
		 * Change region if a known locality is selected
		 */
		function watchLocalityHandler() {
			$log.info('locality changed:', vm.locality, vm.$$locality);
			if (angular.isObject(vm.$$locality.$$selected) && angular.isObject(vm.$$locality.$$selected.region)) {
				$log.log('change');
				vm.$$region.$$selected   = vm.$$locality.$$selected.region;
				vm.$$region.$$searchText = vm.$$locality.$$selected.region.name;
			}
		}

		/**
		 * Change country if a known region is selected
		 */
		function watchRegionHandler() {
			$log.info('region changed:', vm.region, vm.$$region);
			if (angular.isObject(vm.$$region.$$selected) && angular.isObject(vm.$$region.$$selected.country)) {
				$log.log('change');
				vm.$$country.$$selected       = vm.$$region.$$selected.country;
				vm.$$country.$$searchTextName = vm.$$region.$$selected.country.name;
			}
		}

		// UI Watch handlers
		// -----------------
		/**
		 * Set country to selected country object, or create a new country object.
		 */
		function watchCountryUiHandler() {
			$log.info('ui country changed:', vm.$$country, vm.country);
			vm.country = angular.isObject(vm.$$country.$$selected) ? vm.$$country.$$selected : vm.$$country.$$createModel();
			$log.log('country:', vm.country);
		}

		/**
		 * Set locality to selected locality object, or create a new locality object.
		 */
		function watchLocalityUiHandler() {
			vm.locality = angular.isObject(vm.$$locality.$$selected) ? vm.$$locality.$$selected : vm.$$locality.$$createModel();
			$log.log('locality:', vm.locality);
		}

		/**
		 * Set region to selected region object, or create a new region object.
		 */
		function watchRegionUiHandler() {
			$log.info('ui region changed:', vm.$$region, vm.region);
			vm.region = angular.isObject(vm.$$region.$$selected) ? vm.$$region.$$selected : vm.$$region.$$createModel();
			$log.log('region:', vm.region);
		}

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('RegistrationStep4Ctrl', RegistrationStep4Ctrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	RegistrationStep4Ctrl.$inject = [
		// Angular
		'$log',
		// Custom
		'RegistrationFormStateFactory'
	];

	function RegistrationStep4Ctrl(
		// Angular
		$log,
		// Custom
		RegistrationFormStateFactory
	) {
		$log.info('Registration: step 4');
		// ViewModel
		// =========
		var vm = this;

		var formState = RegistrationFormStateFactory; // Get the form state.
		angular.merge(vm, formState);

		$log.log(vm);

		// Functions
		// =========

	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('RideCreateCtrl', RideCreateCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideCreateCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        // Custom
        'DialogFactory',
        'RideFormStateFactory',
        'RideResourceFactory',
        'UserResourceFactory',
        'DriverResourceFactory'
    ];

    function RideCreateCtrl(
        // Angular
        $log,
        $scope,
        // Custom
        DialogFactory,
        RideFormStateFactory,
        RideResourceFactory,
        UserResourceFactory,
        DriverResourceFactory
    ) {
        $log.info('RideCreateCtrl');

        //View Model
        var vm = this;

        // Get the form state.
        var formState   = RideFormStateFactory;
        vm.ride         = formState.ride;
        vm.user         = formState.user;
        //vm.user         = StorageFactory.Local.getUserModel('user');
        //vm.ride.user_id = vm.user.id;
        vm.ride_from    = formState.ride_from;
        vm.ride_to      = formState.ride_to;
        $log.log('vm:', vm);

        vm.processFormStep = processFormStep;

        function processFormStep(event) {
            event.preventDefault();
            $log.info('ride: ', vm.ride);
            $log.info('user: ', vm.user);
            if ($scope.ride_creation_form.$valid) {
                // Set the form state.
                //formState.driver = vm.driver;
                formState.ride      = vm.ride;
                formState.user      = vm.user;
                formState.ride_from = vm.ride_from;
                formState.ride_to   = vm.ride_to;
                saveRide();
            }
        }

        function saveRide(){
            $log.info('saveRide');
            if (!angular.isNumber(formState.ride.id)) {
                var rideResource  = new RideResourceFactory();
                var userResource = new UserResourceFactory();
                //var paramsRide = {
                //    rideId : formState.ride.id
                //};
                var paramsUser = {
                    userId : formState.user.id
                };
                userResource.$get(paramsUser);
                rideResource.ride = vm.ride;
                //rideResource.ride.user_id = vm.user.id;
                //rideResource.driver = formState.driver;
                $log.log(rideResource);
                userResource.$update(paramsUser);
                rideResource.$save()
                    .then(saveRideResourceSuccess)
                    .catch(saveRideResourceError);
                //updateDriver();
            }
        }

        function saveRideResourceError(reason) {
            $log.error('saveRideResourceError:', reason);
            DialogFactory
                .showItems('The ride could not be saved!', reason.errors);
        }

        function saveRideResourceSuccess(response) {
            $log.log('saveRideResourceSuccess:', response);
            // Update the form state.
            formState.ride.id          = response.id;
            //formState.user.id          = response.user.id;
            formState.ride.ride_from   = response.ride_from;
            formState.ride.ride_to     = response.ride_to;
            //formState.ride.user_id     = response.user.id;
            //formState.driver.id      = response.driver_id;
        }

        //$scope.ride = new RideResourceFactory();
        //$scope.addRide = $scope.ride.$save;

        // UpdateDriver
        // ------------
        function updateDriver() {
            $log.info('updateDriver');
            var driverResource = new DriverResourceFactory;
            var params = {
                userId    : formState.user.id,
                rideId    : formState.ride.id
            };
            driverResource.$get(params)
                .then(function () {
                    angular.merge(driverResource, formState.ride);
                    driverResource.$update(params)
                        .then(updateUserSettingsResourceSuccess)
                        .catch(updateUserSettingsResourceError);
                });
        }

        function updateUserSettingsResourceError(reason) {
            $log.error('saveUserSettingsResourceError:', reason);
            DialogFactory
                .showItems('The user settings could not be updated!', reason.errors);
        }

        function updateUserSettingsResourceSuccess(response) {
            $log.log('saveUserSettingsResourceSuccess:', response);
            ToastFactory
                .show('User settings updated!');
        }
    }

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('RidesCtrl', RidesCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RidesCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        // Custom
        'RideResourceFactory'
    ];

    function RidesCtrl(
        // Angular
        $log,
        $scope,
        // Custom
        RideResourceFactory
    ) {
        $log.info('RidesCtrl');

        //var users = UserResourceFactory.query(function() {
        //    console.log(users);
        //});

        $scope.rides = RideResourceFactory.query();

        //$scope.users = [];
        //$scope.loading = false;
        //
        //$scope.init = function() {
        //    $scope.loading = true;
        //    UserResourceFactory.get().success(function(data){
        //        $scope.users = data.data;
        //        $log.log('users: ', users);
        //        $scope.loading = false;
        //    });
        //}
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('SettingsCtrl', SettingsCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	SettingsCtrl.$inject = [
		// Angular
		'$scope'
	];

	function SettingsCtrl(
		// Angular
		$scope
	) {
		var vm = this;

	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('UserUpdateCtrl', UserUpdateCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    UserUpdateCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        '$window',
        // Custom
        'DialogFactory',
        'UserResourceFactory',
        'EditUserFormStateFactory'
    ];

    function UserUpdateCtrl(
        // Angular
        $log,
        $scope,
        $window,
        // Custom
        DialogFactory,
        UserResourceFactory,
        EditUserFormStateFactory
    ) {
        $log.info('UserUpdateCtrl');

        var vm = this;

        var formState  = EditUserFormStateFactory;
        vm.address     = formState.address;
        vm.country     = formState.country;
        vm.locality    = formState.locality;
        vm.region      = formState.region;
        vm.settings    = formState.settings;
        vm.user        = formState.user;

        vm.name        = formState.user.name;
        vm.family_name = formState.user.family_name;
        vm.mobile      = formState.user.mobile;
        //vm.driver      = formState.driver;

        $log.log('vm:', vm);

        vm.processFormStep = processFormStep;

        $scope.showDetail = function(user) {
            $scope.user = user;
            $log.log(user);
            $window.location.href = '#/users/update';
        };

        function processFormStep(event) {
            event.preventDefault();
            $log.info('user:', vm.user);
            if ($scope.edit_user_form.$valid) {
                // Set the form state.
                formState.user             = vm.user;
                formState.user.name        = vm.name;
                formState.user.family_name = vm.family_name;
                formState.user.mobile      = vm.mobile;
                updateUser(user.id);
            }
            $log.info('vm:', vm);
        }

        //$scope.updateUser = function(user) {
        //    UserResourceFactory.update( { userId: user }, function() {
        //        $log.log('The user has been updated');
        //    })
        //        .then(updateUserResourceSuccess())
        //        .catch(updateUserResourceError());
        //};

        function updateUser(user) {
            UserResourceFactory.update( { userId: user }, function() {
                $log.log('The user has been updated');
            })
                .then(updateUserResourceSuccess)
                .catch(updateUserResourceError);
        }

        //function updateUser() {
        //    var userResource = new UserResourceFactory;
        //    var params = {
        //        userId: vm.user.id
        //    };
        //    userResource.$get(params)
        //        .then(function () {
        //            //userResource.driver_id = vm.driver.id;
        //            userResource.$update()
        //                .then(updateUserResourceSuccess)
        //                .catch(updateUserResourceError);
        //        });
        //}

        function updateUserResourceError(reason) {
            $log.error('updateUserResourceError:', reason);
            DialogFactory
                .showItems('The user profile could not be updated!', reason.errors);
        }

        function updateUserResourceSuccess(response) {
            $log.log('updateUserResourceSuccess:', response);
        }
    }

})();
/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuControllers')
        .controller('UsersCtrl', UsersCtrl);

    // Inject dependencies into constructor (needed when JS minification is applied).
    UsersCtrl.$inject = [
        // Angular
        '$log',
        '$scope',
        // Custom
        'UserResourceFactory',
        'StorageFactory',
        'DialogFactory',
        'AddressResourceFactory'
    ];

    function UsersCtrl(
        // Angular
        $log,
        $scope,
        // Custom
        UserResourceFactory,
        AddressResourceFactory,
        StorageFactory,
        DialogFactory
    ) {
        $log.info('UsersCtrl');

        $scope.users = UserResourceFactory.query();

        //SHOW USER DETAILS - SELECTED ROW
        $scope.selIdx= -1;
        $scope.selUser = function(user,idx){
            $scope.selectedUser = user;
            $scope.selIdx = idx;
        };

        $scope.isSelUser = function(user){
            return $scope.selectedUser === user;
        };

        //DELETE USER
        $scope.deleteUserSubject = function(user) {
            UserResourceFactory.delete( { userId: user }, function() {
                $log.log('Deleted from server');
                $scope.users = UserResourceFactory.query();
            } )
        };

        //$scope.deleteUser = function(id){
        //    $scope.deletedUser = id;
        //    $log.log('user_id: ', $scope.deletedUser);
        //
        //    //UserResourceFactory.delete(id);
        //    UserResourceFactory.delete(id)
        //        .then(deleteUserSuccess)
        //        .catch(deleteUserError);
        //    $scope.users = UserResourceFactory.query();
        //    //$log.log($scope.users);
        //};


        //$scope.deleteUser = function(id) {
        //    $log.info('deleteUser');
        //    $scope.deletedUser = id;
        //    $log.log('user_id: ', $scope.deletedUser);
        //    UserResourceFactory.delete();
        //};

        //$scope.deleteUserSubject = function(deleteduser) {
        //    var id = deleteduser.id;
        //    user.deleteUser(id)
        //        .then(function(success) {
        //            UserResourceFactory.query();
        //            $log.log(success);
        //        }, function(error) {
        //            $log.log(error)
        //        })
        //};

        //$scope.userResource = UserResourceFactory.get({ userId: $scope.id }, function() {
        //    $scope.userResource.$delete(function() {
        //        //gone forever!
        //        $log.log('Deleted from server');
        //    });
        //});

        //$scope.deleteUser = function(id) {
        //    var user = UserResourceFactory.get(id, function() {
        //        user.deletedUser = id;
        //        $log.log('user_id: ', user.deletedUser);
        //        user.$delete()
        //            .then(deleteUserSuccess)
        //            .catch(deleteUserError);
        //    })
        //};

        //$scope.remove = function (id,index){
        //    var removeUser = UserResourceFactory.delete(id);
        //    removeUser.success(function(response){
        //        $scope.flash = response.status;
        //        $scope.users.splice(index,1);
        //        $scope.users = UserResourceFactory.query();
        //    })
        //};

        //function showDetails(){
        //    $log.info('showDetails');
        //    var user = UserResourceFactory.get();
        //    user.then(function(data) {
        //        $scope.get = data;
        //        console.log(data);
        //    });
        //    $scope.select = function(user) {
        //        $scope.selected = user;
        //    };
        //    $scope.selected = {};
        //}
    }

})();
/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('EditUserFormStateFactory', EditUserFormStateFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    EditUserFormStateFactory.$inject = [
        // Custom
        'AddressModelFactory',
        'CompanyModelFactory',
        'CountryModelFactory',
        'LocalityModelFactory',
        'RegionModelFactory',
        'SettingsModelFactory',
        'UserModelFactory',
        'DriverModelFactory'
    ];

    function EditUserFormStateFactory (
        // Custom
        AddressModelFactory,
        CompanyModelFactory,
        CountryModelFactory,
        LocalityModelFactory,
        RegionModelFactory,
        SettingsModelFactory,
        UserModelFactory,
        DriverModelFactory
    ) {
        return {
            address : new AddressModelFactory(),
            country : new CountryModelFactory(),
            company : new CompanyModelFactory(),
            locality: new LocalityModelFactory(),
            region  : new RegionModelFactory(),
            settings: new SettingsModelFactory(),
            user    : new UserModelFactory(),
            driver  : new DriverModelFactory()
        };
    }
})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('RegistrationFormStateFactory', RegistrationFormStateFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	RegistrationFormStateFactory.$inject = [
		// Custom
		'AddressModelFactory',
		'CompanyModelFactory',
		'CountryModelFactory',
		'LocalityModelFactory',
		'RegionModelFactory',
		'SettingsModelFactory',
		'UserModelFactory',
		/** @author Lien Mergan */
        'DriverModelFactory'
	];

	function RegistrationFormStateFactory(
		// Custom
		AddressModelFactory,
		CompanyModelFactory,
		CountryModelFactory,
		LocalityModelFactory,
		RegionModelFactory,
		SettingsModelFactory,
		UserModelFactory,
        DriverModelFactory
	) {
		return {
			address : new AddressModelFactory(),
			country : new CountryModelFactory(),
			company : new CompanyModelFactory(),
			locality: new LocalityModelFactory(),
			region  : new RegionModelFactory(),
			settings: new SettingsModelFactory(),
			user    : new UserModelFactory(),
            /** @author Lien Mergan */
            driver  : new DriverModelFactory()
		};
	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('RideFormStateFactory', RideFormStateFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideFormStateFactory.$inject = [
        // Custom
        'RideModelFactory',
        'UserModelFactory'
    ];

    function RideFormStateFactory(
        // Custom
        //DriverModelFactory,
        RideModelFactory,
        UserModelFactory
    ) {
        return {
            //driver  : new DriverModelFactory(),
            ride    : new RideModelFactory(),
            user    : new UserModelFactory()
        };
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('AddressModelFactory', AddressModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	AddressModelFactory.$inject = [
		// Custom
		//'CountryModelFactory'
	];

	function AddressModelFactory(
		// Custom
		//CountryModelFactory
	) {
		/**
		 * Address Model.
		 *
		 * @param data
		 * @constructor
		 */
		function Address(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// FK
			this.locality_id = data.locality_id || null;
			//this.country = null;

			// Properties
			this.street   = data.street || null;
			this.extended = data.extended || null;

			// Properties omitted from JSON by Angular due to `$$` prefix.
		}

		// Methods
		// =======

		return Address;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('CategoryModelFactory', CategoryModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	CategoryModelFactory.$inject = [
		'GoalModelFactory'
	];

	function CategoryModelFactory(
		GoalModelFactory
	) {
		/**
		 * Category Model. Category for goals.
		 *
		 * @param data
		 * @constructor
		 */
		function Category(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// Properties
			this.description = data.description || null;
			this.name        = data.name || null;
			this.goals       = this.$$addGoals(data.goals);
			this.order       = this.$$setOrder(data.order);
		}

		// Methods
		// =======

		Category.prototype.$$addGoals = function (goals) {
			if (angular.isDefined(goals) && angular.isArray(goals)) {
				return goals.map(function (element) {
					return new GoalModelFactory(element);
				})
			} else {
				return [];
			}
		};

		Category.prototype.$$setOrder = function (order) {
			return angular.isNumber(order) ? order : null;
		};

		return Category;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('CompanyModelFactory', CompanyModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	//CompanyModelFactory.$inject = [];

	function CompanyModelFactory() {
		/**
		 * Company Model.
		 *
		 * @param data
		 * @constructor
		 */
		function Company(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// Properties
			this.description = data.description || null;
			this.name        = data.name || null;
		}

		// Methods
		// =======

		return Company;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('CountryModelFactory', CountryModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	//CountryModelFactory.$inject = [];

	function CountryModelFactory() {
		/**
		 * Country Model.
		 *
		 * @param data
		 * @constructor
		 */
		function Country(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// Properties
			this.iso  = data.iso || null;
			this.name = data.name || null;
		}

		// Methods
		// =======

		return Country;
	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('DriverModelFactory', DriverModelFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DriverModelFactory.$inject = [
        // Angular
        '$log',
        // Custom
        'RideModelFactory'
    ];

    function DriverModelFactory(
        // Angular
        $log,
        // Custom
        RideModelFactory
    ) {
        /**
         * Driver Model.
         *
         * @param data
         * @constructor
         */
        function Driver(data) {
            data = data || {};

            moment.locale('en');

            // PK
            this.id = data.id || null;

            // FK
            this.user_id = data.user_id || null;
            this.ride_id = data.ride_id || null;

            // Properties
            this.role = data.role || null;
            this.driverseats      = data.driverseats || null;
            this.car  = data.car || null;
            this.rides = data.rides || [];

            // Properties omitted from JSON by Angular due to `$$` prefix.
            this.$$rides          = []; // All possible rides should be pushed to this array.
        }

        // Methods
        // =======
        Driver.prototype.$$addRide = function (chip) {
            var ride;

            if (angular.isObject(chip)) {
                ride = chip;
            } else {
                var data = { name: chip };
                ride = new RideModelFactory(data);

                // Add to complete list of interests (`md-no-cache` required on `md-autocomplete`)
                this.$$rides.push(ride);
            }
            return ride;
        };

        return Driver;
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('GoalModelFactory', GoalModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalModelFactory.$inject = [
		// Angular
		'$log',
		// Constants
		'TARGET_TYPES',
		// Custom
		'TargetCheckboxModelFactory',
		'TargetDurationModelFactory',
		'TargetRecurringCheckboxModelFactory'
	];

	function GoalModelFactory(
		// Angular
		$log,
		// Constants
		TARGET_TYPES,
		// Custom
		TargetCheckboxModelFactory,
		TargetDurationModelFactory,
		TargetRecurringCheckboxModelFactory
	) {
		// Note: Angular hides properties prefixed with `$$` when converting to JSON
		/**
		 * Goal Model.
		 *
		 * @param data
		 * @constructor
		 */
		function Goal(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// FK
			this.user_id     = data.user_id || null;
			this.category_id = data.category_id || null;

			// Properties
			this.name         = data.name || null;
			this.notes        = data.notes || null;
			this.in_progress  = data.in_progress || null;
			this.target       = null;
			this.target_class = null;

			// Run on construction
			this.$$addTarget(data.target_class, data.target);
		}

		// Methods
		// =======
		Goal.prototype.$$addTarget = function(targetClass, target) {
			switch (targetClass) {
				case TARGET_TYPES.a_Checkbox.value:
					this.$$addTargetCheckbox(target);
					break;
				case TARGET_TYPES.b_RecurringCheckbox.value:
					this.$$addTargetRecurringCheckbox(target);
					break;
				case TARGET_TYPES.c_Duration.value:
					this.$$addTargetDuration(target);
					break;
				default:
					$log.error('Unknown Target Class: ' + targetClass);
					break;
			}
		};

		Goal.prototype.$$addTargetCheckbox = function (target) {
			this.target = new TargetCheckboxModelFactory(target);
			this.target_class = TARGET_TYPES.a_Checkbox.value;
		};

		Goal.prototype.$$addTargetRecurringCheckbox = function (target) {
			this.target = new TargetRecurringCheckboxModelFactory(target);
			this.target_class = TARGET_TYPES.b_RecurringCheckbox.value;
		};

		Goal.prototype.$$addTargetDuration = function (target) {
			this.target = new TargetDurationModelFactory(target);
			this.target_class = TARGET_TYPES.c_Duration.value;
		};

		Goal.prototype.$$hasTargetTypeCheckbox = function () {
			return this.target_class === TARGET_TYPES.a_Checkbox.value;
		};

		Goal.prototype.$$hasTargetTypeRecurringCheckbox = function () {
			return this.target_class === TARGET_TYPES.b_RecurringCheckbox.value;
		};

		Goal.prototype.$$hasTargetTypeDuration = function () {
			return this.target_class === TARGET_TYPES.c_Duration.value;
		};

		return Goal;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('InterestModelFactory', InterestModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	InterestModelFactory.$inject = [
		// Angular
		'$log'
	];

	function InterestModelFactory(
		// Angular
		$log
	) {
		/**
		 * Interest Model. A user has interests.
		 *
		 * @param data
		 * @constructor
		 */
		function Interest(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// Properties
			this.name = data.name || null;

			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$name = angular.isDefined(data.name) ? angular.uppercase(data.name) : null; // Upper case name for searches.
		}

		// Methods
		// =======

		$log.log(Interest);

		return Interest;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('LocalityModelFactory', LocalityModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	//LocalityModelFactory.$inject = [];

	function LocalityModelFactory() {
		/**
		 * Locality Model. A locality is usually a city, village or municipality. An address has a locality and
		 * a locality belongs to a region.
		 *
		 * @param data
		 * @constructor
		 */
		function Locality(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// FK
			this.region_id = data.region_id || null;
			//this.country = null;

			// Properties
			this.postal_code  = data.postal_code || null;
			this.name = data.name || null;
		}

		// Methods
		// =======

		return Locality;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('MoodModelFactory', MoodModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	MoodModelFactory.$inject = [
		// Angular
		'$log',
		// Constants
		'MOOD_TYPES'
	];

	function MoodModelFactory(
		// Angular
		$log,
		// Constants
		MOOD_TYPES
	) {
		// Note: Angular hides properties prefixed with `$$` when converting to JSON
		/**
		 * Mood Model. A user has moods.
		 *
		 * @param data
		 * @constructor
		 */
		function Mood(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// FK
			this.user_id = data.user_id || null;

			// Properties
			this.feeling = data.feeling || null;
		}

		// Methods
		// =======
		Mood.prototype.$$hasFeeling = function (feeling) {
			return this.feeling === feeling;
		};

		Mood.prototype.$$setFeeling = function (feeling) {
			switch (feeling) {
				case MOOD_TYPES.a_FeelingEnergized.value:
				case MOOD_TYPES.b_FeelingGood.value:
				case MOOD_TYPES.c_FeelingOk.value:
				case MOOD_TYPES.d_FeelingTired.value:
				case MOOD_TYPES.e_FeelingExhausted.value:
					this.feeling = feeling;
					break;
				default:
					this.feeling = null;
					break;
			}
		};

		return Mood;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('RegionModelFactory', RegionModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	//RegionModelFactory.$inject = [];

	function RegionModelFactory() {
		/**
		 * Region Model. A locality belongs to a region and a region belongs to a country.
		 *
		 * @param data
		 * @constructor
		 */
		function Region(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// FK
			this.country_id = data.country_id || null;

			// Properties
			this.iso  = data.iso || null;
			this.name = data.name || null;
		}

		return Region;
	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('RideModelFactory', RideModelFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideModelFactory.$inject = [
        // Angular
        '$log'
    ];

    function RideModelFactory(
        // Angular
        $log
    ) {
        /**
         * Ride Model. A driver has rides.
         *
         * @param data
         * @constructor
         */
        function Ride(data) {
            data = data || {};

            // PK
            this.id = data.id || null;

            // FK
            this.user_id = data.user_id || null;

            // Properties
            this.ride_from = data.ride_from || null;
            this.ride_to = data.ride_to || null;
            this.ride_starttime = data.ride_starttime || null;
            this.ride_endtime = data.ride_endtime || null;
            this.ride_price = data.ride_price || null;
            this.ride_comments = data.ride_comments || null;


            // Properties omitted from JSON by Angular due to `$$` prefix.
            //this.$$name = angular.isDefined(data.name) ? angular.uppercase(data.name) : null; // Upper case name for searches.
        }

        // Methods
        // =======

        $log.log(Ride);

        return Ride;
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('SettingsModelFactory', SettingsModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	//SettingsModelFactory.$inject = [];

	function SettingsModelFactory() {
		/**
		 * Settings Model. Contains all user settings.
		 *
		 * @param data
		 * @constructor
		 */
		function Settings(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// FK
			this.user_id = data.user_id || null;

			// Properties
			this.colour_palette      = data.colour_palette      || 'A';
			this.share_address       = data.share_address       || false;
			this.share_birthday      = data.share_birthday      || false;
			this.share_email         = data.share_email         || false;
			this.share_gender        = data.share_gender        || false;
			this.share_interests     = data.share_interests     || false;
			this.share_locality      = data.share_locality      || false;
			this.share_location      = data.share_location      || false;
			this.share_mobile        = data.share_mobile        || false;
			this.share_picture       = data.share_picture       || false;
			this.share_progress      = data.share_progress      || false;
			this.show_notifications  = data.show_notifications  || true;
			this.want_to_collaborate = data.want_to_collaborate || false;
		}

		// Methods
		// =======

		return Settings;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetCheckboxModelFactory', TargetCheckboxModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetCheckboxModelFactory.$inject = [
		// Angular
		'$log'
	];

	function TargetCheckboxModelFactory(
		// Angular
		$log
	) {
		/**
		 * Target Model of type checkbox. A goal has a target.
		 *
		 * @param data
		 * @constructor
		 */
		function TargetCheckbox(data) {
			data = data || {};

			moment.locale('en'); // Moment.js

			// PK
			this.id = data.id || null;

			// Properties
			this.deadline_date     = data.deadline_date || null;
			this.deadline_time     = data.deadline_time || null;
			this.deadline_reminder = data.deadline_reminder || false;
			this.achieved_at       = data.achieved_at || null;

			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$deadline_date = this.$$convertDate(data.deadline_date);
			this.$$deadline_time = this.$$convertTime(data.deadline_time);
			this.$$achieved_at   = data.achieved_at ? true : null;
		}

		// Methods
		// =======
		TargetCheckbox.prototype.$$changedAchievedAt = function () {
			$log.log('changed:', this.$$achieved_at);
			this.achieved_at = this.$$achieved_at ? moment().format('YYYY-MM-DD HH:mm:ss') : null;
		};

		TargetCheckbox.prototype.$$convertDate = function (dateString) {
			if (angular.isDefined(dateString) && dateString !== null) {
				return moment(dateString).toDate();
			} else {
				return null;
			}
		};

		TargetCheckbox.prototype.$$convertTime = function (timeString) {
			if (angular.isDefined(timeString) && timeString !== null) {
				var timeArray = timeString.split(':');
				var time = {
					hours      : timeArray[0],
					minute     : timeArray[1],
					second     : timeArray[2],
					millisecond: 0
				};

				return moment(time).toDate();
			} else {
				return null;
			}
		};

		TargetCheckbox.prototype.$$getSetDeadlineDate = function (newValue) {
			if (arguments.length) {
				this.deadline_date = moment(newValue).format('YYYY-MM-DD');
				return this.$$deadline_date = newValue;
			} else {
				return this.$$deadline_date;
			}
		};

		TargetCheckbox.prototype.$$getSetDeadlineTime = function (newValue) {
			if (arguments.length) {
				this.deadline_time = moment(newValue).format('HH:mm:ss');
				return this.$$deadline_time = newValue;
			} else {
				return this.$$deadline_time;
			}
		};

		return TargetCheckbox;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetDurationModelFactory', TargetDurationModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetDurationModelFactory.$inject = [
		// Angular
		'$filter',
		'$log',
		// Custom
		'INCREMENT_TYPES',
		'UpdateDurationModelFactory'
	];

	function TargetDurationModelFactory(
		// Angular
		$filter,
		$log,
		// Custom
		INCREMENT_TYPES,
		UpdateDurationModelFactory
	) {
		/**
		 * Target Model of type duration. A goal has a target.
		 *
		 * @param data
		 * @constructor
		 */
		function TargetDuration(data) {
			data = data || {};

			moment.locale('en'); // Moment.js

			// PK
			this.id = data.id || null;

			// FK
			this.update = data.update || null;

			// Properties
			this.time_estimated = data.time_estimated || null;
			this.time_increment = data.time_increment || null;

			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$duration       = null;
			this.$$time_increment = 0;

			// Run on construction
			this.$$addUpdate();
			this.$$updateDuration();
		}

		// Methods
		// =======
		TargetDuration.prototype.$$addUpdate = function () {
			if (!angular.isObject(this.update)) {
				this.update = new UpdateDurationModelFactory();
			}
		};

		TargetDuration.prototype.$$setTimeIncrement = function (time_increment) {
			var increment = _.find(INCREMENT_TYPES, function (item) {
				return item.value === time_increment;
			});

			this.$$time_increment = increment.minutes;
		};

		TargetDuration.prototype.$$updateDuration = function () {
			if (angular.isNumber(this.time_estimated) && angular.isString(this.time_increment)) {
				this.$$setTimeIncrement(this.time_increment);

				var minutes = this.time_estimated * this.$$time_increment;
				var duration = moment.duration(minutes, 'minutes');
				if (minutes < 60) {
					this.$$duration  = duration.asMinutes() + ' minutes';
				} else
				if (minutes < 240) {
					this.$$duration  = duration.asHours();
					this.$$duration += (duration === 1) ? ' hour' : ' hours';
				} else {
					this.$$duration = (duration.asDays() === 1) ? duration.asDays() + ' day' : $filter('number')(duration.asDays(), 2) + ' days';
				}
			} else {
				this.$$duration = null;
			}
		};

		return TargetDuration;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetRecurringCheckboxModelFactory', TargetRecurringCheckboxModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetRecurringCheckboxModelFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'UpdateRecurringCheckboxModelFactory'
	];

	function TargetRecurringCheckboxModelFactory(
		// Angular
		$log,
		// Custom
		UpdateRecurringCheckboxModelFactory
	) {
		/**
		 * Target Model of type recurring checkbox. A goal has a target.
		 *
		 * @param data
		 * @constructor
		 */
		function TargetRecurringCheckbox(data) {
			data = data || {};

			moment.locale('en'); // Moment.js

			// PK
			this.id = data.id || null;

			// FK
			this.update = data.update || null;

			// Properties
			this.deadline_date     = data.deadline_date || null;
			this.deadline_time     = data.deadline_time || null;
			this.deadline_reminder = data.deadline_reminder || false;
			this.repeat_deadline   = data.repeat_deadline || null;
			this.repeat_until_date = data.repeat_until_date || null;
			this.repeat_until_time = data.repeat_until_time || null;

			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$deadline_date     = this.$$convertDate(data.deadline_date);
			this.$$deadline_time     = this.$$convertTime(data.deadline_time);
			this.$$repeat_until_date = this.$$convertDate(data.repeat_until_date);
			this.$$repeat_until_time = this.$$convertTime(data.repeat_until_time);

			// Run on construction
			this.$$addUpdate();
		}

		// Methods
		// =======
		TargetRecurringCheckbox.prototype.$$addUpdate = function () {
			if (!angular.isObject(this.update)) {
				this.update = new UpdateRecurringCheckboxModelFactory();
			}
		};

		TargetRecurringCheckbox.prototype.$$convertDate = function (dateString) {
			if (angular.isDefined(dateString) && dateString !== null) {
				return moment(dateString).toDate();
			} else {
				return null;
			}
		};

		TargetRecurringCheckbox.prototype.$$convertTime = function (timeString) {
			if (angular.isDefined(timeString) && timeString !== null) {
				var timeArray = timeString.split(':');
				var time = {
					hours      : timeArray[0],
					minute     : timeArray[1],
					second     : timeArray[2],
					millisecond: 0
				};

				return moment(time).toDate();
			} else {
				return null;
			}
		};

		TargetRecurringCheckbox.prototype.$$getSetDeadlineDate = function (newValue) {
			if (arguments.length) {
				this.deadline_date = moment(newValue).format('YYYY-MM-DD');
				return this.$$deadline_date = newValue;
			} else {
				return this.$$deadline_date;
			}
		};

		TargetRecurringCheckbox.prototype.$$getSetDeadlineTime = function (newValue) {
			if (arguments.length) {
				this.deadline_time = moment(newValue).format('HH:mm:ss');
				return this.$$deadline_time = newValue;
			} else {
				return this.$$deadline_time;
			}
		};

		TargetRecurringCheckbox.prototype.$$getSetRepeatUntilDate = function (newValue) {
			if (arguments.length) {
				this.repeat_until_date = moment(newValue).format('YYYY-MM-DD');
				return this.$$repeat_until_date = newValue;
			} else {
				return this.$$repeat_until_date;
			}
		};

		TargetRecurringCheckbox.prototype.$$getSetRepeatUntilTime = function (newValue) {
			if (arguments.length) {
				this.repeat_until_time = moment(newValue).format('HH:mm:ss');
				return this.$$repeat_until_time = newValue;
			} else {
				return this.$$repeat_until_time;
			}
		};

		return TargetRecurringCheckbox;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UpdateDurationModelFactory', UpdateDurationModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UpdateDurationModelFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'INCREMENT_TYPES'
	];

	function UpdateDurationModelFactory(
		// Angular
		$log,
		// Custom
		INCREMENT_TYPES
	) {
		/**
		 * Update Model of type duration. A target has updates.
		 *
		 * @param data
		 * @constructor
		 */
		function UpdateDuration(data) {
			data = data || {};

			// PK
			this.id = data.id || null;

			// FK
			this.target_id = data.target_id || null;

			// Properties
			this.time_incrementation = data.time_incrementation || 0;

			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$time_increment      = 0;
			this.$$time_incrementation = '';

			// Run on construct
			this.$$changeTimeIncrementation();
		}

		// Methods
		// =======
		UpdateDuration.prototype.$$changeTimeIncrementation = function () {
			$log.log('$$changeTimeIncrementation'
,				this.time_incrementation, this.$$time_increment
			);
			this.$$time_incrementation = this.time_incrementation * this.$$time_increment;
		};

		UpdateDuration.prototype.$$setTimeIncrement = function (time_increment) {
			var increment = _.find(INCREMENT_TYPES, function (item) {
				return item.value === time_increment;
			});

			this.$$time_increment = increment.minutes;
		};

		UpdateDuration.prototype.$$timeDecrement = function (event) {
			event.preventDefault();

			var min = 0;
			if (min < this.time_incrementation) {
				this.time_incrementation--;
			} else {
				this.time_incrementation = min;
			}
			this.$$changeTimeIncrementation();
		};

		UpdateDuration.prototype.$$timeIncrement = function (event) {
			event.preventDefault();

			var max = 200;
			if (this.time_incrementation < max) {
				this.time_incrementation++;
			} else {
				this.time_incrementation = max;
			}
			this.$$changeTimeIncrementation();
		};

		return UpdateDuration;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UpdateRecurringCheckboxModelFactory', UpdateRecurringCheckboxModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UpdateRecurringCheckboxModelFactory.$inject = [
		// Angular
		'$log'
	];

	function UpdateRecurringCheckboxModelFactory(
		// Angular
		$log
	) {
		/**
		 * Update Model of type recurring checkbox. A target has updates.
		 *
		 * @param data
		 * @constructor
		 */
		function UpdateRecurringCheckbox(data) {
			data = data || {};

			moment.locale('en');

			// PK
			this.id = data.id || null;

			// FK
			this.target_id = data.target_id || null;

			// Properties
			this.achieved_at = data.achieved_at || null;

			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$achieved_at = data.achieved_at ? true : null;
		}

		// Methods
		// =======
		UpdateRecurringCheckbox.prototype.$$changedAchievedAt = function () {
			$log.log('changed:', this.$$achieved_at);
			this.achieved_at = this.$$achieved_at ? moment().format('YYYY-MM-DD HH:mm:ss') : null;
		};

		return UpdateRecurringCheckbox;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UserModelFactory', UserModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UserModelFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'InterestModelFactory'
	];

	function UserModelFactory(
		// Angular
		$log,
		// Custom
		InterestModelFactory
	) {
		/**
		 * User Model.
		 *
		 * @param data
		 * @constructor
		 */
		function User(data) {
			data = data || {};

			moment.locale('en');

			// PK
			this.id = data.id || null;

			// Properties
			this.name      = data.name || null;
			this.password  = data.password || null;
			this.birthday  = data.birthday || null;
			this.interests = data.interests || [];

			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$birthday           = this.$$toDate(this.birthday);
			this.$$interests          = []; // All possible interests should be pushed to this array.
			this.$$interestSearchText = '';
			this.$$interestSelected   = null;
		}

		// Methods
		// =======
		User.prototype.$$addInterest = function (chip) {
			var interest;

			if (angular.isObject(chip)) {
				interest = chip;
			} else {
				var data = { name: chip };
				interest = new InterestModelFactory(data);

				// Add to complete list of interests (`md-no-cache` required on `md-autocomplete`)
				this.$$interests.push(interest);
			}

			return interest;
		};

		User.prototype.$$getSetBirthday = function (newValue) {
			if (arguments.length) {
				this.birthday = moment(newValue).format('YYYY-MM-DD');
				return this.$$birthday = newValue;
			} else {
				return this.$$birthday;
			}
		};

		User.prototype.$$interestFilter = function (interestSearchText) {
			var searchText = angular.uppercase(interestSearchText);

			return function(interest) {
				return (interest.$$name.indexOf(searchText) === 0);
			};
		};

		User.prototype.$$interestSearchTextChanged = function () {};

		User.prototype.$$interestSelectedItemChange = function () {};

		User.prototype.$$searchInterest = function () {
			return this.$$interests.filter(this.$$interestFilter(this.$$interestSearchText));
		};

		User.prototype.$$toDate = function (dateString) {
			return angular.isString(dateString) ? moment(dateString).toDate() : null;
		};

		return User;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('AddressResourceFactory', AddressResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	AddressResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'UriFactory'
	];

	function AddressResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('addresses/:addressId');

		var paramDefaults = {
			addressId: '@id'
		};

		var actions = {
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('AuthUserResourceFactory', AuthUserResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	AuthUserResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'UriFactory'
	];

	function AuthUserResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('auth');

		var paramDefaults = {};

		var actions = {
			'login': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('CategoryResourceFactory', CategoryResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	CategoryResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'UriFactory'
	];

	function CategoryResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('categories/:categoryId');

		var paramDefaults = {
			categoryId: '@id'
		};

		var actions = {
			'queryWithGoals': {
				isArray: false,
				method: 'GET',
				params: {
					'include[]': 'goals.target'
				},
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('CompanyResourceFactory', CompanyResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	CompanyResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'UriFactory'
	];

	function CompanyResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('companies/:companyId');

		var paramDefaults = {
			companyId: '@id'
		};

		var actions = {
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('CountryResourceFactory', CountryResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	CountryResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'CountryResponseInterceptorFactory',
		'GenericResponseTransformerFactory',
		'UriFactory'
	];

	function CountryResourceFactory(
		// Angular
		$resource,
		// Custom
		CountryResponseInterceptorFactory,
		GenericResponseTransformerFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('countries/:countryId');

		var paramDefaults = {
			countryId: '@id'
		};

		var actions = {
			'query': {
				interceptor: {
					response: CountryResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				transformResponse: GenericResponseTransformerFactory
			},
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */
/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('DriverResourceFactory', DriverResourceFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DriverResourceFactory.$inject = [
        // Angular
        '$resource',
        // Custom
        'GenericResponseTransformerFactory',
        'UriFactory',
        'DriverResponseInterceptorFactory'
    ];

    function DriverResourceFactory(
        // Angular
        $resource,
        // Custom
        GenericResponseTransformerFactory,
        UriFactory,
        DriverResponseInterceptorFactory
    ) {
        var url = UriFactory.getApi('drivers/:driverId');

        var paramDefaults = {
            driverId: '@id'
        };

        var actions = {
            'get': {
                interceptor: {
                    response: DriverResponseInterceptorFactory
                },
                isArray: false,
                method: 'GET',
                transformResponse: GenericResponseTransformerFactory
            },
            'query': {
                interceptor: {
                    response: DriverResponseInterceptorFactory
                },
                isArray: true,
                method: 'GET',
                transformResponse: GenericResponseTransformerFactory
            },
            'save': {
                isArray: false,
                method: 'POST',
                transformResponse: GenericResponseTransformerFactory
            },
            'update': {
                interceptor: {
                    response: DriverResponseInterceptorFactory
                },
                isArray: false,
                method: 'PUT',
                transformResponse: GenericResponseTransformerFactory
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('GoalResourceFactory', GoalResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'GoalResponseInterceptorFactory',
		'UriFactory'
	];

	function GoalResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		GoalResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('goals/:goalId');

		var paramDefaults = {
			goalId: '@id'
		};

		var actions = {
			'update': {
				interceptor: {
					response: GoalResponseInterceptorFactory
				},
				isArray: false,
				method: 'PUT',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('InterestResourceFactory', InterestResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	InterestResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'InterestResponseInterceptorFactory',
		'UriFactory'
	];

	function InterestResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		InterestResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('interests/:interestId');

		var paramDefaults = {
			interestId: '@id'
		};

		var actions = {
			query: {
				interceptor: {
					response: InterestResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				transformResponse: [GenericResponseTransformerFactory]
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('LocalityResourceFactory', LocalityResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	LocalityResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'LocalityResponseInterceptorFactory',
		'UriFactory'
	];

	function LocalityResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		LocalityResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('localities/:localityId');

		var paramDefaults = {
			localityId: '@id'
		};

		var actions = {
			'getWithRegion': {
				interceptor: {
					response: LocalityResponseInterceptorFactory
				},
				method: 'GET',
				params: {
					'include[]': 'region'
				}
			},
			'getWithCountry': {
				interceptor: {
					response: LocalityResponseInterceptorFactory
				},
				method: 'GET',
				params: {
					'include[]': 'region.country'
				}
			},
			'query': {
				interceptor: {
					response: LocalityResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				transformResponse: GenericResponseTransformerFactory
			},
			'queryWithRegion': {
				interceptor: {
					response: LocalityResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				params: {
					'include[]': 'region'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'queryWithCountry': {
				interceptor: {
					response: LocalityResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				params: {
					'include[]': 'region.country'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('LocationResourceFactory', LocationResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	LocationResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'UriFactory'
	];

	function LocationResourceFactory(
		// Angular
		$resource,
		// Custom
		UriFactory
	) {
		var url = UriFactory.getApi('locations/:locationId');

		var paramDefaults = {
			locationId: '@id'
		};

		var actions = {
			'getWithAddress': {
				method: 'GET',
				params: {
					'include[]': 'address'
				}
			},
			'queryWithAddress': {
				isArray: false,
				method: 'GET',
				params: {
					'include[]': 'address'
				}
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('RegionResourceFactory', RegionResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	RegionResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'RegionResponseInterceptorFactory',
		'UriFactory'
	];

	function RegionResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		RegionResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('regions/:regionId');

		var paramDefaults = {
			regionId: '@id'
		};

		var actions = {
			'getWithCountry': {
				method: 'GET',
				params: {
					'include[]': 'country'
				}
			},
			'query': {
				interceptor: {
					response: RegionResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				transformResponse: GenericResponseTransformerFactory
			},
			'queryWithCountry': {
				interceptor: {
					response: RegionResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				params: {
					'include[]': 'country'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('RideResourceFactory', RideResourceFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideResourceFactory.$inject = [
        // Angular
        '$resource',
        // Custom
        'GenericResponseTransformerFactory',
        'RideResponseInterceptorFactory',
        'UriFactory'
    ];

    function RideResourceFactory(
        // Angular
        $resource,
        // Custom
        GenericResponseTransformerFactory,
        RideResponseInterceptorFactory,
        UriFactory
    ) {
        var url = UriFactory.getApi('rides/:rideId');

        var paramDefaults = {
            rideId: '@id'
        };

        var actions = {
            'query': {
                interceptor: {
                    response: RideResponseInterceptorFactory
                },
                isArray: true,
                method: 'GET',
                transformResponse: GenericResponseTransformerFactory
            },
            'save': {
            isArray: false,
                method: 'POST',
                transformResponse: GenericResponseTransformerFactory
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('StatisticResourceFactory', StatisticResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	StatisticResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'UriFactory'
	];

	function StatisticResourceFactory(
		// Angular
		$resource,
		// Custom
		UriFactory
	) {
		var url = UriFactory.getApi('statistics/:statisticId');

		var paramDefaults = {
			statisticId: '@id'
		};

		var actions = {};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetCheckboxResourceFactory', TargetCheckboxResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetCheckboxResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'TargetCheckboxResponseInterceptorFactory',
		'UriFactory'
	];

	function TargetCheckboxResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		TargetCheckboxResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('targets/checkbox/:targetCheckboxId');

		var paramDefaults = {
			targetCheckboxId: '@id'
		};

		var actions = {
			'update': {
				interceptor: {
					response: TargetCheckboxResponseInterceptorFactory
				},
				isArray: false,
				method: 'PUT',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetDurationResourceFactory', TargetDurationResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetDurationResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'TargetDurationResponseInterceptorFactory',
		'UriFactory'
	];

	function TargetDurationResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		TargetDurationResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('targets/duration/:targetDurationId');

		var paramDefaults = {
			targetDurationId: '@id'
		};

		var actions = {
			'update': {
				interceptor: {
					response: TargetDurationResponseInterceptorFactory
				},
				isArray: false,
				method: 'PUT',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetRecurringCheckboxResourceFactory', TargetRecurringCheckboxResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetRecurringCheckboxResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'TargetRecurringCheckboxResponseInterceptorFactory',
		'UriFactory'
	];

	function TargetRecurringCheckboxResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		TargetRecurringCheckboxResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('targets/recurringcheckbox/:targetRecurringCheckboxId');

		var paramDefaults = {
			targetRecurringCheckboxId: '@id'
		};

		var actions = {
			'update': {
				interceptor: {
					response: TargetRecurringCheckboxResponseInterceptorFactory
				},
				isArray: false,
				method: 'PUT',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UpdateDurationResourceFactory', UpdateDurationResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UpdateDurationResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'UriFactory'
	];

	function UpdateDurationResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('updates/duration/:updateDurationId');

		var paramDefaults = {
			updateDurationId: '@id'
		};

		var actions = {
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UpdateRecurringCheckboxResourceFactory', UpdateRecurringCheckboxResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UpdateRecurringCheckboxResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'UriFactory'
	];

	function UpdateRecurringCheckboxResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('updates/recurringcheckbox/:updateRecurringCheckboxId');

		var paramDefaults = {
			updateRecurringCheckboxId: '@id'
		};

		var actions = {
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UserCategoryResourceFactory', UserCategoryResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UserCategoryResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'UriFactory'
	];

	function UserCategoryResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		UriFactory
	) {

		var url = UriFactory.getApi('users/:userId/categories/:categoryId');

		var paramDefaults = {
			userId    : '@id',
			categoryId: '@id'
		};

		var actions = {
			'getWithGoals': {
				isArray: false,
				method: 'GET',
				params: {
					'include[]': 'goals'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'getWithTarget': {
				isArray: false,
				method: 'GET',
				params: {
					'include[]': 'goals.target'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'queryWithGoals': {
				isArray: true,
				method: 'GET',
				params: {
					'include[]': 'goals'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'queryWithTarget': {
				isArray: true,
				method: 'GET',
				params: {
					'include[]': 'goals.target'
				},
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UserGoalResourceFactory', UserGoalResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UserGoalResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'GoalResponseInterceptorFactory',
		'UriFactory'
	];

	function UserGoalResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		GoalResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('users/:userId/goals/:goalId');

		var paramDefaults = {
			userId: '@id',
			goalId: '@id'
		};

		var actions = {
			'getWithTarget'  : {
				interceptor: {
					response: GoalResponseInterceptorFactory
				},
				method: 'GET',
				params: {
					'include[]': 'target'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'queryInProgress': {
				isArray: true,
				method: 'GET',
				params: {
					'filter[in_progress]': true
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'queryWithTarget': {
				interceptor: {
					response: GoalResponseInterceptorFactory
				},
				isArray: false,
				method: 'GET',
				params: {
					'include[]': 'target'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'update': {
				interceptor: {
					response: GoalResponseInterceptorFactory
				},
				isArray: false,
				method: 'PUT',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UserMoodResourceFactory', UserMoodResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UserMoodResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'MoodResponseInterceptorFactory',
		'UriFactory'
	];

	function UserMoodResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		MoodResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('users/:userId/moods/:moodId');

		var paramDefaults = {
			userId: '@id',
			moodId: '@id'
		};

		var actions = {
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			},
			'statistics': {
				interceptor: {
					response: MoodResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UserSettingsResourceFactory', UserSettingsResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UserSettingsResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'SettingsResponseInterceptorFactory',
		'UriFactory'
	];

	function UserSettingsResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		SettingsResponseInterceptorFactory,
		UriFactory
	) {
		var url = UriFactory.getApi('users/:userId/settings/:settingsId');

		var paramDefaults = {
			userId    : '@id',
			settingsId: '@id'
		};

		var actions = {
			'get': {
				interceptor: {
					response: SettingsResponseInterceptorFactory
				},
				isArray: false,
				method: 'GET',
				transformResponse: GenericResponseTransformerFactory
			},
			'query': {
				interceptor: {
					response: SettingsResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				transformResponse: GenericResponseTransformerFactory
			},
			'queryLast': {
				interceptor: {
					response: SettingsResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				params: {
					'limit'   : 1,
					'sort[id]': 'desc'
				},
				transformResponse: GenericResponseTransformerFactory
			},
			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			},
			'update': {
				interceptor: {
					response: SettingsResponseInterceptorFactory
				},
				isArray: false,
				method: 'PUT',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UserResourceFactory', UserResourceFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UserResourceFactory.$inject = [
		// Angular
		'$resource',
		// Custom
		'GenericResponseTransformerFactory',
		'UriFactory',
		'UserResponseInterceptorFactory'
	];

	function UserResourceFactory(
		// Angular
		$resource,
		// Custom
		GenericResponseTransformerFactory,
		UriFactory,
		UserResponseInterceptorFactory
	) {
		var url = UriFactory.getApi('users/:userId');

		var paramDefaults = {
			userId: '@id'
		};

		var actions = {
			'get': {
				interceptor: {
					response: UserResponseInterceptorFactory
				},
				isArray: true,
				method: 'GET',
				transformResponse: GenericResponseTransformerFactory
			},

            /**
             * @author    Lien Mergan
             * @copyright Copyright © 2014-2015 Artevelde University College Ghent
             * show, query and delete method
             */
            'show': {
                interceptor: {
                    response: UserResponseInterceptorFactory
                },
                isArray: false,
                method: 'GET',
                transformResponse: GenericResponseTransformerFactory
            },
            'query': {
                interceptor: {
                    response: UserResponseInterceptorFactory
                },
                isArray: true,
                method: 'GET',
                transformResponse: GenericResponseTransformerFactory
            },
            'delete': {
                interceptor: {
                    response: UserResponseInterceptorFactory
                },
                isArray: false,
                method: 'DELETE'
                //transformResponse: GenericResponseTransformerFactory
            },

			'save': {
				isArray: false,
				method: 'POST',
				transformResponse: GenericResponseTransformerFactory
			},
			'update': {
				interceptor: {
					response: UserResponseInterceptorFactory
				},
				isArray: false,
				method: 'PUT',
				transformResponse: GenericResponseTransformerFactory
			}
		};

		return $resource(url, paramDefaults, actions);
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('AppCtrl', AppCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	AppCtrl.$inject = [
		// Angular
		'$log',
		'$scope',
		// Angular Material Design
		'$mdSidenav'
	];

	function AppCtrl(
		// Angular
		$log,
		$scope,
		// Angular Material Design
		$mdSidenav
	) {
		$log.info('AppCtrl');

		// Add the function to the AppCtrl scope. It will be inherited by the child controller scope
		$scope.openLeftSidenav = openLeftSidenav;

		// Functions
		// =========
		function openLeftSidenav() {
			$log.log('openLeftSidenav');

			$mdSidenav('left')
				.open();
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('BottomSheetCtrl', BottomSheetCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	BottomSheetCtrl.$inject = [
		'$scope',
		'$mdBottomSheet'
	];

	function BottomSheetCtrl(
		$scope,
		$mdBottomSheet
	) {

		$scope.items = [
			{ name: 'Hangout', icon: 'hangout' },
			{ name: 'Mail', icon: 'mail' },
			{ name: 'Message', icon: 'message' },
			{ name: 'Copy', icon: 'copy' },
			{ name: 'Facebook', icon: 'facebook' },
			{ name: 'Twitter', icon: 'twitter' },
		];
		$scope.listItemClick = function($index) {
			var clickedItem = $scope.items[$index];
			//$mdBottomSheet.hide(clickedItem);
		};

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('DialogCtrl', DialogCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	DialogCtrl.$inject = [
		// Angular Material Design
		'$mdDialog'
	];

	function DialogCtrl(
		// Angular Material Design
		$mdDialog
	) {
		// ViewModel
		var vm = this;

		vm.hide = hide;

		// Functions
		// =========
		function hide() {
			$mdDialog.hide();
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('LeftCtrl', LeftCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	LeftCtrl.$inject = [
		// Angular
		'$log',
		// Angular Material Design
		'$mdSidenav'
	];

	function LeftCtrl(
		// Angular
		$log,
		// Angular Material Design
		$mdSidenav
	) {
		$log.info('LeftCtrl');
		// ViewModel
		var vm = this;

		vm.close = close;

		// Functions
		// =========
		function close() {
			$mdSidenav('left')
				.close()
				.then(function(){
					$log.log('Left sidenav has done closing!');
				});

		}

	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuControllers')
		.controller('ToastCtrl', ToastCtrl);

	// Inject dependencies into constructor (needed when JS minification is applied).
	ToastCtrl.$inject = [
		// Angular Material Design
		'$mdToast'
	];

	function ToastCtrl(
		// Angular Material Design
		$mdToast
	) {
		// ViewModel
		var vm = this;

		vm.hide = hide;

		// Functions
		// =========
		function hide() {
			$mdToast.hide();
		}
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('CountryResponseInterceptorFactory', CountryResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	CountryResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'CountryModelFactory'
	];

	function CountryResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		CountryModelFactory
	) {
		function interceptor(response) {
			//$log.info('CountryResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new CountryModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('DriverResponseInterceptorFactory', DriverResponseInterceptorFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DriverResponseInterceptorFactory.$inject = [
        // Angular
        '$log',
        // Custom
        'DriverModelFactory'
    ];

    function DriverResponseInterceptorFactory(
        // Angular
        $log,
        // Custom
        DriverModelFactory
    ) {
        function interceptor(response) {
            //$log.info('UserResponseInterceptor:', response);
            if (angular.isArray(response.data)) {
                response.resource = response.data.map(transformToModel);
            } else {
                response.resource = transformToModel(response.data);
            }

            return response;
        }

        function transformToModel(data) {
            return new DriverModelFactory(data);
        }

        return interceptor;
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('GoalResponseInterceptorFactory', GoalResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GoalResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'GoalModelFactory'
	];

	function GoalResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		GoalModelFactory
	) {
		function interceptor(response) {
			//$log.info('GoalResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new GoalModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('InterestResponseInterceptorFactory', InterestResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	InterestResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'InterestModelFactory'
	];

	function InterestResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		InterestModelFactory
	) {
		function interceptor(response) {
			//$log.info('InterestResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new InterestModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('LocalityResponseInterceptorFactory', LocalityResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	LocalityResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'LocalityModelFactory'
	];

	function LocalityResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		LocalityModelFactory
	) {
		function interceptor(response) {
			//$log.info('LocalityResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new LocalityModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('MoodResponseInterceptorFactory', MoodResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	MoodResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'MoodModelFactory'
	];

	function MoodResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		MoodModelFactory
	) {
		function interceptor(response) {
			//$log.info('MoodResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new MoodModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('RegionResponseInterceptorFactory', RegionResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	RegionResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'RegionModelFactory'
	];

	function RegionResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		RegionModelFactory
	) {
		function interceptor(response) {
			//$log.info('RegionResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new RegionModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Lien Mergan
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 */

;(function () { 'use strict';

    angular.module('smuServices')
        .factory('RideResponseInterceptorFactory', RideResponseInterceptorFactory);

    // Inject dependencies into constructor (needed when JS minification is applied).
    RideResponseInterceptorFactory.$inject = [
        // Angular
        '$log',
        // Custom
        'RideModelFactory'
    ];

    function RideResponseInterceptorFactory(
        // Angular
        $log,
        // Custom
        RideModelFactory
    ) {
        function interceptor(response) {
            //$log.info('UserResponseInterceptor:', response);
            if (angular.isArray(response.data)) {
                response.resource = response.data.map(transformToModel);
            } else {
                response.resource = transformToModel(response.data);
            }

            return response;
        }

        function transformToModel(data) {
            return new RideModelFactory(data);
        }

        return interceptor;
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('SettingsResponseInterceptorFactory', SettingsResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	SettingsResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'SettingsModelFactory'
	];

	function SettingsResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		SettingsModelFactory
	) {
		function interceptor(response) {
			//$log.info('UserResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new SettingsModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetCheckboxResponseInterceptorFactory', TargetCheckboxResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetCheckboxResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'TargetCheckboxModelFactory'
	];

	function TargetCheckboxResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		TargetCheckboxModelFactory
	) {
		function interceptor(response) {
			//$log.info('TargetCheckboxResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new TargetCheckboxModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetDurationResponseInterceptorFactory', TargetDurationResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetDurationResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'TargetDurationModelFactory'
	];

	function TargetDurationResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		TargetDurationModelFactory
	) {
		function interceptor(response) {
			//$log.info('TargetDurationResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new TargetDurationModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('TargetRecurringCheckboxResponseInterceptorFactory', TargetRecurringCheckboxResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	TargetRecurringCheckboxResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'TargetRecurringCheckboxModelFactory'
	];

	function TargetRecurringCheckboxResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		TargetRecurringCheckboxModelFactory
	) {
		function interceptor(response) {
			//$log.info('TargetRecurringCheckboxResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new TargetRecurringCheckboxModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('UserResponseInterceptorFactory', UserResponseInterceptorFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	UserResponseInterceptorFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'UserModelFactory'
	];

	function UserResponseInterceptorFactory(
		// Angular
		$log,
		// Custom
		UserModelFactory
	) {
		function interceptor(response) {
			//$log.info('UserResponseInterceptor:', response);
			if (angular.isArray(response.data)) {
				response.resource = response.data.map(transformToModel);
			} else {
				response.resource = transformToModel(response.data);
			}

			return response;
		}

		function transformToModel(data) {
			return new UserModelFactory(data);
		}

		return interceptor;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('GenericResponseTransformerFactory', GenericResponseTransformerFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	GenericResponseTransformerFactory.$inject = [
		// Angular
		'$log'
	];

	function GenericResponseTransformerFactory(
		// Angular
		$log
	) {
		/**
		 * Transform { "data": [{},{}] } to generic objects [{},{}]
		 *
		 * @param data
		 * @param headers
		 * @param status
		 * @returns {*}
		 */
		function transformer(data, headers, status) {
			//$log.info('GenericResponseTransformer:', data);
			var parsedData = angular.fromJson(data);

			if (angular.isDefined(parsedData.data)) {
				return parsedData.data;
			}

			if (angular.isDefined(parsedData.errors)) {
				return parsedData.errors;
			}

			$log.warn('Transformer could not find any transformable information!');
		}

		return transformer;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('CountryUiModelFactory', CountryUiModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	CountryUiModelFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'CountryModelFactory',
		'CountryResourceFactory'
	];

	function CountryUiModelFactory(
		// Angular
		$log,
		// Custom
		CountryModelFactory,
		CountryResourceFactory
	) {
		/**
		 * Country Model for use in the User Interface only.
		 *
		 * @constructor
		 */
		function Country() {
			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$searchTextIso  = null;
			this.$$searchTextName = null;
			this.$$selected       = null;
		}

		// Methods
		// =======
		Country.prototype.$$createModel = function (data) {
			$log.log('$$createModel Country with:', data);

			return new CountryModelFactory(data);
		};

		Country.prototype.$$searchByIso = function () {
			var params = {
				'filter[iso]': this.$$searchTextIso
			};

			return CountryResourceFactory
				.query(params);
		};

		Country.prototype.$$searchByName = function () {
			var params = {
				'filter[name]': this.$$searchTextName
			};

			return CountryResourceFactory
				.query(params);
		};

		return Country;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('LocalityUiModelFactory', LocalityUiModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	LocalityUiModelFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'LocalityModelFactory',
		'LocalityResourceFactory'
	];

	function LocalityUiModelFactory(
		// Angular
		$log,
		// Custom
		LocalityModelFactory,
		LocalityResourceFactory
	) {
		/**
		 * Locality Model for use in the User Interface only.
		 *
		 * @constructor
		 */
		function Locality() {
			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$searchTextName       = null;
			this.$$searchTextPostalCode = null;
			this.$$selected             = null;
		}

		// Methods
		// =======
		//Locality.prototype.$$changeByName = function () {
		//	$log.log('$$changeByName');
		//	var self = this;
		//	this.$$searchByName()
		//		.$promise
		//		.then(function (results) {
		//			$log.log('$$changeByName results:', results);
		//			if (results.data.length < 1) {
		//				var data = {
		//					name       : self.$$searchTextName,
		//					postal_code: self.$$searchTextPostalCode
		//				};
		//				self.$$selected = self.$$createModel(data);
		//			}
		//		});
		//};

		//Locality.prototype.$$changeByPostalCode = function () {
		//	$log.log('$$changeByPostalCode');
		//	var self = this;
		//	this.$$searchByPostalCode()
		//		.$promise
		//		.then(function (results) {
		//			$log.log('$$changeByPostalCode results:', results);
		//			if (results.data.length < 1) {
		//				var data = {
		//					name       : self.$$searchTextName,
		//					postal_code: self.$$searchTextPostalCode
		//				};
		//				self.$$selected = self.$$createModel(data);
		//			}
		//		});
		//};

		Locality.prototype.$$createModel = function (data) {
			$log.log('$$createModel Locality with:', data);

			return new LocalityModelFactory(data);
		};

		Locality.prototype.$$searchByName = function () {
			var params = {
				'filter[name]': this.$$searchTextName
			};

			return LocalityResourceFactory
				.queryWithCountry(params);
		};

		Locality.prototype.$$searchByPostalCode = function () {
			var params = {
				'filter[postal_code]': this.$$searchTextPostalCode
			};

			return LocalityResourceFactory
				.queryWithCountry(params);
		};

		return Locality;
	}

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2014-2015 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () { 'use strict';

	angular.module('smuServices')
		.factory('RegionUiModelFactory', RegionUiModelFactory);

	// Inject dependencies into constructor (needed when JS minification is applied).
	RegionUiModelFactory.$inject = [
		// Angular
		'$log',
		// Custom
		'RegionModelFactory',
		'RegionResourceFactory'
	];

	function RegionUiModelFactory(
		// Angular
		$log,
		// Custom
		RegionModelFactory,
		RegionResourceFactory
	) {
		/**
		 * Region Model for use in the User Interface only.
		 *
		 * @constructor
		 */
		function Region() {
			// Properties omitted from JSON by Angular due to `$$` prefix.
			this.$$searchTextIso  = null;
			this.$$searchTextName = null;
			this.$$selected       = null;
		}

		// Methods
		// =======
		Region.prototype.$$createModel = function (data) {
			$log.log('$$createModel Region with:', data);

			return new RegionModelFactory(data);
		};

		Region.prototype.$$searchByIso = function () {
			var params = {
				'filter[iso]': this.$$searchTextIso
			};

			return RegionResourceFactory
				.queryWithCountry(params);
		};

		Region.prototype.$$searchByName = function () {
			var params = {
				'filter[name]': this.$$searchTextName
			};

			return RegionResourceFactory
				.queryWithCountry(params);
		};

		return Region;
	}

})();

//# sourceMappingURL=frontoffice.js.map