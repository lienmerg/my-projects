/**
 * Created by lienmergan on 18/12/14.
 */
$(function(){
    //linkernavigatie
    $.getJSON("data/navigation-left.json",callback2);
    //rechternavigatie
    //$.getJSON("data/navigation-right.json", callback3);
});

function callback2(data){
    console.log(data);
    $.each(data.navigation-left, function(i,linkernavigatie){
        console.log(linkernavigatie.label);
        var link = $("<a>").html(linkernavigatie.label).attr("href",linkernavigatie.page);
        $("<li>").html(link).appendTo("#navbar-left");
    });
}
