/**
 * Created by lienmergan on 18/12/14.
 */

$(function() {
    //iCheck functie voor het boodschappenlijstje
    $('input').iCheck({
        checkboxClass: 'icheckbox_flat-orange',
        radioClass: 'iradio_flat-orange',
        increaseArea: '20%' // optional
    });
    //slider ah begin vd ingrediënten
    $('#slider').nivoSlider();

    //video plugin voor de bereiding
    $('#cooking-dish').fitVids();

    //nice to know - tonen van foto's
    $('#ntk-1').fluidbox({
        viewportFill:0.9,
        debounceResize: false,
        stackIndex:900
    });
    $('#ntk-2').fluidbox({
        viewportFill:0.9,
        debounceResize: false,
        stackIndex:900
    });
    $('#ntk-3').fluidbox({
        viewportFill:0.9,
        debounceResize: false,
        stackIndex:900
    });
    $('#ntk-4').fluidbox({
        viewportFill:0.9,
        debounceResize: false,
        stackIndex:900
    });

    //cookies uitlezen en waarden in velden plaatsen
    var cookieFirstName = $.cookie("first_name");

    //enkel en alleen als de cookie bestaat, dit uitvoeren
    if(cookieFirstName != null){
        $("#first_name").val(cookieFirstName);
    }

    //cookies boodschappenlijstje uitlezen
    var chickenBreast = $.cookie("ingredient-item-1");
    if(chickenBreast != null){
        $("#ingredient-item-1").val(chickenBreast);
    }
    var cashewNut = $.cookie("ingredient-item-2");
    if(cashewNut != null){
        $("#ingredient-item-2").val(cashewNut);
    }
    var onionChopped = $.cookie("ingredient-item-3");
    if(onionChopped != null){
        $("#ingredient-item-3").val(onionChopped);
    }
    var garlicChopped = $.cookie("ingredient-item-4");
    if(chickenBreast != null){
        $("#first_name").val(chickenBreast);
    }
    var paprika = $.cookie("ingredient-item-5");
    if(chickenBreast != null){
        $("#first_name").val(chickenBreast);
    }
    var mushrooms = $.cookie("ingredient-item-6");
    if(chickenBreast != null){
        $("#first_name").val(chickenBreast);
    }
    var fishSauce = $.cookie("ingredient-item-7");
    if(chickenBreast != null){
        $("#first_name").val(chickenBreast);
    }
    var oysterSauce = $.cookie("ingredient-item-8");
    if(chickenBreast != null){
        $("#first_name").val(chickenBreast);
    }
    var brownSugar = $.cookie("ingredient-item-9");
    if(chickenBreast != null){
        $("#first_name").val(chickenBreast);
    }
    var riceOil = $.cookie("ingredient-item-10");
    if(chickenBreast != null){
        $("#first_name").val(chickenBreast);
    }

    //modernizr toevoegen
    $("html").addClass("no-js");
    $("<script>").attr("src", "components/modernizr/modernizr.js").appendTo("head");

    //memorygame initialiseren
    $('#my-memorygame').quizyMemoryGame({
        itemWidth: 156,
        itemHeight: 156,
        itemsMargin:40,
        colCount:5,
        animType:'flip',
        flipAnim:'tb',
        animSpeed:250,
        resultIcons:true
    });

    //add to shopping list
    var submitShoppingList = $('#submitShoppingList');
    var removeShoppingList = $('#removeShoppingList');
    submitShoppingList.click(function() {
        var toAdd = $('input#checkbox').val();
        $('#shopping-list').append('<input type="checkbox" checked /> ' + toAdd);
    });

});