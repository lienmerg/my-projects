My Projects
=============

**Author**: Lien Mergan

Here I have collected my projects I have worked on during my study Bachelor Graphical and Digital Media at the Arteveldehogeschool in Gent during academic year 2014-2015.

* a site about how to cook [chicken cashew](https://bitbucket.org/lienmerg/my-projects/src/5153caba08f7a0db43958731516f33748b3217d6/site_chicken_cashew/?at=master); the emphasis of this project was about the design and the use of JavaScript and jQuery
* a [to do application](https://bitbucket.org/lienmerg/my-projects/src/0e4e6e5e68787a20b8f35686d31ecbbded6738b1/site_todo_application/?at=master); emphasis here is on the use of JavaScript and a modern design
* a [carpool application](https://bitbucket.org/lienmerg/my-projects/src/1f8aa98356aaa7c61e1c2e36d72d7a7422e5ef72/site_carpool_application/?at=master); emphasis of this project is on Laravel in combination with AngularJS